package com.surveyor.insurvey.in_surveyapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.melnykov.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import adapter.ArrayAdapterWithIcon;
import dto.ConnectionDetector;
import dto.FormCountDTO;
import dto.FormDTO;
import dto.UserDTO;
import dto.UserProfileDTO;
import sql.Contact;
import sql.DataBaseHandler;
import sql.FormCountTableAdapter;
import sql.FormTableAdapter;
import sql.UserTableAdapter;


public class ProfileActivity extends ActionBarActivity {
private ImageView profile;
    private TextView fname;
    private TextView lname;
    private TextView uphone;
    private TextView uemail;

    private TextView c_name;
    private TextView c_desc;
    private TextView c_contact;
    private TextView c_email;
    private TextView c_physical;
    private TextView c_postal;
    private int userID;
    private UserProfileDTO userProfileDTO;
    private EditText edtOldPassword;
    private EditText edtNewPassword;
    private EditText edtConfirmPassword;

    EditText editTextfname,editTextlname,editTextemail,editTextphone;
    private Button submitPassword;
    private Button btnCancelNewPass;
    private String oldPass,newPass,confirmPass;
    private Button btnChangePassword;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private ProgressDialog dialog;
    private SharedPreferences sp;
    private UserTableAdapter uta;
    private UserDTO userDTO;
    private EditText textViewFirstName,
                     editTextLastName,
                     editTextEmail,
                     editTextPhoneNo;
    Button buttonUpdateDetails,addImage;
    ImageView imgProfile;
    private Bitmap myProfileImage;
    ArrayList<Contact> imageArry = new ArrayList<Contact>();
    private static final int CAMERA_REQUEST = 1;
    private static final int PICK_FROM_GALLERY = 2;
    ListView dataList;
    byte[] imageName;
    int imageId;

    DataBaseHandler db;
    String TITLES[] = {"Main Menu", "Download", "Survey List", "Surveys Actions","Completed","About", "Logout"};
    int ICONS[] = {R.drawable.ic_home,
            R.drawable.ic_download,
            R.drawable.ic_list,
            R.drawable.ic_actions,
            R.drawable.ic_completed,
            R.drawable.ic_about,
            R.drawable.ic_logout};

    String NAME = "";
    String EMAIL = "";
    int PROFILE = R.drawable.pro;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
      //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        NAME = sp.getString("name","") +" "+sp.getString("lastname","");
        Log.i("Names ",sp.getString("name",""));
        Log.i("Names ",sp.getString("lastname",""));
        Log.i("Names ",NAME);
        EMAIL = sp.getString("email","");
        setFields();
        setNavigationDrawer();

    }
   private void setDialog() {
    LayoutInflater factory = LayoutInflater.from(this);
    final View deleteDialogView = factory.inflate(
            R.layout.change_pass, null);
    final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
    deleteDialog.setView(deleteDialogView);
    deleteDialog.setCancelable(false);
    deleteDialog.setCanceledOnTouchOutside(false);
    //setFinishOnTouchOutside(false);
    edtOldPassword = (EditText) deleteDialogView.findViewById(R.id.edtOldPassword);
    edtNewPassword = (EditText) deleteDialogView.findViewById(R.id.edtNewPassword);
    edtConfirmPassword = (EditText) deleteDialogView.findViewById(R.id.edtConfirmNewPassword);
    submitPassword = (Button)deleteDialogView.findViewById(R.id.btnSubmitNewPassword);
    btnCancelNewPass = (Button)deleteDialogView.findViewById(R.id.btnCancelChengePassword);
    imgProfile = (ImageView)findViewById(R.id.imgProfile);
    submitPassword.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            oldPass = edtOldPassword.getText().toString();
            newPass = edtNewPassword.getText().toString();
            confirmPass = edtConfirmPassword.getText().toString();
            int uID = sp.getInt("userID",0);
           final  UserDTO u = new UserDTO();
            u.setUserID(uID);
            u.setPassword(confirmPass);
           // Toast.makeText(getApplicationContext(), uID + " Old Pass " + oldPass, Toast.LENGTH_SHORT).show();
            cd = new ConnectionDetector(getApplicationContext());
            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                if (oldPass.length() > 0) {
                    if (newPass.length() > 0) {
                        if (newPass.equals(confirmPass)) {
                             dialog("Updating password...");
                            //String URL = "http://10.0.2.2/insurvey/index.php?requestType=14&oldPassword="+oldPass+"&newPassword="+newPass+"&userID="+Integer.toString(uID);
                            String URL = "http://www.insurvey.co.za/api?requestType=14&oldPassword="+oldPass+"&newPassword="+newPass+"&userID="+Integer.toString(uID);
                          //  Toast.makeText(getApplicationContext(), uID+" Old Pass "+oldPass, Toast.LENGTH_SHORT).show();
                            JSONObject js = new JSONObject();

                            //Log.i(LOG, js.toString());
                            RequestQueue queue = Volley
                                    .newRequestQueue(getApplicationContext());
                            try {
                                StringRequest request = new StringRequest(URL,
                                        new Response.Listener<String>() {

                                            @Override
                                            public void onResponse(String arg0) {



                                                //  Log.i(LOG, arg0.toString());

                                                myVolley(arg0);
                                                int pass = updateUserProfileSQLLite(u);

                                                if(pass > 0){

                                                }
                                                 dialog.dismiss();

                                            }
                                        }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(
                                            VolleyError arg0) {
                                        // TODO Auto-generated method stub

                                         dialog.dismiss();
                                        internetErrorDialog();
                                        // toast("Check your network connection");
                                        // Log.i(LOG, arg0.toString());
                                    }
                                });
                                queue.add(request);
                            } catch (Exception e) {

                                 dialog.dismiss();
                                e.printStackTrace();
                            }
                        } else {
                            edtConfirmPassword.setError("New Password and Password Confirmation differs.");
                        }
                    } else {
                        edtNewPassword.setError("New password is required.");
                    }
                } else {
                    edtOldPassword.setError("Old password is required");
                }
            } else {
                internetErrorDialog();
            }

            // Toast.makeText(getApplicationContext(), "Inserted ", Toast.LENGTH_SHORT).show();
            // startActivity(new Intent(getApplicationContext(), do_survey.class));

            // Toast.makeText(getApplicationContext(), "Inserted ", Toast.LENGTH_SHORT).show();
            // startActivity(new Intent(getApplicationContext(), do_survey.class));

        }
    });
       btnCancelNewPass.setOnClickListener(new View.OnClickListener() {

           @Override
           public void onClick(View v) {
               deleteDialog.dismiss();
           }
       });

    deleteDialog.show();
}
private void setFields() {
    UserProfileDTO userProfileDTO = new UserProfileDTO();
    db = new DataBaseHandler(this);
    buttonUpdateDetails = (Button) findViewById(R.id.buttonUpdateDetails);
    fname = (TextView) findViewById(R.id.tv_profile_name);
    c_name = (TextView) findViewById(R.id.tv_company);
    c_email = (TextView) findViewById(R.id.tv_email_address);
    textViewFirstName = (EditText) findViewById(R.id.editTextFirstName);
    editTextLastName=(EditText) findViewById(R.id.editTextLastName);
    editTextEmail=(EditText) findViewById(R.id.editTextEmail);
    editTextPhoneNo= (EditText) findViewById(R.id.editTextPhoneNo);
    fname.setText(sp.getString("name","")+" "+sp.getString("lastname",""));
    c_name.setText(sp.getString("companyName",""));
    uta = new UserTableAdapter(getApplicationContext());
    c_email.setText(sp.getString("email",""));

    editTextfname = (EditText) findViewById(R.id.editTextFirstName);
    editTextlname = (EditText) findViewById(R.id.editTextLastName);
    editTextemail = (EditText) findViewById(R.id.editTextEmail);
    editTextphone = (EditText) findViewById(R.id.editTextPhoneNo);

    textViewFirstName.setText(sp.getString("name",""));
    editTextLastName.setText(sp.getString("lastname",""));
    editTextEmail.setText(sp.getString("email",""));
    editTextPhoneNo.setText(sp.getString("phone", ""));

    btnChangePassword = (Button)findViewById(R.id.btnChangePassword);
    btnChangePassword.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
               setDialog();
        }
    });
    profile = (ImageView)findViewById(R.id.imgProfile);
    String b = sp.getString("imagePreferance"+sp.getInt("userID",0),"");
    myProfileImage = BitmapFactory.decodeResource(getResources(), R.drawable.pro);
    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            selectImageOption();
        }
    });


    if(sp.getInt("user",0)==sp.getInt("userID",0)){
        /**
         * Reading and getting all records from database
         */
            Contact contacts = db.getContact(sp.getInt("userID",0));
           // Toast.makeText(getApplicationContext(),"Image Key "+contacts.get_userid(),Toast.LENGTH_SHORT).show();
                byte[] outImage = contacts.getImage();
                ByteArrayInputStream imageStream = new ByteArrayInputStream(outImage);
                myProfileImage = BitmapFactory.decodeStream(imageStream);
                profile.setImageBitmap(myProfileImage);
               // Toast.makeText(getApplicationContext(),"Image set ",Toast.LENGTH_SHORT).show();

    }

    profile.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            selectImageOption();
            return true;
        }
    });
    buttonUpdateDetails.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Toast.makeText(getApplicationContext(),"Still under construction",Toast.LENGTH_SHORT).show();
            updateUserProfile();
        }
    });

}

    private void setNavigationDrawer(){
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        FormTableAdapter fta = new FormTableAdapter(getApplicationContext());
        FormCountTableAdapter ftac = new FormCountTableAdapter(getApplicationContext());
        List<FormDTO> frmList = new ArrayList<FormDTO>();
        List<FormCountDTO> countDTOs = new ArrayList<FormCountDTO>();
        fta.open();
        frmList = fta.getFormList(sp.getInt("userID",0));
        fta.close();
        ftac.open();
        countDTOs = ftac.getFormCountList(sp.getInt("userID",0));
        ftac.close();
        String cnt[];
        if(sp.getString("fc","").equals("")){
            cnt = new String[]{"-1",String.valueOf(sp.getInt("formCount",0) - frmList.size()),String.valueOf(frmList.size()),"-1","-1","-1","-1"};
        }else{
            cnt = new String[] {"-1",sp.getString("fc",""),String.valueOf(frmList.size()),"-1","-1","-1","-1"};
        }
        mAdapter = new MyAdapter(TITLES,ICONS,NAME,EMAIL,PROFILE,this,cnt,myProfileImage);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        final GestureDetector mGestureDetector = new GestureDetector(ProfileActivity.this, new GestureDetector.SimpleOnGestureListener() {

            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });


        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(),motionEvent.getY());

                if(child!=null && mGestureDetector.onTouchEvent(motionEvent)){
                   // Drawer.closeDrawers();
                  //  Toast.makeText(ProfileActivity.this, "The Item Clicked is: " + recyclerView.getChildPosition(child), Toast.LENGTH_SHORT).show();
                    navigate(recyclerView.getChildPosition(child));
                    return true;
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }


        });

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.openDrawer,R.string.closeDrawer){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }



        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

    }
    private void navigate(int number){
        Context cta = getApplicationContext();
        Intent intent;
        switch (number) {
            case 0:
                Drawer.closeDrawers();
                break;
            case 1:
                intent = new Intent(cta,Main_OneActivity.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 2:
                if(sp.getString("fc","").equals("")) {
                intent = new Intent(cta, MySurvey_List.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                }
                break;
            case 3:
                    intent = new Intent(cta, List_Downloaded_Surveys.class);
                    // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Drawer.closeDrawers();
                    startActivity(intent);
                    finish();
                break;

            case 4:
                intent = new Intent(cta, SurveyActionActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;

            case 5:
                intent = new Intent(cta, CompletedSurveyActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 6:
                Toast.makeText(getApplicationContext(),"Still Under Construction",Toast.LENGTH_SHORT).show();
                Drawer.closeDrawers();
                break;
            case 7:
                intent = new Intent(cta,LogIn.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;

        }

    }
    private void myVolley(String arg0) {
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                infoDialog(obj.getString("message"));
                JSONArray array = obj.getJSONArray("users");
                JSONObject jso = new JSONObject();
                for (int i = 0; i < array.length(); ++i) {
                    jso = array.getJSONObject(i);
                    userDTO = new UserDTO(jso.getInt("id_user"),
                            jso.getString("name"),
                            jso.getString("username"),
                            jso.getString("password"),
                            jso.getString("phone"),
                            jso.getString("email"),
                            jso.getString("physical_address"),
                            jso.getString("postal_address"),
                            jso.getInt("id_company_fk"),
                            jso.getInt("id_permission_fk"),jso.getString("lastname"));
                   // dialog.dismiss();
                }
                //toast_error(obj.getString(user.getName()));
                //updateUserIntoSQLlite(userDTO);
            } else {
                dialog.dismiss();
                infoDialog(obj.getString("message"));
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private void myUpdateUserVolley(String arg0) {
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                infoDialog("User updated successfully");

               // update user in sqllite
            } else {
                dialog.dismiss();
                infoDialog(obj.getString("Unable to update user"));
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private void updateUserProfile(){

        final String fname = editTextfname.getText().toString();
        final String lname = editTextLastName.getText().toString();
        final  String email = editTextEmail.getText().toString();
        final String phone = editTextPhoneNo.getText().toString();
        final UserDTO u = new UserDTO();
        u.setUserID(sp.getInt("userID",0));
        u.setEmail(email);
        u.setPhone(phone);
        u.setLastname(lname);
        u.setName(fname);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            if (fname.matches("[a-zA-Z ]+")) {
                if (lname.matches("[a-zA-Z ]+")) {
                    if (isEmailValid(email.toString()) == true) {
                         if (phone.length() == 10) {

                        dialog("Updating user...");

                        // String URL = "http://10.0.2.2/insurvey/index.php?requestType=18&userID="+sp.getInt("userID",0)+"&lname="+lname+"&phone="+phone+"&email="+email+"&fname="+fname;
                        String URL = "http://www.insurvey.co.za/api?requestType=18&userID="+sp.getInt("userID",0)+"&lname="+lname+"&phone="+phone+"&email="+email+"&fname="+fname;
                        RequestQueue queue = Volley
                                .newRequestQueue(getApplicationContext());
                        try {
                            StringRequest request = new StringRequest(URL,
                                    new Response.Listener<String>() {

                                        @Override
                                        public void onResponse(String arg0) {
                                            //  Log.i(LOG, arg0.toString())
                                            myUpdateUserVolley(arg0);
                                            dialog.dismiss();
                                            SharedPreferences.Editor ed = sp.edit();
                                            ed.putString("name",fname);
                                            ed.putString("lastname",lname);
                                            ed.putString("email",email);
                                            ed.putString("phone",phone);
                                            ed.commit();

                                            int row_aff = updateUserProfileSQLLite(u);

                                            if(row_aff > 0){
                                               // Toast.makeText(getApplicationContext(),"User Updated success",Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                    }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(
                                        VolleyError arg0) {
                                    // TODO Auto-generated method stub

                                    dialog.dismiss();
                                    internetErrorDialogprofile();
                                    // toast("Check your network connection");
                                    // Log.i(LOG, arg0.toString());
                                }
                            });
                            queue.add(request);
                        } catch (Exception e) {

                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    } else {
                             editTextPhoneNo.setError("Phone Must Be 10 Digits.");
                    }
                } else {
                    editTextemail.setError("Invalid Email Address.");
                }
            } else {
                editTextfname.setError("Surname must be Alphabets Only.");
            }
        } else {
                editTextlname.setError("Name must be Alphabets Only.");
        }


    }else{
            internetErrorDialog();
        }
    }

    private void updateUserIntoSQLlite(UserDTO p){
        uta = new UserTableAdapter(getApplicationContext());
        uta.open();
        uta.updateUserPassword(p);
        uta.close();
    }
    private int updateUserProfileSQLLite(UserDTO u){
        uta = new UserTableAdapter(getApplicationContext());
        uta.open();
        int row_aff =  uta.updateUserDTO(u);
        uta.close();

        return  row_aff;

    }


    private void dialog(String status) {
        dialog = new ProgressDialog(ProfileActivity.this,
                AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Please wait");
        dialog.setTitle(status);
        dialog.setIcon(R.drawable.abc_edit_text_material);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }

    private void errorDialog(String m){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage(m);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
// here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.error32);
        alertDialog.show();
    }
    private void infoDialog(String m){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Info");
        alertDialog.setMessage(m);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
// here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.warning_blue);
        alertDialog.show();
    }
    private void internetErrorDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No internet connection.please connect to the internet to change password.");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
// here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.error32);
        alertDialog.show();
    }
    private void internetErrorDialogprofile(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage("No internet connection.please connect to update profile.");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
// here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.error32);
        alertDialog.show();
    }
    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.main_menu) {
            startActivity(new Intent(ProfileActivity.this,Main_OneActivity.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
private void selectImageOption(){
    final String [] items = new String[] {"From Gallery", "From Camera"};
    final Integer[] icons = new Integer[] {R.drawable.ic_gal, R.drawable.ic_camera};
    ListAdapter adapter = new ArrayAdapterWithIcon(ProfileActivity.this, items, icons);

    new AlertDialog.Builder(ProfileActivity.this).setTitle("Select Option")
            .setAdapter(adapter, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item ) {
                    if (item == 0) {
                        callGallery();
                    }
                    if (item == 1) {

                        callCamera();
                    }
                }
            }).show();

}
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageView myProfileImage ;
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case CAMERA_REQUEST:

                Bundle extras = data.getExtras();

                if (extras != null) {
                    myProfileImage = (ImageView)findViewById(R.id.circleView);
                    Bitmap yourImage = extras.getParcelable("data");
                    // convert bitmap to byte
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    yourImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    profile.setImageBitmap(yourImage);
                    String encodedProfileImage = encodeTobase64(yourImage);
                    SharedPreferences.Editor ed = sp.edit();
                    ed.putString("encodedProfileImage",encodedProfileImage);
                    ed.commit();
                    myProfileImage.setImageBitmap(yourImage);
                    byte imageInByte[] = stream.toByteArray();
                    Log.e("output before ", imageInByte.toString());
                    // Inserting Contacts
                    Log.d("Insert: ", "Inserting ..");
                    if(sp.getInt("user",0) == sp.getInt("userID",0)){
                        db.updateContact((new Contact(sp.getInt("userID",0),"Android", imageInByte)));
                       // Toast.makeText(ProfileActivity.this, "Updated " , Toast.LENGTH_SHORT).show();
                    }else{
                        db.addContact(new Contact(sp.getInt("userID",0),"Android", imageInByte));
                       // Toast.makeText(ProfileActivity.this, "Inserted", Toast.LENGTH_SHORT).show();
                        ed.putInt("user",sp.getInt("userID",0));
                        ed.commit();
                    }

                }
                break;
            case PICK_FROM_GALLERY:
                Bundle extras2 = data.getExtras();

                if (extras2 != null) {
                    Bitmap yourImage = extras2.getParcelable("data");
                    // convert bitmap to byte
                    myProfileImage = (ImageView)findViewById(R.id.circleView);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    yourImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    profile.setImageBitmap(yourImage);
                    myProfileImage.setImageBitmap(yourImage);
                    String encodedProfileImage = encodeTobase64(yourImage);
                    SharedPreferences.Editor ed = sp.edit();
                    ed.putString("encodedProfileImage",encodedProfileImage);
                    ed.commit();
                    byte imageInByte[] = stream.toByteArray();
                    Log.e("output before ", imageInByte.toString());
                    // Inserting Contacts
                    Log.d("Insert: ", "Inserting ..");
                    if(sp.getInt("user",0) == sp.getInt("userID",0)){
                        db.updateContact((new Contact(sp.getInt("userID",0),"Android", imageInByte)));
                        //Toast.makeText(ProfileActivity.this, "Updated " , Toast.LENGTH_SHORT).show();
                    }else{
                        db.addContact(new Contact(sp.getInt("userID",0),"Android", imageInByte));
                        //Toast.makeText(ProfileActivity.this, "Inserted", Toast.LENGTH_SHORT).show();
                        ed.putInt("user",sp.getInt("userID",0));
                        ed.commit();
                    }

                }

                break;
        }
    }
    public void callCamera() {
        Intent cameraIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra( android.provider.MediaStore.EXTRA_SIZE_LIMIT, "720000");
        startActivityForResult(cameraIntent, CAMERA_REQUEST);

    }

    /**
     * open gallery method
     */

    public void callGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 0);
        intent.putExtra("aspectY", 0);
        intent.putExtra("outputX", 200);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(
                Intent.createChooser(intent, "Complete action using"),
                PICK_FROM_GALLERY);

    }
    public static String encodeTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);


        Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }
    public Bitmap ByteArrayToBitmap(byte[] byteArray)
    {
        ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(byteArray);
        Bitmap bitmap = BitmapFactory.decodeStream(arrayInputStream);
        return bitmap;
    }


}


