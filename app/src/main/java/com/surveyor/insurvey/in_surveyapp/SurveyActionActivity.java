package com.surveyor.insurvey.in_surveyapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import adapter.SurveyActionAdapter;
import dto.ActivityLogDTO;
import dto.FormCountDTO;
import dto.FormDTO;
import dto.PersonDTO;
import dto.SurveyActionDTO;
import sql.ActiveLogTableAdapter;
import sql.FormCountTableAdapter;
import sql.FormTableAdapter;
import sql.PersonTableAdapter;


public class SurveyActionActivity extends ActionBarActivity {
   private ListView list;
   private SurveyActionDTO surveyActionDTO;
   private SurveyActionAdapter saa;
   private Context ctx;
   private List<SurveyActionDTO> surveyActionDTOs;
   private ProgressDialog dialog;

    private PersonTableAdapter pta;
    private ActiveLogTableAdapter atlta;
    private List<PersonDTO> p;
    private List<ActivityLogDTO> activityLogDTOs;
    private SharedPreferences sp;
    private FormTableAdapter ftac;
    private RequestQueue mRequestQueue;
    private FormDTO formDTO;
    private Bitmap myProfileImage;
    private List<FormDTO> lst;
    private int cnt[];
    private boolean found = false;
    List<FormDTO> frmList;
    private RelativeLayout surveyActionLayout;
    private Button btnCloseForm;

    String TITLES[] = {"Main Menu", "Download", "Survey List", "Surveys Actions","Completed","About", "Logout"};
    int ICONS[] = {R.drawable.ic_home,
            R.drawable.ic_download,
            R.drawable.ic_list,
            R.drawable.ic_actions,
            R.drawable.ic_completed,
            R.drawable.ic_about,
            R.drawable.ic_logout};

    String NAME = "";
    String EMAIL = "";
    int PROFILE = R.drawable.pro;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_action);
        setValues();
    }

    private void setValues(){
        ctx = getApplicationContext();
        sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        list = (ListView) findViewById(android.R.id.list);
        surveyActionDTO = new SurveyActionDTO();
        surveyActionDTOs = new ArrayList<SurveyActionDTO>();
        NAME = sp.getString("name","") +" "+sp.getString("lastname","");
        EMAIL = sp.getString("email","");
        surveyActionLayout = (RelativeLayout)findViewById(R.id.surveyActionLayout);
        btnCloseForm = (Button)findViewById(R.id.btnCloseForm);

        btnCloseForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Main_OneActivity.class));
                finish();
            }
        });

        querySurveyActionList();
        setNavigationDrawer();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_survey_action, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void querySurveyActionList(){
        mRequestQueue = Volley.newRequestQueue(this);
        RequestQueue queue = Volley
                .newRequestQueue(getApplicationContext());
        dialog();
        int userID = sp.getInt("userID",0);
       // String url = "http://10.0.2.2/insurvey/index.php?requestType=20&userID=" + userID;
       String url = "http://www.insurvey.co.za/api?requestType=20&userID=" + userID;

        StringRequest request = new StringRequest(url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String arg0) {
                      //  Log.i(LOG, arg0.toString());
                        voly(arg0);
                        setFormListAdapter(surveyActionDTOs);
                        dialog.dismiss();
                        // sla.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(
                    VolleyError arg0) {
                // TODO Auto-generated method stub
                //dialog.dismiss();
                // toast("Network Connection Error");
                //toast("Check your network connection");
                //Log.i(LOG, arg0.toString());
            }
        });
        queue.add(request);
    }

    private void voly(String arg0) {
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("0")) {
                surveyActionLayout.setVisibility(View.GONE);
              Log.i("Success ",obj.getString("success"));
                JSONArray array = obj.getJSONArray("forms");
                JSONObject jso = new JSONObject();
                for (int i = 0; i < array.length(); ++i) {
                    jso = array.getJSONObject(i);
                    surveyActionDTO = new SurveyActionDTO(jso.getString("start_date_time"),jso.getString("end_date_time"),
                            jso.getString("start_city"),jso.getString("end_city"), jso.getString("name"));

                    surveyActionDTOs.add(surveyActionDTO);

                }
            } else {
                // toast(obj.getString("message"));
                //
                surveyActionLayout.setVisibility(View.VISIBLE);
                Log.i("TAG", obj.getString("message"));
                dialog.dismiss();
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    private void setNavigationDrawer(){
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size
        FormTableAdapter fta = new FormTableAdapter(getApplicationContext());
        FormCountTableAdapter ftac = new FormCountTableAdapter(getApplicationContext());
        List<FormDTO> frmList = new ArrayList<FormDTO>();
        List<FormCountDTO> countDTOs = new ArrayList<FormCountDTO>();
        fta.open();
        frmList = fta.getFormList(sp.getInt("userID",0));
        fta.close();
        ftac.open();
        countDTOs = ftac.getFormCountList(sp.getInt("userID",0));
        ftac.close();
        String cnt[];
        if(sp.getString("fc","").equals("")){
            cnt = new String[]{"-1",String.valueOf(sp.getInt("formCount",0) - frmList.size()),String.valueOf(frmList.size()),"-1","-1","-1","-1"};
        }else{
            cnt = new String[] {"-1",sp.getString("fc",""),String.valueOf(frmList.size()),"-1","-1","-1","-1"};
        }
        myProfileImage = BitmapFactory.decodeResource(getResources(), R.drawable.pro);
        String encodedProfileImage = sp.getString("encodedProfileImage","");
        if( !encodedProfileImage.equalsIgnoreCase("") ){
            byte[] b = Base64.decode(encodedProfileImage, Base64.DEFAULT);
            myProfileImage = BitmapFactory.decodeByteArray(b, 0, b.length);
            // imageConvertResult.setImageBitmap(bitmap);
        }

        mAdapter = new MyAdapter(TITLES,ICONS,NAME,EMAIL,PROFILE,getApplicationContext(),cnt,myProfileImage);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        final GestureDetector mGestureDetector = new GestureDetector(ctx, new GestureDetector.SimpleOnGestureListener() {

            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });


        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(),motionEvent.getY());

                if(child!=null && mGestureDetector.onTouchEvent(motionEvent)){
                    //Drawer.closeDrawers();
                   // Toast.makeText(ctx, "The Item Clicked is: " + recyclerView.getChildPosition(child), Toast.LENGTH_SHORT).show();
                    navigate(recyclerView.getChildPosition(child));
                    return true;

                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }


        });

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.openDrawer,R.string.closeDrawer){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }



        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

    }
    private void navigate(int number){

        Intent intent;
        switch (number) {
            case 0:
                intent = new Intent(ctx, ProfileActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 1:
                intent = new Intent(SurveyActionActivity.this,Main_OneActivity.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 2:
                if(sp.getString("fc","").equals("")) {
                intent = new Intent(SurveyActionActivity.this, MySurvey_List.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                }
                break;
            case 3:
                    intent = new Intent(SurveyActionActivity.this, List_Downloaded_Surveys.class);
                    // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Drawer.closeDrawers();
                    startActivity(intent);
                    finish();
                break;

            case 4:
                Drawer.closeDrawers();
                break;

            case 5:
                intent = new Intent(SurveyActionActivity.this, CompletedSurveyActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 6:
                Toast.makeText(getApplicationContext(),"Still Under Construction",Toast.LENGTH_SHORT).show();
                Drawer.closeDrawers();
                break;
            case 7:
                intent = new Intent(SurveyActionActivity.this,LogIn.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
        }

    }
    private void setFormListAdapter(List<SurveyActionDTO> fl) {
        saa = new SurveyActionAdapter(getApplicationContext(), R.layout.survey_action, fl);
        list.setAdapter(saa);

    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(),Main_OneActivity.class);
        startActivity(intent);
        finish();

    }
    private void dialog() {
        dialog = new ProgressDialog(SurveyActionActivity.this,
                AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Please wait");
        dialog.setTitle("loading ...");
        dialog.setIcon(R.drawable.form_small);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }
}
