package com.surveyor.insurvey.in_surveyapp;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.util.ArrayList;

import WriteData.ImageFile;
import dto.QuestionImage;
import sql.QuestionImageTableAdapter;

public class TestImage extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_image);

        ImageView img = (ImageView) findViewById(R.id.img);
        ArrayList<QuestionImage> qImage = new ArrayList<>();

        QuestionImageTableAdapter qta = new QuestionImageTableAdapter(getApplicationContext());
        qta.open();
        qImage = qta.getFormList(887);
        qta.close();


       // img.setImageBitmap(imgBit);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
