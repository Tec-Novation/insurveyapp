package com.surveyor.insurvey.in_surveyapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dto.UserDTO;
import dto.UserProfileDTO;
import sql.UserTableAdapter;


public class RegisterActivity extends ActionBarActivity {
    private Button btnRegister;
    private EditText name;
    private EditText lastName;
    private EditText phone;
    private EditText email;
    private EditText physicalAddress;
    private EditText postalAddress;
    private EditText password;
    private EditText ConfirmPassword;
    private  EditText username;
    private SharedPreferences sp;
    private ProgressDialog dialog;
    private Context ctx;
    private UserProfileDTO dto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setUserFields();
    }

    private void setUserFields(){
        ctx = getApplicationContext();
        sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        userTableAdapter = new UserTableAdapter(ctx);
        name = (EditText)findViewById(R.id.edtUName);
        lastName= (EditText)findViewById(R.id.edtULastName);
        phone= (EditText)findViewById(R.id.edtUphone);
        email= (EditText)findViewById(R.id.edtEmail);
        physicalAddress= (EditText)findViewById(R.id.edtPhysicallAddress);
        postalAddress= (EditText)findViewById(R.id.edtPostalAddress);
        password= (EditText)findViewById(R.id.edtRegPassword);
        ConfirmPassword= (EditText)findViewById(R.id.edtRegConfirmPassword);
        btnRegister = (Button)findViewById(R.id.btnRegisterButton);
        username = (EditText)findViewById(R.id.edtRegUsername);
        name.requestFocus();


        btnRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

             String url = "http://www.insurvey.co.za/api?requestType=9&name="+name.getText().toString()+"&lastname="+lastName.getText().toString()+"&username="+username.getText().toString()+"&password="+password.getText().toString()+"&phone="+phone.getText().toString()
                     +"&email="+email.getText().toString()+"&physical_address="+physicalAddress.getText().toString()+"&postal_address="+postalAddress.getText().toString();
                        //String url = "http://10.0.2.2/insurvey/index.php?requestType=9&name="+name.getText().toString()+"&lastname="+lastName.getText().toString()+"&username="+username.getText().toString()+"&password="+password.getText().toString()+"&phone="+phone.getText().toString()
                      // +"&email="+email.getText().toString()+"&physical_address="+physicalAddress.getText().toString()+"&postal_address="+postalAddress.getText().toString();
                JSONObject jo = new JSONObject();
                if (name.getText().toString().matches("[a-zA-Z ]+")) {
                    if (lastName.getText().toString().matches("[a-zA-Z ]+")) {
                        if (phone.getText().length() == 10) {
                            if (isEmailValid(email.getText().toString()) == true) {
                                if (password.getText().length() > 0) {
                                    if (password
                                            .getText()
                                            .toString()
                                            .equals(ConfirmPassword.getText()
                                                    .toString())) {
                                        dialog();
                                        RequestQueue queue = Volley
                                                .newRequestQueue(ctx);
                                        try {
                                            StringRequest request = new StringRequest(
                                                    Request.Method.POST,
                                                    url,
                                                    new Response.Listener<String>() {

                                                        @Override
                                                        public void onResponse(String arg0) {
                                                            Log.i("TAG ERROR",arg0.toString());
                                                                    myVolley(arg0);
                                                        }
                                                    },
                                                    new Response.ErrorListener() {

                                                        @Override
                                                        public void onErrorResponse(
                                                                VolleyError arg0) {
                                                            dialog.dismiss();
                                                            errorDialog();
                                                            Log.i("TAG ERROR",
                                                                    arg0.toString());
                                                        }
                                                    });

                                            queue.add(request);
                                            Log.i("TAG", jo.toString());
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    } else {

                                        ConfirmPassword
                                                .setError("Password Do Not Match.");

                                    }
                                } else {
                                    password.setError("Please Enter Password.");
                                }
                            } else {
                                email.setError("Invalid Email Address.");
                            }

                        } else {
                            phone.setError("Phone Must Be 10 Digits.");
                        }
                    } else {
                        lastName.setError("Surname must be Alphabets Only.");
                    }
                } else {
                    name.setError("Name Must Be Alphabets Only.");
                }
            }
        });
    }
    private void myVolley(String arg0) {
        UserDTO user = null;
        dto = new UserProfileDTO();

        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                toast(obj.getString("message"));
                JSONArray array = obj.getJSONArray("users");
                JSONObject jso = new JSONObject();
                for (int i = 0; i < array.length(); ++i) {
                    jso = array.getJSONObject(i);
                     user = new UserDTO(jso.getInt("id_user"),
                            jso.getString("name"),
                            jso.getString("username"),
                            jso.getString("password"),
                            jso.getString("phone"),
                            jso.getString("email"),
                            jso.getString("physical_address"),
                            jso.getString("postal_address"),
                            jso.getInt("id_company_fk"),
                            jso.getInt("id_permission_fk"),jso.getString("lastname"));
                    Toast.makeText(ctx,user.getName()+" Logged",Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor ed = sp.edit();
                    ed.putInt("userID",user.getUserID());
                    ed.commit();
                    //Log.i("TAG obj.getString", obj.getString("name"));
                    Log.i("TAG user.getName()", user.getName());
                    //CreateUserTable(user);
                    dialog.dismiss();
                    startActivity(new Intent(RegisterActivity.this,
                            LogIn.class));

                    finish();
                }
                //toast_error(obj.getString(user.getName()));
                dto.setUserID(user.getUserID());
                InsertUserIntoSQLlite(user);

            } else {
                toast_error(obj.getString("message"));
              dialog.dismiss();
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }
    private void dialog() {
        dialog = new ProgressDialog(RegisterActivity.this,
                AlertDialog.THEME_HOLO_LIGHT);
        dialog.setMessage("Registering...");
        dialog.setTitle("Please wait");
        dialog.setIcon(R.drawable.warning_blue);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }
    private void toast_error(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_error,
                (ViewGroup) findViewById(R.id.toast_layout_root1));
        TextView textView = (TextView) layout.findViewById(R.id.txtTOASTMessage1);
        textView.setText(message);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
    private void errorDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Network connection error");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
// here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.ic_error);
        alertDialog.show();
    }
    private void errorRegisterEmail(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Email already exist");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
// here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.ic_error);
        alertDialog.show();
    }

    private void toast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,
                (ViewGroup) findViewById(R.id.toast_layout_root));
        TextView textView = (TextView) layout.findViewById(R.id.txtTOASTMessage);
        textView.setText(message);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
    private void InsertUserIntoSQLlite(UserDTO userDTO){
        try {
            userTableAdapter.open();
            userTableAdapter.insertUsers(userDTO);
            userTableAdapter.close();
        }catch (Exception e){
            //Toast.makeText(ctx,"Not inserted",Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
    private UserTableAdapter userTableAdapter;
}
