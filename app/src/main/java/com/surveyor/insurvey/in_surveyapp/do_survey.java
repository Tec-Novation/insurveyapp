package com.surveyor.insurvey.in_surveyapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import WriteData.ImageFile;
import dto.ActivityLogDTO;
import dto.AnswersDTO;
import dto.ConnectionDetector;
import dto.ContactDTO;
import dto.DemorgraphicDTO;
import dto.FormCountDTO;
import dto.PersonDTO;
import dto.QuestionImage;
import dto.SaveAnswersDTO;
import dto.SectionQuestionDTO;
import dto.SurveyActionDTO;
import sql.ActiveLogTableAdapter;
import sql.ContactTableAdapter;
import sql.DemorgraphicTableAdapter;
import sql.FormCountTableAdapter;
import sql.PersonTableAdapter;
import sql.QuestionImageTableAdapter;
import sql.SaveAnswerTableAdapter;
import sql.SectionQuestionTableAdapter;
import sql.SurveyActionTableAdapter;

import static com.android.volley.Request.Priority;


public class do_survey extends ActionBarActivity{
    private SharedPreferences sp;
    private Context ctx;
    private SectionQuestionTableAdapter sqta;
    private int formID;
    private String json;
    private static Button btnAnswers;
    private static EditText edPhone;
    private static EditText edCompanyPhone;
    private static EditText edNoOfOccupants;
    private static EditText edName;
    private static EditText edSurname;
    private  static Button edDateOfBirth;
    private int count = 0;
    private static EditText edEmail;
    private AnswersDTO answersDTO;
    private ArrayList<AnswersDTO> answersDTOs;
    private String answering;
    private SurveyActionTableAdapter surveyActionTableAdapter;
    private int i = 1;
    private SurveyActionDTO surveyActionDTO;
    private final static String STATUS = "Not-Specified";
    private String networkProvider;
    private Priority priority = Priority.HIGH;
    private LinearLayout progressBarLayout;
    private  List<DemorgraphicDTO> lst = new ArrayList<DemorgraphicDTO>();
    private CheckBox smsConcent;
    private ArrayList<String> personalDataList = new ArrayList<>();
    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year_x, month_x, day_x;
    private final int DIALOG_ID = 0;
    private  static String dateOfbirth = "";
    private int sms_consent = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.do_survey);
        setFields();
        ViewGroup v = (LinearLayout)findViewById(R.id.theLayout);
        Log.i("View Count ",String.valueOf(v.getChildCount()));

    }

    private void setFields() {

        ctx = getApplicationContext();
        sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        sqta = new SectionQuestionTableAdapter(ctx);
        formID = sp.getInt("formID1", 0);
        sqta.open();
        cd = new  ConnectionDetector(getApplicationContext());
        sectionQuestionDTOs = sqta.getListSectionQuestionFormID(formID);
        activeLogTableAdapter = new ActiveLogTableAdapter(ctx);
        sata = new SaveAnswerTableAdapter(getApplicationContext());
        fta = new FormCountTableAdapter(getApplicationContext());
        surveyActionTableAdapter  = new SurveyActionTableAdapter(getApplicationContext());
        surveyActionDTO = new SurveyActionDTO();
        cta = new ContactTableAdapter(getApplicationContext());
        dta = new DemorgraphicTableAdapter(getApplicationContext());

        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);
        //progress dialog

        pDialog = new ProgressDialog(do_survey.this);
        pDialog.setMessage("Please wait ...");
        pDialog.setCancelable(false);
        dateOfbirth = STATUS;

        personDTO  = new PersonDTO();

        personDTO.setAge(STATUS);
        personDTO.setGender(STATUS);
        personDTO.setRace(STATUS);
        personDTO.setMarital(STATUS);
        personDTO.setEmployment(STATUS);
        personDTO.setIncome(STATUS);
        personDTO.setName(STATUS);
        personDTO.setCompanyPhone(STATUS);
        personDTO.setDateOfbirth(STATUS);
        personDTO.setNoOfOccupants(STATUS);
        personDTO.setInhousehold(STATUS);
        personDTO.setMunicipality(STATUS);

        getLocation();
      //  saveAnswerTableAdapter = new SaveAnswerTableAdapter(getApplicationContext());
        answersDTOs = new ArrayList<AnswersDTO>();
        sqta.close();


        dta.open();
        lst = dta.getDemoList(sp.getInt("formID1", 0));
        dta.close();

        List<String> mList = new ArrayList<String>();
        for (SectionQuestionDTO c : sectionQuestionDTOs) {
            mList.add(c.getQuestion());
            mList.add(String.valueOf(c.getQuestionID()));
            mList.add(c.getJasonCode());
            json = c.getJasonCode();
            generateForm(json, c.getQuestionID());
            Log.i("MList", " " + mList);
        }

        generateDemo();
        addHeading();
        addButtons();
        //Log.i("MList"," "+mList);

    }

    private void addHeading(){
        tv = new TextView(this);
        tv.setTextSize(24);
        tv.setText(sp.getString("frmName","")+" questionnaire");
        tv.setGravity(Gravity.LEFT | Gravity.CENTER);
        tv.setBackgroundResource(R.color.blue_dark2);
        tv.setTextColor(Color.parseColor("#FFFFFF"));
        tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
        tv.setPadding(20, 10, 0, 20);
        createtv2(tv);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_do_survey, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
        pDialog.hide();
    }

    public void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    /**
     * Controls
     */
    private TextView tv = null;
    private RadioGroup rg = null;
    private Spinner spin = null;
    private CheckBox checkboxItem = null;
    private EditText et = null;
    private DatePicker dp = null;
    private ImageView img = null;

    private LinearLayout linearLayoutContainer;
    public void generateForm(String jsn, final int questionID) {
        /**
         * field_type
         */
        String textbox = "text";
        String textarea = "paragraph";
        String dropdown = "dropdown";
        String checkbox = "checkboxes";
        String radio = "radio";
        String number = "number";
        String email = "email";
        String date = "date";

        QuestionImageTableAdapter qta = new QuestionImageTableAdapter(getApplicationContext());
        qta.open();
        ArrayList<QuestionImage> qImage = qta.getFormList(questionID);
        qta.close();

        //Log.i("qImage ",  qImage.get(0).getLocation());

        ArrayList<String> items = new ArrayList<String>();
        try {
            JSONObject root = new JSONObject(jsn);

            if(root.getString("field_type").equals(textbox) || root.getString("field_type").equals(textarea))
            {
                JSONObject  field_options;

                tv = new TextView(this);
                tv.setTextSize(20);
                tv.setTextColor(Color.parseColor("#FFC107"));
                tv.setText(i++ + ". " + root.getString("label"));

                createElement(tv);

                et = new EditText(this);

                et.setId(questionID);
                if(root.has("field_options")){
                    field_options = root.getJSONObject("field_options");
                    int maxLength = field_options.optInt("maxlength");
                    if(maxLength == 0){
                        maxLength = 600;
                    }
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    et.setFilters(fArray);
                }

               // Log.i("qImage text", "" + questionID + "" + qImage.get(0).getLocation());

                linearLayoutContainer = new LinearLayout(this);
                addImage(qImage, 0,"");

                createElement(et,linearLayoutContainer);

            }

            if(root.getString("field_type").equals(number))
            {
                tv = new TextView(this);
                tv.setText(i++ + ") " + root.getString("label"));
                tv.setTextSize(20);
                tv.setTextColor(Color.parseColor("#FFC107"));

                createElement(tv);

              //  addImage(qImage, 0);

                et = new EditText(this);
                et.setId(questionID);
                et.setInputType(InputType.TYPE_CLASS_NUMBER);
                createElement(et);
            }

            if(root.getString("field_type").equals(date))
            {
                tv = new TextView(this);
                tv.setText(i+++". "+root.getString("label"));
                tv.setTextSize(20);
                tv.setTextColor(Color.parseColor("#FFC107"));
                createElement(tv);

                if(qImage.size() > 0){

                    try{
                        Bitmap imgBit = ImageFile.getImageBitmap(getApplicationContext(), qImage.get(0).getLocation());

                        img = new ImageView(this);

                        img.setImageBitmap(imgBit);

                        createImage(img);
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }

                }

                dp = new DatePicker(this);
                dp.setId(questionID);



                createElement(dp);

             //   createImage(qImage,questionID);
            }

            if(root.getString("field_type").equals(email))
            {
                tv = new TextView(this);
                tv.setTextColor(Color.parseColor("#FFC107"));
                tv.setTextSize(20);
                tv.setText(i++ + ". " + root.getString("label"));
                et = new EditText(this);
                et.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

                createElement(tv);

              //  addImage(qImage,0);

                createElement(et);
             //   createImage(qImage,questionID);
            }

            if(root.getString("field_type").equals(radio)) {
                tv = new TextView(this);
                tv.setTextColor(Color.parseColor("#FFC107"));
                tv.setText(i++ + ". " + root.getString("label"));
                tv.setTextSize(20);
                createElement(tv);

                rg = new RadioGroup(this);
                rg.setOrientation(RadioGroup.VERTICAL);
                //rg.setBackgroundResource(R.drawable.big_card_grey);
                rg.setId(questionID);

                linearLayoutContainer = new LinearLayout(this);

                linearLayoutContainer.setOrientation(LinearLayout.HORIZONTAL);

            }
            if(root.getString("field_type").equals(dropdown)) {
                tv = new TextView(this);
                tv.setText(i++ + ". " + root.getString("label"));
                tv.setTextSize(20);
                tv.setTextColor(Color.parseColor("#FFC107"));
                createElement(tv);

                spin = new Spinner(getApplicationContext());
               // spin.setPopupBackgroundResource(R.drawable.spinner);
                //spin.setBackgroundResource(R.drawable.btn_dropdown_normal);
                spin.setId(questionID);

            }

            if(root.getString("field_type").equals(checkbox)) {

                linearLayoutContainer = new LinearLayout(this);

                tv = new TextView(this);
                tv.setTextSize(20);
                tv.setTextColor(Color.parseColor("#FFC107"));
                tv.setText(i++ + ". " + root.getString("label"));
                createElement(tv);

            }

            JSONArray fieldOptions = new JSONArray("[" + root.getString("field_options") + "]");
            for(int j = 0; j < fieldOptions.length(); j++) {
                JSONObject fieldOptionsObject = fieldOptions.getJSONObject(j);

                if(root.getString("field_type").equals(checkbox) ||  root.getString("field_type").equals(radio) || root.getString("field_type").equals(dropdown)) {
                    try {
                        JSONArray justOptions = new JSONArray(fieldOptionsObject.getString("options"));

                        for (int l = 0; l < justOptions.length(); l++) {
                            JSONObject optionsObject = justOptions.getJSONObject(l);

                            if(root.getString("field_type").equals(radio))
                            {
                                RadioButton newRadio = createElement(new RadioButton(this), rg);
                                newRadio.setText(optionsObject.getString("label"));
                                newRadio.setId(l);
                                newRadio.setTextSize(20);
                                newRadio.setTextColor(Color.parseColor("#FFFFFF"));
                                newRadio.setButtonDrawable(R.drawable.apptheme_btn_radio_holo_light);
                                SharedPreferences.Editor ed = sp.edit();
                                ed.putInt("radioButtonID", questionID);
                                ed.commit();

                                addImage(qImage, l,optionsObject.getString("label"));

                            }

                            if(root.getString("field_type").equals(dropdown))
                            {
                                addSpinnerItem(spin, optionsObject, items, questionID);
                            }

                            if(root.getString("field_type").equals(checkbox))
                            {
                               // linearLayoutContainer = new LinearLayout(this);
                                checkboxItem = new CheckBox(this);
                                checkboxItem.setText(optionsObject.getString("label"));
                                checkboxItem.setId(questionID);
                                checkboxItem.setTextSize(20);
                                checkboxItem.setTextColor(Color.parseColor("#FFFFFF"));
                                checkboxItem.setButtonDrawable(R.drawable.apptheme_btn_checkbox_holo_light);

                                addImage(qImage, l,optionsObject.getString("label"));

                                createElement(checkboxItem,linearLayoutContainer,l);
                            }
                        }//end options for loop (for radio buttons/checkboxes/dropdowns)
                    }
                    catch (Exception ex) {
                        Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
                        TextView qwe = new TextView(this);
                        qwe.setText(ex.toString());
                        LinearLayout myLayout = (LinearLayout)findViewById(R.id.theLayout);
                        myLayout.addView(qwe);

                    }
                }
            }//end fieldOptions for loop
            if(root.getString("field_type").equals(radio))
            {

                createElement(rg,linearLayoutContainer);
                Log.i("qImage radio", "" + questionID +" "+ qImage.size());
            }
            if(root.getString("field_type").equals(dropdown))
            {

                createElement(spin);

                Log.i("qImage spin", "" + questionID +" "+  qImage.size());
            }
            //}//end root for loop
        }catch(Exception ex)
        {
            TextView qwe = new TextView(this);
            qwe.setText(ex.toString());
            LinearLayout myLayout = (LinearLayout)findViewById(R.id.theLayout);
            myLayout.addView(qwe);
        }

    }

    private void addButtons() {

        btnAnswers = new Button(this);
        btnAnswers.setText("Save");
        btnAnswers.setBackgroundResource(R.drawable.btn_normal);
        btnAnswers.setAllCaps(false);
        btnAnswers.setTextColor(Color.parseColor("#FFFFFF"));
        //btnAnswers.setId();
        createElement(btnAnswers);
        btnAnswers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnAnswers.setEnabled(false);
                new TakePrintScreen().execute();
            }
        });

    }

    private ArrayList<EditText> myEditTextList = new ArrayList<>();
    private ArrayList<String> loopPersonalData(){
        List<String> al = new ArrayList<>();
        Set<String> hs = new HashSet<>();
        ArrayList<String> personalDataList = new ArrayList<>();
        LinearLayout myLayout = (LinearLayout) findViewById(R.id.theDemo);

        //ArrayList<EditText> myEditTextList = new ArrayList<EditText>();

        for( int i = 0; i < myLayout.getChildCount(); i++ ){
            if( myLayout.getChildAt( i ) instanceof EditText ) {

                EditText ed = (EditText) myLayout.getChildAt(i);
                myEditTextList.add((EditText) myLayout.getChildAt(i));

                al = new ArrayList<>();
                al.add(""+ed.getHint());
                hs = new HashSet<>();

                hs.addAll(al);
                al.clear();
                al.addAll(hs);
            }
        }

        for(String d:al){

            personalDataList.add(d);
            Log.i("EDITTESTT ", d);
        }

        return personalDataList;
    }

    private ArrayList<Button> myButtonList = new ArrayList<>();

    private ArrayList<String> loopButtonData(){
        List<String> al = new ArrayList<>();
        Set<String> hs = new HashSet<>();
        ArrayList<String> personalDataList = new ArrayList<>();
        LinearLayout myLayout = (LinearLayout) findViewById(R.id.theDemo);

        //ArrayList<EditText> myEditTextList = new ArrayList<EditText>();

        for( int i = 0; i < myLayout.getChildCount(); i++ ){
            if( myLayout.getChildAt( i ) instanceof Button ) {

                Button ed = (Button) myLayout.getChildAt(i);
                myButtonList.add((Button) myLayout.getChildAt(i));

                al = new ArrayList<>();
                al.add(""+ed.getHint());
                hs = new HashSet<>();

                hs.addAll(al);
                al.clear();
                al.addAll(hs);
            }
        }

        for(String d:al){
            personalDataList.add(d);
            Log.i("EDITTESTT ", d);
        }

        return personalDataList;
    }

    private void saveAnswers() {
        LinearLayout root = (LinearLayout) findViewById(R.id.theLayout);
        loopQuestions(root);
       // storeAnswer();
    }

    private void loopQuestions(ViewGroup parent) {
        int activityID = 0;
        int userID = sp.getInt("userID",0);
        int formID = sp.getInt("formID1",0);

        String text = "";

        SaveAnswersDTO saveAnswersDTO = new SaveAnswersDTO();
        ActivityLogDTO activityLogDTO = new ActivityLogDTO();
        saveAnswersDTO.setUserID(userID);
        saveAnswersDTO.setFormID(formID);
        activityLogDTO.setUserID(userID);
        activityLogDTO.setFormID(formID);

       // dialog();

        for (int i = 0; i < parent.getChildCount(); i++) {
            final ProgressDialog pDialog = new ProgressDialog(this);
            pDialog.setMessage("Loading ...");
            pDialog.setTitle("Please wait");
            pDialog.setIcon(R.drawable.warning_blue);
            pDialog.show();

            View child = parent.getChildAt(i);
            if (child instanceof RadioGroup) {
                //Support for RadioGroups
                RadioGroup radio = (RadioGroup) child;
                storeAnswer(radio.getId(), radio.getCheckedRadioButtonId());

                for(int x = 0; x < radio.getChildCount(); x++) {
                   final RadioButton btn = (RadioButton) radio.getChildAt(x);
                    if(btn.getId() == radio.getCheckedRadioButtonId()) {
                        text = btn.getText().toString();
                        activityID = 1;
                        Log.i("Radio ",text);
                        // do something with text
                       // return;
                    }else {
                        activityID = 2;

                    }

                }


                saveAnswersDTO.setAnswer(text.replace(" ", "_").trim());
                saveAnswersDTO.setQuestionID(radio.getId());
                saveAnswersDTO.setPersonID(personID);

                saveAnswersDTO.setActivityID(activityID);

                activityLogDTO.setActivityID(activityID);
                setActivityLog(activityLogDTO);

                setAnswers(saveAnswersDTO);
                Log.i("Activity ID Radio ",String.valueOf(activityID));
            } else if (child instanceof CheckBox) {

                //Support for Checkboxes
                 CheckBox cb = (CheckBox) child;
                if (((CheckBox) child).isChecked()) {
                    String a = ((CheckBox) child).getText().toString();
                    saveAnswersDTO.setAnswer(a.replace(" ","_").trim());
                    activityID = 1;
                    Log.i("Added ",a);
                }
                else{
                    saveAnswersDTO.setAnswer("");
                    activityID = 2;
                    Log.i("Added Nothing","");
                }

                saveAnswersDTO.setQuestionID(cb.getId());
                //answersDTO.setActivityLogID(activityID);
              //  saveAnswersDTO.setPersonID(sp.getInt("personID",0));


                saveAnswersDTO.setActivityID(activityID);
                saveAnswersDTO.setPersonID(personID);


                activityLogDTO.setActivityID(activityID);
                setActivityLog(activityLogDTO);

                setAnswers(saveAnswersDTO);
                Log.i("Activity ID cb ",String.valueOf(activityID));
                //end of checkbox
            } else
            if (child instanceof EditText) {
                //Support for EditText
                EditText tx = (EditText) child;
                String a =  tx.getText().toString();

                if (a.equals("")) {
                    activityID = 2;
                    Log.i("Text nothing",a);
                }else{
                    activityID = 1;
                    Log.i("Text has some ",a);
                }
                saveAnswersDTO.setAnswer(a.replace(" ", "_").trim());
                saveAnswersDTO.setQuestionID(tx.getId());
                saveAnswersDTO.setPersonID(personID);

                saveAnswersDTO.setActivityID(activityID);

                activityLogDTO.setActivityID(activityID);
                setActivityLog(activityLogDTO);

                setAnswers(saveAnswersDTO);
                Log.i("Activity ID Text ",String.valueOf(activityID));

            } else
            if (child instanceof ToggleButton) {
                //Support for ToggleButton
                ToggleButton tb = (ToggleButton) child;
                Log.w("ANDROID DYNAMIC VIEWS:", "Toggle: " + tb.getText());
            } else
            if (child instanceof DatePicker) {

                DatePicker dp = (DatePicker) child;
                String date = Integer.toString(dp.getDayOfMonth()) + "/" +Integer.toString(dp.getMonth() + 1) + "/" + Integer.toString(dp.getYear());
                //  Log.w("ANDROID DYNAMIC VIEWS:", "EdiText: " + tx.getText());
                if (!date.equals("")) {
                    activityID = 1;
                } else {
                    activityID = 2;
                }

                saveAnswersDTO.setAnswer(date);
                saveAnswersDTO.setQuestionID(dp.getId());
                saveAnswersDTO.setPersonID(personID);

                saveAnswersDTO.setActivityID(activityID);

                activityLogDTO.setActivityID(activityID);
                setActivityLog(activityLogDTO);

                setAnswers(saveAnswersDTO);
                Log.i("Activity ID date ",String.valueOf(activityID));


            }else
            if(child instanceof Spinner){
                    Spinner sp1 = (Spinner) child;
                    String selected = sp1.getSelectedItem().toString();
                    activityID = 1;

                    saveAnswersDTO.setAnswer(selected.toString().replace(" ", "_").trim());
                    saveAnswersDTO.setQuestionID(sp1.getId());
                    saveAnswersDTO.setPersonID(personID);

                    saveAnswersDTO.setActivityID(activityID);

                    activityLogDTO.setActivityID(activityID);
                    setActivityLog(activityLogDTO);

                    setAnswers(saveAnswersDTO);

                Log.i("Activity ID spinner ",String.valueOf(activityID));
            }

            if (child instanceof ViewGroup) {
                //Nested Q&A
                ViewGroup group = (ViewGroup) child;
                loopQuestions(group);

            }
            pDialog.dismiss();
        }

      //  dialog.dismiss();
       //Toast.makeText(this, "List of my Answers "+ answersDTOs,Toast.LENGTH_SHORT).show();
    }

    private void storeAnswer(int question, int answer) {
        Log.i("ANDROID DYNAMIC VIEWS:", "Question: " + String.valueOf(question) + " * " + "Answer: " + String.valueOf(answer));

        //Toast toast = Toast.makeText(this, String.valueOf(question) + " * " + "Answer: " + String.valueOf(answer), Toast.LENGTH_LONG);
        //toast.setGravity(Gravity.TOP, 25, 400);
        //toast.show();
    }

    private void setActivityLog(ActivityLogDTO a){
        activeLogTableAdapter.open();

        activeLogTableAdapter.insertActivityLog(a);

    }

    private void setAnswers(SaveAnswersDTO a) {
        Log.i("Answers you", a.getAnswer());
        sata.open();

        sata.insertSaveAnswers(a);
    }

    @Override
    protected void onStop () {
        super.onStop();
        sata.close();
        activeLogTableAdapter.close();
    }
    @Override
    protected void onStart () {
        super.onStart();
        activeLogTableAdapter.open();
        sata.open();
    }

    private void generatePersonInfo(LinearLayout layout){
     LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
     LinearLayout.LayoutParams t = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,60);
     p.setMargins(30, 5, 24, 5);
     t.setMargins(0, 0, 0, 20);
        dta.open();
        lst = dta.getDemoList(sp.getInt("formID1", 0));
        dta.close();



        if(lst.size() >  0){
            tv =  new TextView(this);
            tv.setText("Personal Information");
            tv.setBackgroundResource(R.color.blue_dark2);
            tv.setTextColor(Color.parseColor("#FFFFFF"));
            tv.setTextSize(24);
            tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
            tv.setGravity(Gravity.LEFT | Gravity.CENTER);
            tv.setPadding(20, 0, 0, 0);
            layout.addView(tv, t);

        }

     for(DemorgraphicDTO d:lst){
         Log.i("vesta "," "+d.getCompanyco());
         if(!d.getPersonalData().equals("")){

             if(d.getPersonalData().equalsIgnoreCase("Phone Number")){
                 tv =  new TextView(this);
                 tv.setText(d.getPersonalData());
                 tv.setId(d.getDemorgraphicID());
                 tv.setBackgroundResource(R.color.headers);
                 tv.setTextColor(Color.parseColor("#FFFFFF"));
                 tv.setTextSize(20);
                 tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
                 tv.setGravity(Gravity.LEFT | Gravity.CENTER);
                 tv.setPadding(20, 0, 0, 0);
                 layout.addView(tv, t);

                 EditText edName = new EditText(this);
                 edName.setText("");
                 edName.setTextColor(Color.parseColor("#ffff"));
                 setIDs(edName, d.getPersonalData());
                 edName.setTextSize(22);
                 edName.setHint(d.getPersonalData());
                 layout.addView(edName, t);

                 smsConcent = new CheckBox(this);
                 smsConcent.setText("Do you want to recieve sms for this survey?");
                 smsConcent.setTextSize(20);
                 smsConcent.setId(R.id.SMSConcent);
                 smsConcent.setTextColor(Color.parseColor("#FFFFFF"));
                 smsConcent.setButtonDrawable(R.drawable.apptheme_btn_checkbox_holo_light);
                 layout.addView(smsConcent, t);

                 smsConcent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                 {
                     @Override
                     public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                     {
                         if ( isChecked )
                         {
                             sms_consent = 1;


                         }else{
                             sms_consent = 0;
                         }

                     }
                 });


             }else if(d.getPersonalData().equalsIgnoreCase("Date Of Birth")) {

                 tv =  new TextView(this);
                 tv.setText(d.getPersonalData());
                 tv.setId(d.getDemorgraphicID());
                 tv.setBackgroundResource(R.color.headers);
                 tv.setTextColor(Color.parseColor("#FFFFFF"));
                 tv.setTextSize(20);
                 tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
                 tv.setGravity(Gravity.LEFT | Gravity.CENTER);
                 tv.setPadding(20, 0, 0, 0);
                 layout.addView(tv, t);

                 edDateOfBirth = new Button(this);
                 edDateOfBirth.setText("Set Date");

                 edDateOfBirth.setTextColor(Color.BLACK);
//                 setIDs(edDateOfBirth, d.getPersonalData());
                 edDateOfBirth.setTextSize(22);
                 edDateOfBirth.setHint(d.getPersonalData());
                 layout.addView(edDateOfBirth, t);

                 edDateOfBirth.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         //showDialog(DIALOG_ID);
                         showDatePickerDialog();
                     }
                 });

             }else{
                 tv =  new TextView(this);
                 tv.setText(d.getPersonalData());
                 tv.setId(d.getDemorgraphicID());
                 tv.setBackgroundResource(R.color.headers);
                 tv.setTextColor(Color.parseColor("#FFFFFF"));
                 tv.setTextSize(20);
                 tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
                 tv.setGravity(Gravity.LEFT | Gravity.CENTER);
                 tv.setPadding(20, 0, 0, 0);
                 layout.addView(tv, t);

                 EditText edName = new EditText(this);
                 edName.setText("");
                 setIDs(edName, d.getPersonalData());
                 edName.setHint(d.getPersonalData());
                 edName.setTextColor(Color.parseColor("#FFFFFF"));
                 edName.setTextSize(22);
                 layout.addView(edName, t);

             }

         }
     }

//     tv =  new TextView(this);
//     tv.setText("Personal Information");
//     tv.setBackgroundResource(R.color.blue_dark2);
//     tv.setTextColor(Color.parseColor("#FFFFFF"));
//     tv.setTextSize(24);
//     tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
//     tv.setGravity(Gravity.LEFT | Gravity.CENTER);
//     tv.setPadding(20, 0, 0, 0);
//     layout.addView(tv, t);
//
//        TextView tvName = new TextView(this);
//        tvName.setText("Name:");
//        tvName.setTextColor(Color.parseColor("#FFC107"));
//        tvName.setTextSize(22);
//
//        layout.addView(tvName, p);
//
//        edName = new EditText(this);
//        edName.setText("");
//        edName.setHint("Name:");
//        edName.setId(R.id.pPhone);
//        edName.setInputType(InputType.TYPE_CLASS_PHONE);
//        edName.setTextSize(20);
//        createElement1(edName);
//
//        TextView tvSurname = new TextView(this);
//        tvSurname.setText("Surname:");
//        tvSurname.setTextColor(Color.parseColor("#FFC107"));
//        tvSurname.setTextSize(22);
//
//        layout.addView(tvSurname, p);
//
//        edSurname = new EditText(this);
//        edSurname.setText("");
//        edSurname.setHint("Surname:");
//        edSurname.setId(R.id.pPhone);
//        edSurname.setInputType(InputType.TYPE_CLASS_PHONE);
//        edSurname.setTextSize(20);
//        createElement1(edSurname);
//
//     TextView tvdateOfirth= new TextView(this);
//     tvdateOfirth.setText("Date Of Birth:");
//     tvdateOfirth.setTextColor(Color.parseColor("#FFC107"));
//     tvdateOfirth.setTextSize(22);
//
//     layout.addView(tvdateOfirth, p);
//
//     edDateOfBirth = new EditText(this);
//     edDateOfBirth.setText("");
//     edDateOfBirth.setHint("Date Of Birth:");
//     edDateOfBirth.setId(R.id.pPhone);
//     edDateOfBirth.setInputType(InputType.TYPE_CLASS_PHONE);
//     edDateOfBirth.setTextSize(20);
//     createElement1(edDateOfBirth);
//
//     TextView tvPhone = new TextView(this);
//     tvPhone.setText("Cellphone Number:");
//     tvPhone.setTextColor(Color.parseColor("#FFC107"));
//     tvPhone.setTextSize(22);
//
//     layout.addView(tvPhone, p);
//
//     edPhone = new EditText(this);
//     edPhone.setText("");
//     edPhone.setHint("Cell Phone Number:");
//     edPhone.setId(R.id.pPhone);
//     edPhone.setInputType(InputType.TYPE_CLASS_PHONE);
//     edPhone.setTextSize(20);
//     createElement1(edPhone);
//
//     smsConcent = new CheckBox(this);
//
//     smsConcent.setText("Do you want to recieve sms for this survey?");
//
//     smsConcent.setTextSize(20);
//
//     smsConcent.setTextColor(Color.parseColor("#FFFFFF"));
//
//     smsConcent.setButtonDrawable(R.drawable.apptheme_btn_checkbox_holo_light);
//
//     createElement1(smsConcent);
//
//     TextView tvCompanyPhone = new TextView(this);
//     tvCompanyPhone.setText("Employer's Contact:");
//     tvCompanyPhone.setTextColor(Color.parseColor("#FFC107"));
//     tvCompanyPhone.setTextSize(22);
//
//     layout.addView(tvCompanyPhone, p);
//
//     edCompanyPhone = new EditText(this);
//     edCompanyPhone.setText("");
//     edCompanyPhone.setHint("Company's Phone Number:");
//     edCompanyPhone.setId(R.id.pPhone);
//     edCompanyPhone.setInputType(InputType.TYPE_CLASS_PHONE);
//     edCompanyPhone.setTextSize(20);
//     createElement1(edCompanyPhone);
//
//        TextView tvNoOfOccupants = new TextView(this);
//        tvNoOfOccupants.setText("Number of Occupants:");
//        tvNoOfOccupants.setTextColor(Color.parseColor("#FFC107"));
//        tvNoOfOccupants.setTextSize(22);
//
//        layout.addView(tvNoOfOccupants, p);
//
//        edNoOfOccupants = new EditText(this);
//        edNoOfOccupants.setText("");
//        edNoOfOccupants.setHint("Number of Occupants");
//        edNoOfOccupants.setId(R.id.pPhone);
//        edNoOfOccupants.setInputType(InputType.TYPE_CLASS_PHONE);
//        edNoOfOccupants.setTextSize(20);
//        createElement1(edNoOfOccupants);
//
//
//     TextView tvEmail = new TextView(this);
//     tvEmail.setText("Email Address:");
//     tvEmail.setTextColor(Color.parseColor("#FFC107"));
//     tvEmail.setTextSize(22);
//     layout.addView(tvEmail, p);
//
//     edEmail = new EditText(this);
//     edEmail.setHint("Email Address:");
//     edEmail.setId(R.id.pEmail);
//     edEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
//     edEmail.setTextSize(20);
//     createElement1(edEmail);

     TextView tvNSP = new TextView(this);
     tvNSP.setTextColor(Color.parseColor("#FFC107"));
     tvNSP.setText("Network Service Provider.");
     tvNSP.setTextSize(22);
     layout.addView(tvNSP, p);
     RadioGroup radioGroupProvider = new RadioGroup(this);
    // radioGroupProvider.setId(View.generateViewId());
     createElement1(radioGroupProvider);
     String []serviceProvider = {"Vodacom","MTN","Cell C","8ta"};

     for(int i =0; i<serviceProvider.length;i++)
     {
         final RadioButton radioButtonView = new RadioButton(this);
         radioButtonView.setText(serviceProvider[i]);
         radioButtonView.setTextSize(18);
         radioButtonView.setTextColor(Color.parseColor("#FFFFFF"));
         radioButtonView.setButtonDrawable(R.drawable.apptheme_btn_radio_holo_light);
         radioGroupProvider.addView(radioButtonView, p);
         radioButtonView.setChecked(true);

         radioButtonView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
               // Toast.makeText(getApplicationContext(),"Gender :"+radioButtonView.getText().toString(),Toast.LENGTH_SHORT).show();
                 networkProvider = radioButtonView.getText().toString();
             }
         });
     }

        radioGroupProvider.setVisibility(View.GONE);
        tvNSP.setVisibility(View.GONE);
 }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {


            String m = "";
            String d = "";

                if(month < 10){
                    m ="0"+month;
                }
                if(day < 10){
                    d ="0"+day;
                }

                String str = year + "-" + m + "-" + d;
                // parse the String "29/07/2013" to a java.util.Date object

                // format the java.util.Date object to the desired format
//                Date date = new Date(str);
//                String formattedDate = new SimpleDateFormat("yyyy-MM-dd ").format(date);
//                dateOfbirth = formattedDate;
//                Log.i("dateOfbirth ",dateOfbirth);


                dateOfbirth = str;
                edDateOfBirth.setText(str);
        }
    }
    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void generateDemo(){

        String age = "";
        String gender = "";
        String race = "";
        String employment = "";
        String marital = "";

    if(lst.size() > 0){

        ArrayList<String> inhouseholds = new ArrayList<>();
        inhouseholds.add("Not-Specified");
        ArrayList<String> municipality = new ArrayList<>();
        municipality.add("Not-Specified");
        LinearLayout.LayoutParams spinner = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams t = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        p.setMargins(24, 10, 24, 14);
        t.setMargins(35, 40, 24, 24);
        LinearLayout layout = (LinearLayout) findViewById(R.id.theDemo);
        generatePersonInfo(layout);

        layout.setVisibility(View.GONE);

        TextView tvDermographic = new TextView(this);
        tvDermographic.setTextColor(Color.parseColor("#FFFFFF"));
        // tv.setBackgroundResource(R.color.blue_dark2);
        tvDermographic.setText("Dermographic data");
        tvDermographic.setTextSize(24);
        tvDermographic.setTypeface(tv.getTypeface(), Typeface.BOLD);
        tvDermographic.setGravity(Gravity.LEFT | Gravity.CENTER);
        tvDermographic.setBackgroundResource(R.color.blue_dark2);
        tvDermographic.setPadding(20, 10, 0, 20);
        createtv2(tvDermographic);

        RadioGroup radioGroupGender = new RadioGroup(this);
        // radioGroupGender.setId(View.generateViewId());
        RadioGroup radioGroupRace = new RadioGroup(this);
        //  radioGroupRace.setId(View.generateViewId());
        RadioGroup radioGroupAge = new RadioGroup(this);
        //radioGroupAge.setId(View.generateViewId());
        RadioGroup radioGroupMarital = new RadioGroup(this);
        // radioGroupMarital.setId(View.generateViewId());
        RadioGroup radioGroupEmployment = new RadioGroup(this);
        //radioGroupEmployment.setId(View.generateViewId());
        RadioGroup radioGroupIncome = new RadioGroup(this);


        Spinner spinnerInhouseHold = new Spinner(this);
        spinnerInhouseHold.setPadding(20, 10, 0, 20);

        Spinner spinnerMunicipality = new Spinner(this);
        spinnerMunicipality.setPadding(20, 10, 0, 20);

        TextView tvGender = new TextView(this);
        tvGender.setTextColor(Color.parseColor("#FFC107"));
      //  tvGender.setText("Gender:");
        tvGender.setTextSize(22);
        layout.addView(tvGender, p);
        layout.addView(radioGroupGender, p);

        TextView tvAge = new TextView(this);
        tvAge.setTextColor(Color.parseColor("#FFC107"));
       // tvAge.setText("Age:");
        tvAge.setTextSize(22);
        layout.addView(tvAge, p);
        layout.addView(radioGroupAge, p);

        TextView tvRace = new TextView(this);
        tvRace.setTextColor(Color.parseColor("#FFC107"));
        tvRace.setText("");
        tvRace.setTextSize(22);
        layout.addView(tvRace, p);
        layout.addView(radioGroupRace, p);

        TextView  tvMarital = new TextView(this);
        tvMarital.setTextColor(Color.parseColor("#FFC107"));
        tvMarital.setText("");
        tvMarital.setTextSize(22);
        layout.addView(tvMarital, p);
        layout.addView(radioGroupMarital, p);

        TextView tvEmployment = new TextView(this);
        tvEmployment.setTextColor(Color.parseColor("#FFC107"));
        tvEmployment.setText("");
        tvEmployment.setTextSize(22);
        layout.addView(tvEmployment, p);
        layout.addView(radioGroupEmployment, p);
        //Toast.makeText(getApplicationContext(),"Length "+lst.size(),Toast.)

        TextView tvIncome = new TextView(this);
        tvIncome.setTextColor(Color.parseColor("#FFC107"));
        tvIncome.setText("Income Category:");
        tvIncome.setTextSize(22);
        layout.addView(tvIncome, p);
        layout.addView(radioGroupIncome, p);

        TextView tvInhousehold = new TextView(this);
        tvInhousehold.setTextColor(Color.parseColor("#FFC107"));
        tvInhousehold.setText("Number of Inhousehold");
        tvInhousehold.setTextSize(22);
        layout.addView(tvInhousehold, p);
        layout.addView(spinnerInhouseHold, spinner);

        TextView tvMunicipality = new TextView(this);
        tvMunicipality.setTextColor(Color.parseColor("#FFC107"));
        tvMunicipality.setText("Municipality");
        tvMunicipality.setTextSize(22);
        layout.addView(tvMunicipality, p);
        layout.addView(spinnerMunicipality, spinner);

        layout.setVisibility(View.VISIBLE);

        for(DemorgraphicDTO d:lst){

            Log.i("Employment ", d.getEmployment());
            Log.i("Marital ", d.getMarital());
            if(!d.getGender().equals("")){

                tvGender.setText("Gender:");
                final RadioButton radioButtonViewGender = new RadioButton(this);
                radioButtonViewGender.setText(d.getGender());
                radioButtonViewGender.setTextSize(20);
                // radioButtonViewGender.setId(View.generateViewId());
                radioButtonViewGender.setTextColor(Color.parseColor("#FFFFFF"));
                radioButtonViewGender.setButtonDrawable(R.drawable.apptheme_btn_radio_holo_light);
                radioGroupGender.addView(radioButtonViewGender, p);
                radioButtonViewGender.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getApplicationContext(),"Gender :"+radioButtonViewGender.getText().toString(),Toast.LENGTH_SHORT).show();
                        String gender = radioButtonViewGender.getText().toString();
                        personDTO.setGender(gender);
                    }
                });
            }
            if(!d.getAge().equals("")){
                if(Character.isDigit(d.getAge().charAt(0))){

                    tvAge.setText("Age:");
                    final RadioButton radioButtonViewAge = new RadioButton(this);
                    radioButtonViewAge.setText(d.getAge());
                    radioButtonViewAge.setTextSize(20);
                    // radioButtonViewAge.setId(View.generateViewId());
                    radioButtonViewAge.setTextColor(Color.parseColor("#FFFFFF"));
                    radioButtonViewAge.setButtonDrawable(R.drawable.apptheme_btn_radio_holo_light);
                    radioGroupAge.addView(radioButtonViewAge, p);
                    radioButtonViewAge.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(getApplicationContext(),"Age :"+radioButtonViewAge.getText().toString(),Toast.LENGTH_SHORT).show();
                            String age = radioButtonViewAge.getText().toString();
                            personDTO.setAge(age);
                        }
                    });
                }

            }

            if(!d.getRace().equals("")){
                tvRace.setText("Ethnicity/Race:");
                final RadioButton radioButtonViewRace = new RadioButton(this);
                radioButtonViewRace.setText(d.getRace());
                radioButtonViewRace.setTextSize(20);
                // radioButtonViewRace.setId(View.generateViewId());
                radioButtonViewRace.setTextColor(Color.parseColor("#FFFFFF"));
                radioButtonViewRace.setButtonDrawable(R.drawable.apptheme_btn_radio_holo_light);
                radioGroupRace.addView(radioButtonViewRace, p);
                radioButtonViewRace.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Toast.makeText(getApplicationContext(),"Race :"+radioButtonViewRace.getText().toString(),Toast.LENGTH_SHORT).show();
                        String race = radioButtonViewRace.getText().toString();
                        personDTO.setRace(race);

                    }
                });
            }
            if(!d.getMarital().equals("")){
                tvMarital.setText("Marital Status:");
                final RadioButton radioButtonMarital = new RadioButton(this);
                radioButtonMarital.setText(d.getMarital());
                radioButtonMarital.setTextSize(20);
                // radioButtonMarital.setId(View.generateViewId());
                radioButtonMarital.setTextColor(Color.parseColor("#FFFFFF"));
                radioButtonMarital.setButtonDrawable(R.drawable.apptheme_btn_radio_holo_light);
                radioGroupMarital.addView(radioButtonMarital, p);
                radioButtonMarital.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Toast.makeText(getApplicationContext(),"Marital status :"+radioButtonMarital.getText().toString(),Toast.LENGTH_SHORT).show();
                        String maritalStatus = radioButtonMarital.getText().toString();
                        personDTO.setMarital(maritalStatus);
                    }
                });
            }
            if(!d.getEmployment().equals("")){
                Log.i("eloi ",d.getEmployment());
                tvEmployment.setText("Employment Status:");
                final RadioButton radioButtonViewEmployment = new RadioButton(this);
                radioButtonViewEmployment.setText(d.getEmployment());
                radioButtonViewEmployment.setTextSize(20);
                //radioButtonViewEmployment.setId(View.generateViewId());
                radioButtonViewEmployment.setTextColor(Color.parseColor("#FFFFFF"));
                radioButtonViewEmployment.setButtonDrawable(R.drawable.apptheme_btn_radio_holo_light);
                radioGroupEmployment.addView(radioButtonViewEmployment, p);
                radioButtonViewEmployment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getApplicationContext(),"Employment status :"+radioButtonViewEmployment.getText().toString(),Toast.LENGTH_SHORT).show();
                        String employment = radioButtonViewEmployment.getText().toString();
                        personDTO.setEmployment(employment);
                    }
                });
            }

            if(!d.getIncome().equals("")){

                final RadioButton radioButtonViewIncome = new RadioButton(this);
                radioButtonViewIncome.setText(d.getIncome());
                radioButtonViewIncome.setTextSize(20);
                //radioButtonViewEmployment.setId(View.generateViewId());
                radioButtonViewIncome.setTextColor(Color.parseColor("#FFFFFF"));
                radioButtonViewIncome.setButtonDrawable(R.drawable.apptheme_btn_radio_holo_light);
                radioGroupIncome.addView(radioButtonViewIncome, p);
                radioButtonViewIncome.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(getApplicationContext(),"Employment status :"+radioButtonViewEmployment.getText().toString(),Toast.LENGTH_SHORT).show();
                        String income = radioButtonViewIncome.getText().toString();
                        personDTO.setIncome(income);
                    }
                });
            }

            if(!d.getInhousehold().equals("")){

                inhouseholds.add(d.getInhousehold());

                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                        (this, android.R.layout.simple_spinner_item,
                                inhouseholds); //selected i

                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spinnerInhouseHold.setAdapter(spinnerArrayAdapter);

                spinnerInhouseHold.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String inhouse = spinnerInhouseHold.getSelectedItem().toString();
                        personDTO.setInhousehold(inhouse);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }


            if(!d.getMunicipality().equals("")){

                municipality.add(d.getMunicipality());

                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                        (this, android.R.layout.simple_spinner_item,
                                municipality); //selected i

                spinnerMunicipality.setAdapter(spinnerArrayAdapter);

                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spinnerMunicipality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String municipality = spinnerMunicipality.getSelectedItem().toString();
                        personDTO.setMunicipality(municipality);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }


        }


        if(tvAge.getText().toString().equals("")){

            tvAge.setVisibility(View.GONE);
            radioGroupAge.setVisibility(View.GONE);
        }
        if(tvGender.getText().toString().equals("")){

            tvGender.setVisibility(View.GONE);
            radioGroupGender.setVisibility(View.GONE);
        }
        if(tvMarital.getText().toString().equals("")){

            tvMarital.setVisibility(View.GONE);
            radioGroupMarital.setVisibility(View.GONE);
        }
        if(tvEmployment.getText().toString().equals("")){

            tvEmployment.setVisibility(View.GONE);
            radioGroupEmployment.setVisibility(View.GONE);
        }
        if(tvRace.getText().toString().equals("")){

            tvRace.setVisibility(View.GONE);
            radioGroupRace.setVisibility(View.GONE);
        }

        if(tvIncome.getText().toString().equals("")){

            tvIncome.setVisibility(View.GONE);
            radioGroupIncome.setVisibility(View.GONE);
        }

        if(tvInhousehold.getText().toString().equals("")){

            tvInhousehold.setVisibility(View.GONE);
            spinnerInhouseHold.setVisibility(View.GONE);
        }

        if(tv.getText().toString().equals("")){

            tvMunicipality.setVisibility(View.GONE);
            spinnerMunicipality.setVisibility(View.GONE);
        }
    }
 }

    private void saveDemographicData(){

    networkProvider = "4";
    switch (networkProvider){
        case "Vodacom":
            networkProvider = "1";
            break;
        case "MTN":
            networkProvider = "2";
            break;
        case "Cell C":
            networkProvider = "3";
            break;
        case "8ta":
            networkProvider = "4";
            break;
        default:
            networkProvider = "1";

    }
    int serviceProvideID = Integer.parseInt(networkProvider);
//  String email = edEmail.getText().toString();
    String age = personDTO.getAge().replace(" ", "_");
    String race = personDTO.getRace();
    String gender = personDTO.getGender().replace(" ", "_");
    String marital = personDTO.getMarital().replace(" ", "_");
    String employment = personDTO.getEmployment().replace(" ", "_");

    String inhouseholds = personDTO.getInhousehold().replace(" ", "_");
    String municipality = personDTO.getMunicipality().replace(" ", "_");

    String income = "";
        try {
            income = URLEncoder.encode(personDTO.getIncome(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String noOfOccupants = STATUS;
        String companyContact = STATUS;
        String name=STATUS;
        String surname = STATUS;
        String mEmailAddress = STATUS;
        String phoneNo = STATUS;
        String address = STATUS;
        int userID = sp.getInt("userID", 0);
        loopPersonalData();
//        loopButtonData();
//        for(Button s:myButtonList) {
//            switch (s.getHint().toString()) {
//                case "Date Of Birth":
//                    dateOfbirth = s.getText().toString();
//                    break;
//            }
//        }

    for(EditText s:myEditTextList){
        switch (s.getHint().toString()){
            case "Name":
                name = s.getText().toString();
                break;
            case "Surname":
                surname = s.getText().toString();
                break;
            case "Address":
                address = s.getText().toString().replace(" ","_");
                break;
            case "Number Of Occupants":
                noOfOccupants = s.getText().toString();
                break;
            case "Phone":
                phoneNo = s.getText().toString();
               // String phone = null;
                if( phoneNo.length() > 0) {
                    if (!phoneNo.substring(1, phoneNo.length()).equals("+") && !phoneNo.substring(1, phoneNo.length()).equals("2") ) {
                        phoneNo = phoneNo.substring(1, phoneNo.length());
                        phoneNo = "27" + phoneNo;
                        Log.i("phone ", phoneNo);

                    }else
                    if (phoneNo.substring(1, phoneNo.length()).equals("+")){
                        phoneNo = phoneNo.toString().substring(1, phoneNo.length());
                    }
                }

                break;
            case "Email Address":
                mEmailAddress = s.getText().toString();
                break;
            case "Company's Contact":
                companyContact = s.getText().toString();
                break;
        }
    }


    PersonDTO p = new PersonDTO (
            userID,
            serviceProvideID,
            phoneNo,
            mEmailAddress,
            age,
            race,
            gender,
            marital,
            employment,
            sp.getInt("formID1",0),
            sms_consent,
            String.valueOf(generateUID()),
            dateOfbirth,
            income,
            noOfOccupants,
            companyContact,
            name,
            surname,
            address,
            inhouseholds,
            municipality);

    PersonTableAdapter pta = new PersonTableAdapter(getApplicationContext());
    pta.open();
    pta.insertPerson(p);
    pta.close();

        if(phoneNo.length() > 0) {
            ContactDTO contactDTO = new ContactDTO(sp.getInt("userID", 0), phoneNo, formID);
            cta.open();
            cta.insertContact(contactDTO);
            cta.close();
        }

}

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void setPersonID(){

    List<PersonDTO> p = new ArrayList<>();
    PersonTableAdapter pta = new PersonTableAdapter(getApplicationContext());
    pta.open();
    p = pta.getPersonList();
    pta.close();

    personID = p.size() + 1;
        Log.i("p.size ", "" + p.size());
        Log.i("perID ", String.valueOf(p.size() + 1));
}

    public void createElement(Button element) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.bottomMargin = 20;
        layoutParams.weight = 1.0f;
        layoutParams.gravity = Gravity. CENTER;

        LinearLayout layout = (LinearLayout) findViewById(R.id.theLayout);
        layout.addView(element, layoutParams);
    }

    public void createElement(EditText element,LinearLayout linearLayoutImages) {

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(24, 24, 24, 24);

        LinearLayout.LayoutParams layoutParamsForImages = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(48, 24, 24, 24);

        LinearLayout layout = (LinearLayout) findViewById(R.id.theLayout);

        layoutParamsForImages.setMargins(40,10,0,0);

        linearLayoutImages.setPadding(3,3,3,3);

        layout.addView(linearLayoutImages, layoutParamsForImages);

        layout.addView(element, layoutParams);
    }

    public void createElement1(EditText element) {

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(24, 24, 24, 24);
        LinearLayout layout = (LinearLayout) findViewById(R.id.theDemo);
        layout.addView(element, layoutParams);
    }

    public void createElement1(CheckBox element) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(24,10, 10, 10);
        LinearLayout layout = (LinearLayout) findViewById(R.id.theDemo);
        layout.addView(element, layoutParams);
    }

    public void createElement(TextView element) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(40, 24, 160, 30);
        LinearLayout layout = (LinearLayout) findViewById(R.id.theLayout);
        layout.addView(element);
    }

    public void createImageView(ImageView imageView) {

        LinearLayout.LayoutParams layoutParams = new   LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(24, 30, 24, 40);
        LinearLayout layout = (LinearLayout) findViewById(R.id.theLayout);
        layout.addView(imageView);


    }

    public void createtv2(TextView element) {

        LinearLayout.LayoutParams layoutParams = new   LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,90);
        layoutParams.setMargins(24, 30, 24, 40);
        LinearLayout layout = (LinearLayout) findViewById(R.id.theDemo);
        layout.addView(element);
    }

    public RadioButton createElement(final RadioButton element, RadioGroup group) {


        RadioGroup.LayoutParams params
                = new RadioGroup.LayoutParams(getApplicationContext(), null);

        params.setMargins(10, 10, 0, 0);

        group.addView(element,params);
        return element;
    }

    public void createElement(RadioGroup element,LinearLayout linearLayoutImages) {

        linearLayoutImages.setOrientation(LinearLayout.HORIZONTAL);

      //  element.setBackgroundResource(R.drawable.square_red);

        LinearLayout linearLayout = new LinearLayout(this);

        linearLayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(24, 24, 24, 24);

        LinearLayout.LayoutParams layoutParamsForInnerLayouts = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        linearLayout.addView(linearLayoutImages, layoutParamsForInnerLayouts);

        linearLayout.addView(element, layoutParamsForInnerLayouts);

        LinearLayout layout = (LinearLayout) findViewById(R.id.theLayout);

        layout.addView(linearLayout,layoutParams);


    }

    public void createElement1(RadioGroup element) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(24, 24, 24, 24);
        LinearLayout layout = (LinearLayout) findViewById(R.id.theDemo);
        layout.addView(element, layoutParams);
    }

    public void createElement(CheckBox element,LinearLayout linearLayoutImages,int index) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(24, 24, 24, 24);
        LinearLayout layout = (LinearLayout) findViewById(R.id.theLayout);

        LinearLayout linearLayout = new LinearLayout(this);

        LinearLayout.LayoutParams layoutParamsForImags = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        //  linearLayout.setLayoutParams(layoutParams);

        layoutParams.setMargins(24, 24, 24, 24);

        LinearLayout.LayoutParams layoutParamsForInnerLayouts = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        if(index == 0 ){

            layout.addView(linearLayoutImages,layoutParams);
        }

        layout.addView(element,layoutParams);

      //  layout.addView(linearLayout);
    }

    public void createElement(Spinner element) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(24, 24, 24, 24);
        LinearLayout layout = (LinearLayout) findViewById(R.id.theLayout);
        layout.addView(element, layoutParams);
    }

    public void createElement(DatePicker element) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT
                , LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(24, 24, 24, 24);
        LinearLayout layout = (LinearLayout) findViewById(R.id.theLayout);
        layout.addView(element, layoutParams);
    }

    public void createImage(ImageView imageView) {

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT
                ,  LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(24, 24, 24, 24);

        linearLayoutContainer.addView(imageView, layoutParams);

    }

    public void addSpinnerItem(Spinner element, JSONObject object, ArrayList<String> items, Integer questionID) {
        try {
            items.add(object.getString("label"));
        } catch (Exception ex) {
           // Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
        }
        String[] itemsString = new String[items.size()];
        itemsString = items.toArray(itemsString);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, itemsString);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        element.setAdapter(spinnerArrayAdapter);
        element.setId(questionID);
    }

    private void getLocation() {

        String Address  = "";
        GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {

            double end_lat = gpsTracker.latitude;

            double end_long =gpsTracker.longitude;

            String end_place = gpsTracker.getLocality(this);

            int userID = sp.getInt("userID",0);

            int formID = sp.getInt("formID1", 0);

           // String startTime = sp.getString("datetime",0);
            Date d = new Date();

            String cityName = null;
            Geocoder gcd = new Geocoder(getBaseContext(),
                    Locale.getDefault());
            List<Address> addresses = new ArrayList<>();
            try {
                addresses = gcd.getFromLocation(end_lat, end_long, 1);
                if (addresses.size() > 0)

                System.out.println(addresses.get(0).getLocality());
                Address = addresses.get(0).getLocality()+" "+ addresses.get(0).getPostalCode();
                Log.i("GPS Address ", Address +" "+  addresses.get(0).getLocality()+" "+ addresses.get(0).getPostalCode());
              //  cityName = addresses.get(0).getLocality();
            } catch (IOException e) {
                e.printStackTrace();
            }


            String endTime = getCurrentTimeLong();
            String start_lat = sp.getString("Latitude", "");
            String start_long = sp.getString("Longitude", "");
            String place = sp.getString("place","");

            surveyActionDTO.setId_form_fk(formID);
            surveyActionDTO.setId_user_fk(userID);
            surveyActionDTO.setStart_city(sp.getString("place",""));
            surveyActionDTO.setEnd_city(Address);
            surveyActionDTO.setStart_latitude(start_lat);
            surveyActionDTO.setStart_longitude(start_long);
            surveyActionDTO.setStart_date_time(sp.getString("datetime",""));
            surveyActionDTO.setEnd_date_time(endTime);
            surveyActionDTO.setEnd_latitude(String.valueOf(end_lat));
            surveyActionDTO.setEnd_longitude(String.valueOf(end_long));
            surveyActionTableAdapter.open();
            surveyActionTableAdapter.insertSurveyAction(surveyActionDTO);

          //  Toast.makeText(getApplicationContext(),"Inserted",Toast.LENGTH_SHORT).show();
            Log.i("DateTime ",endTime);
            surveyActionTableAdapter.close();
           // Toast.makeText(getApplicationContext(),"Date time "+endTime,Toast.LENGTH_LONG).show();
        } else {
            gpsTracker.showSettingsAlert();
        }

    }

    private void dialog() {
        dialog = new ProgressDialog(do_survey.this,
                AlertDialog.THEME_HOLO_LIGHT);
        dialog.setMessage("Please wait");
        dialog.setTitle("loading ...");
        dialog.setIcon(R.drawable.form_small);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }

    private String getCurrentTimeLong(){
        java.util.Date dt = new java.util.Date();

        java.text.SimpleDateFormat sdf =
                new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(dt);

        return currentTime;

}

    private void saveSurvey() {
        List<SurveyActionDTO> list = new ArrayList<SurveyActionDTO>();
        SurveyActionTableAdapter sta = new SurveyActionTableAdapter(getApplicationContext());
        sta.open();
        list = sta.getListSurveyActionID(sp.getInt("userID", 0));
        sta.close();
        if(list.size() > 0){
            dialog();
            for (SurveyActionDTO s : list) {
                //String url = "http://10.0.2.2/insurvey/index.php?requestType=19&userID="+s.getId_user_fk()+"&start_date_time=" + s.getStart_date_time().replace(" ", "%") + "&end_date_time=" + s.getEnd_date_time().replace(" ", "%") + "&start_latitude=" + s.getStart_latitude() + "&start_longitude=" + s.getStart_longitude() + "&end_latitude=" + s.getEnd_latitude() + "&end_longitude=" + s.getEnd_longitude() + "&id_form_fk="+s.getId_form_fk()+"&start_city=" + s.getStart_city() + "&end_city=" + s.getEnd_city();
                String url = "http://www.insurvey.co.za/api?requestType=19&userID="+ s.getId_user_fk()
                        +"&start_date_time=" + s.getStart_date_time().replace(" ", "%")
                        + "&end_date_time=" + s.getEnd_date_time().replace(" ", "%")
                        + "&start_latitude=" + s.getStart_latitude()
                        + "&start_longitude=" + s.getStart_longitude()
                        + "&end_latitude=" + s.getEnd_latitude()
                        + "&end_longitude=" + s.getEnd_longitude()
                        + "&id_form_fk="+s.getId_form_fk()
                        + "&start_city=" + s.getStart_city().replace(" ","_")
                        + "&end_city=" + s.getEnd_city().replace(" ","_");
                Log.i("BATHONG", url);

                StringRequest request2 = new StringRequest(url,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String arg0) {
                                myVolleySaveSurveyActions(arg0);
                                surveyActionTableAdapter.open();
                                surveyActionTableAdapter.deleteSurveyActions(sp.getInt("userID", 0));
                                surveyActionTableAdapter.close();
                                dialog.dismiss();
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(
                            VolleyError arg0) {


                        dialog.dismiss();// Se
                        // TODO Auto-generated method stub
                    }
                });
                request2.setTag("saveAnswer");
                queue.add(request2);
                dialog.dismiss();
            }

            List<ActivityLogDTO> alist = new ArrayList<ActivityLogDTO>();
            final ActiveLogTableAdapter alt = new ActiveLogTableAdapter(getApplicationContext());
            alt.open();
            alist = alt.getActivityLogList(sp.getInt("userID", 0));
            sta.close();

            for (ActivityLogDTO a : alist) {
                Log.i("URL ",String.valueOf(a.getActivityID()));
                // String urlActivityLog = "http://10.0.2.2/insurvey/index.php?requestType=13&id_user_fk="+a.getUserID()+"&id_form_fk="+a.getFormID()+"&id_activity_fk=" + a.getActivityID();
                String urlActivityLog = "http://www.insurvey.co.za/api?requestType=13&id_user_fk="+a.getUserID()+"&id_form_fk="+a.getFormID()+"&id_activity_fk=" + a.getActivityID();
                //String url="http://10.0.2.2/insurvey/index.php?requestType=19&userID=2&start_date_time="+s.getStart_date_time()+"&end_date_time="+s.getEnd_date_time()+"&place="+s.getPlace()+"&latitude="+s.getLatitude()+"&longitude="+s.getLongitude()+"&id_form_fk=64";
                //String url = "http://www.insurvey.co.za/api?requestType=19&userID=" + sp.getInt("userID", 0) + "&start_date_time=" +s.getStart_date_time()
                //  +"&end_date_time="+s.getEnd_date_time()+"&place="+s.getPlace()+"&latitude="+s.getLatitude()+"&longitude="+s.getLongitude()+"&id_form_fk="+s.getId_form_fk();
                Log.i("URL ", urlActivityLog);
                // Toast.makeText(getApplicationContext(), "End " + s.getStart_date_time(), Toast.LENGTH_LONG).show();

                StringRequest requestActivityLog = new StringRequest(urlActivityLog,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String arg0) {
                                Log.i("Responce :",arg0);
                                myVolleyActivityLog(arg0);
                                alt.open();
                                alt.deleteActivityLogs(sp.getInt("userID", 0));
                                alt.close();
                                dialog.dismiss();
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(
                            VolleyError arg0) {
                        dialog.dismiss();// Se
                        // TODO Auto-generated method stub
                    }
                });
                requestActivityLog.setTag("saveAnswer");
                queue.add(requestActivityLog);
                dialog.dismiss();
            }

        }

    }

    private void myVolley1(String arg0) {
        int x = 0;
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                Log.i("TAG", obj.getString("message"));
                JSONArray array = obj.getJSONArray("answers");
                JSONObject jso = new JSONObject();
            }

            else {
                // toast_error(obj.getString("message"));
                dialog.dismiss();
                //errorDialog();
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //Toast.makeText(getApplicationContext(),"count "+count,Toast.LENGTH_SHORT).show();
    }

    private void myVolleySaveSurveyActions(String arg0) {

        try {
            Log.i("Res ", arg0);
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                JSONArray array = obj.getJSONArray("survey_actions");
                Log.i("TAG", obj.getString("message"));

            } else {
                Log.i("TAG", obj.getString("message"));
                dialog.dismiss();

                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {

            // TODO Auto-generated catch block
            dialog.dismiss();
            e.printStackTrace();
        }
    }

    private void myVolleyActivityLog(String arg0) {

        try {
            Log.i("Res ", arg0);
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                // toast_error(obj.getString("message"));
                Log.i("TAG", obj.getString("message"));

            } else {

                dialog.dismiss();

                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {

            // TODO Auto-generated catch block
            dialog.dismiss();
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.warning_blue)

                .setTitle("Exit Survey")
                .setMessage("Save the survey?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        btnAnswers.performClick();
                        startActivity(new Intent(do_survey.this,List_Downloaded_Surveys.class));
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       // deletePerson();
                      //  startActivity(new Intent(do_survey.this,List_Downloaded_Surveys.class));
                        finish();
                    }
                })
                .show();
    }

    private class TakePrintScreen extends AsyncTask<Void, String, String> {
    private ProgressDialog progress;

        @Override
        protected void onPreExecute() {
           // btnAnswers.setVisibility(View.GONE);
            progress = ProgressDialog.show(do_survey.this, "Saving survey",
                    "Please Wait.. ", true);

//            if (edEmail.length() > 0) {
//
//                if (!isEmailValid(edEmail.getText().toString())) {
//                    //edEmail.requestFocus();
//                    edEmail.setError("Invalid Email Address");
//                    return;
//                }
//            }
//
//            if (edPhone.length() > 0) {
//
//                if (isEmailValid(edPhone.getText().toString())) {
//                    //edEmail.requestFocus();
//                    edPhone.setError("Invalid Cell Phone Number");
//                    return;
//                }
//            }


        }
        @Override
        protected String doInBackground(Void... params) {
            try {
                setUIGetLocation();

            } catch (Exception e) {
                e.printStackTrace();
                setUIThreads("Survey is NOT Saved");
                // Log.e(Tag, e.getMessage().toString());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
        }
    }

   private void setUIGetLocation(){
    new Thread()
    {
        public void run()
        {
            do_survey.this.runOnUiThread(new Runnable()
            {
                public void run()
                {
                    setPersonID();
                    fta.open();
                    fta.insertFormCount(new FormCountDTO(sp.getString("frmName", ""), sp.getInt("userID", 0), getCurrentTimeLong()));
                    fta.close();
                    getLocation();
                    saveDemographicData();
                    saveAnswers();
                    setUIThreads("Survey saved successfully");
                    startActivity(new Intent(do_survey.this, List_Downloaded_Surveys.class));
                    finish();
                }
            });
        }
    }.start();
  }

    private void setUIThreads(final String message){
        new Thread()
        {
            public void run()
            {
                do_survey.this.runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        Toast.makeText(do_survey.this, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }.start();
    }
    private List<SectionQuestionDTO> sectionQuestionDTOs;
    private ActiveLogTableAdapter activeLogTableAdapter;
    private FormCountTableAdapter fta;

    //
    private void getStartLocation() {
        GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {

            double Latitude = gpsTracker.latitude;

            double Longitude =gpsTracker.longitude;

            String cityName = null;
            Geocoder gcd = new Geocoder(getBaseContext(),
                    Locale.getDefault());
            List<Address> addresses = new ArrayList<>();
            try {
                addresses = gcd.getFromLocation(Latitude, Longitude, 1);
                if (addresses.size() > 0)
                    //  System.out.println("dresscode"+addresses.get(0).getLocality());
                    cityName = addresses.get(0).getLocality();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String Address = "";
            try{
                Address = addresses.get(0).getAddressLine(0);
                //  System.out.println("dresscode"+addresses.get(0).getLocality());
            }catch (Exception e){
                Toast.makeText(getApplicationContext(),"Not connected",Toast.LENGTH_SHORT).show();
            }

            System.out.println("dresscode "+Address);
            String start_place = gpsTracker.getLocality(this);

            String start_lat = String.valueOf(Latitude);
            String start_long = String.valueOf(Longitude);

            SharedPreferences.Editor ed = sp.edit();
            ed.putString("Latitude",start_lat);
            ed.putString("Longitude",start_long);
            ed.putString("place", Address);
            ed.commit();

        } else {

            gpsTracker.showSettingsAlert();
        }

    }
    LinearLayout layoutImageSignle;
    private void addImage(ArrayList<QuestionImage> qImage,int index,String answer){

        layoutImageSignle = new LinearLayout(this);

        if(qImage.size() > 0){

            try{
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100,ViewGroup.LayoutParams.WRAP_CONTENT);

                if(index > 0){

                    layoutParams.setMargins(20, 0, 0, 0);

                }
               //

                Bitmap imgBit = ImageFile.getImageBitmap(getApplicationContext(), qImage.get(index).getLocation());

                img = new ImageView(this);

                img.setImageBitmap(imgBit);

              //  linearLayoutContainer.setLayoutParams(layoutParams);
                layoutImageSignle.setGravity(Gravity.CENTER);
                layoutImageSignle.addView(img);
//                layoutImageSignle.setPadding(2,2,2,2);
                //layoutImageSignle.setBackgroundResource(R.drawable.big_card);

               // layoutImageSignle.setPadding(3,3,3,3);

                LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT
                        ,LinearLayout.LayoutParams.WRAP_CONTENT);

                linearLayoutContainer.setLayoutParams(layoutParams1);

                linearLayoutContainer.addView(layoutImageSignle, layoutParams);

              //  linearLayoutContainer.setBackgroundResource(R.drawable.big_card);


            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private String  generateUID(){

        SessionIdentifierGenerator generator = new SessionIdentifierGenerator();
        String uid = generator.nextSessionId();

        return uid;

    }

    public final class SessionIdentifierGenerator {
        private SecureRandom random = new SecureRandom();

        public String nextSessionId() {
            return new BigInteger(130, random).toString(32);
        }
    }

    private void setIDs(EditText ed,String text){

        switch (text){
            case "Name":
                ed.setId(R.id.pName);
                break;
            case "Surname":
                ed.setId(R.id.pSurname);
                break;
            case "Date Of Birth":
                ed.setId(R.id.pDOB);
                break;
            case "Number Of Occupants":
                ed.setId(R.id.pNumberOfOccupants);
                break;
            case "Address":
                ed.setId(R.id.pAddress);
                break;
            case "Phone Number":
                ed.setId(R.id.pPhoneNo);
                break;
            case "Email Address":
                ed.setId(R.id.pEmailAddress);
                break;
            case "Company's Contact":
                ed.setId(R.id.pCompanyContact);
                break;
        }

    }
    private DemorgraphicTableAdapter dta;
    private PersonDTO personDTO;
    private int personID;
    private ProgressDialog dialog;
    private ConnectionDetector cd;
    RequestQueue queue;


    private List<SaveAnswersDTO> saveAnswersDTOs = new ArrayList<SaveAnswersDTO>();
    private List<ActivityLogDTO> activityLogDTOs = new ArrayList<ActivityLogDTO>();
    private SaveAnswerTableAdapter sata;
    private ContactTableAdapter cta;

    private List<PersonDTO> pList;
    private boolean  destroy = false;
    private boolean  saved = false;
    List<PersonDTO> p;
    List<ContactDTO> cList;
    private  AlertDialog deleteDialog;
    private int totalContacts;
    boolean active;
    int amt = 0;
    private ProgressDialog pDialog;

}