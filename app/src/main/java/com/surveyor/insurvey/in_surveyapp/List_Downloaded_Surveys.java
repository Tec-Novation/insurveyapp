package com.surveyor.insurvey.in_surveyapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.melnykov.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dto.ActivityLogDTO;
import dto.ConnectionDetector;
import dto.ContactDTO;
import dto.DemorgraphicDTO;
import dto.FormCountDTO;
import dto.FormDTO;
import dto.PersonDTO;
import dto.SMSReport;
import dto.SaveAnswersDTO;
import dto.SectionQuestionDTO;
import dto.SurveyActionDTO;
import extras.SendSMS;
import sql.ActiveLogTableAdapter;
import sql.ContactTableAdapter;
import sql.DemorgraphicTableAdapter;
import sql.FormCountTableAdapter;
import sql.FormTableAdapter;
import sql.PersonTableAdapter;
import sql.SaveAnswerTableAdapter;
import sql.SectionQuestionTableAdapter;
import sql.SurveyActionTableAdapter;


public class List_Downloaded_Surveys extends ActionBarActivity {
    private FormTableAdapter fta;
    List<FormDTO> frmList;
    private ListView lstView1;
    private SurveyListdapter sla;
    private RelativeLayout RelLayout;
    private RelativeLayout formLayout;
    private Button closeform;
    private int formID1;
    private int userID;
    private EditText edtPersonFirstName;
    private EditText edtPersonLastName;
    private EditText edtPersonPhone;
    private EditText edtPersonEmail;
    private Button btnsubmit;
    private Spinner sp_serviceProvide;
    final Context context = this;
    private LayoutInflater layoutInflater;
    private int count = 0;
    private View promptView;
    private String name, lname, phone, email;
    private int serviceProviderID;
    private Bitmap myProfileImage;
    private PersonTableAdapter pta;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private ProgressDialog dialog;
    private SectionQuestionTableAdapter sqta;
    private List<SectionQuestionDTO> sqList;
    private FormCountTableAdapter ftac;
    private List<PersonDTO> getpList;
    private int personCount = 0;
    RequestQueue queue;
    String urlDemo;

    private ArrayList<Integer> personIdList = new ArrayList<>();

    private SharedPreferences sp;
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    String TITLES[] = {"Main Menu", "Download", "Survey List", "Surveys Actions","Completed","About", "Logout"};
    int ICONS[] = {R.drawable.ic_home,
                   R.drawable.ic_download,
                   R.drawable.ic_list,
                   R.drawable.ic_actions,
                   R.drawable.ic_completed,
                   R.drawable.ic_about,
                   R.drawable.ic_logout};

    //And we also create a int resource for profile picture in the header view

    String NAME = "";
    String EMAIL = "";
    int PROFILE = R.drawable.pro;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;
    String recieve_sms = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_surveys);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setHomeButtonEnabled(true);
        setFields();

        JSONArray array = new JSONArray();

//
//        for(int i=1;i <= 50;i++){
//            JSONObject object = new JSONObject();
//            try {
//
//                object.put("id",i);
//                object.put("ids","Municipality-"+i);
//                object.put("value","Ward "+i );
//                array.put(object);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//
//        Log.i( "JSONArrays ",array.toString());


    }



    private void setFields() {

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        fta = new FormTableAdapter(getApplicationContext());
        sata = new SaveAnswerTableAdapter(getApplicationContext());
        fcta = new FormCountTableAdapter(getApplicationContext());
        dta = new DemorgraphicTableAdapter(getApplicationContext());
        lstView1 = (ListView) findViewById(R.id.lstSurveysSQLite);
        int formID1 = sp.getInt("formID1", 0);
        getpList = new ArrayList<PersonDTO>();
        cd = new ConnectionDetector(getApplicationContext());
        pta = new PersonTableAdapter(getApplicationContext());
        cta = new ContactTableAdapter(getApplicationContext());
        userID = sp.getInt("userID", 0);
        sqList = new ArrayList<SectionQuestionDTO>();
        sqta = new SectionQuestionTableAdapter(getApplicationContext());
        // RelLayout = (RelativeLayout)findViewById(R.id.RelLayout);
        formLayout = (RelativeLayout) findViewById(R.id.formLayout);
        queue = Volley
                .newRequestQueue(getApplicationContext());
        closeform = (Button) findViewById(R.id.btnCloseForm);
        NAME = sp.getString("name", "") + " " + sp.getString("lastname", "");
        EMAIL = sp.getString("email", "");
        surveyActionTableAdapter = new SurveyActionTableAdapter(getApplicationContext());
        surveyActionDTO = new SurveyActionDTO();

        pta.open();
        getpList = pta.getPersonsList(sp.getInt("userID",0));
        pta.close();
        personCount = getpList.size();
        setFormListAdapter();
//        sata.open();
//        saveAnswersDTOs = sata.getSavedAnswers(sp.getInt("userID", 0));
//        sata.close();
        try {
            if(getpList.size() > 0) {
                saveSurvey();
                addPerson();
                //checkLoginStatus();
            }

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Unable to save survey", Toast.LENGTH_SHORT).show();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.attachToListView(lstView1);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(List_Downloaded_Surveys.this, MySurvey_List.class));
                finish();
            }
        });
        closeform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(List_Downloaded_Surveys.this, Main_OneActivity.class));
                finish();
            }
        });
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size
        // sqList


        FormTableAdapter fta = new FormTableAdapter(getApplicationContext());
        ftac = new FormCountTableAdapter(getApplicationContext());
        List<FormDTO> frmList = new ArrayList<FormDTO>();
        List<FormCountDTO> countDTOs = new ArrayList<FormCountDTO>();
        fta.open();
        frmList = fta.getFormList(sp.getInt("userID", 0));
        fta.close();
        ftac.open();
        countDTOs = ftac.getFormCountList(sp.getInt("userID", 0));
        SharedPreferences.Editor ed = sp.edit();
        ed.putInt("SQLFCount", countDTOs.size()).commit();
        ftac.close();
        String cnt[];
        if (sp.getString("fc", "").equals("")) {
            cnt = new String[]{"-1", String.valueOf(sp.getInt("formCount", 0) - frmList.size()), String.valueOf(frmList.size()), "-1", "-1", "-1", "-1"};
        } else {
            cnt = new String[]{"-1", sp.getString("fc", ""), String.valueOf(frmList.size()), "-1", "-1", "-1", "-1"};
        }
        // String cnt[] = {"-1",String.valueOf(sp.getInt("formCount",0) - frmList.size()),String.valueOf(frmList.size()),"-1",String.valueOf(countDTOs.size()),"-1","-1"};
        //int cnt[] = {-1, sp.getInt("formCount",0) -  frmList.size(),frmList.size(),-1,countDTOs.size(),-1,-1};
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.pro);
        String encodedProfileImage = sp.getString("encodedProfileImage", "");
        if (!encodedProfileImage.equalsIgnoreCase("")) {
            byte[] b = Base64.decode(encodedProfileImage, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
            //imageConvertResult.setImageBitmap(bitmap);
        }

        mAdapter = new MyAdapter(TITLES, ICONS, NAME, EMAIL, PROFILE, this, cnt, bitmap);      // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        final GestureDetector mGestureDetector = new GestureDetector(List_Downloaded_Surveys.this, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });


        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                    //  Drawer.closeDrawers();
                    //  Toast.makeText(List_Downloaded_Surveys.this,"The Item Clicked is: "+recyclerView.getChildPosition(child),Toast.LENGTH_SHORT).show();
                    navigate(recyclerView.getChildPosition(child));
                    return true;

                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }


        });

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }


        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(List_Downloaded_Surveys.this, Main_OneActivity.class));
        finish();
    }

    private void navigate(int number) {
    Context ctx = getApplicationContext();
        Intent intent;
        switch (number) {
            case 0:
                intent = new Intent(ctx, ProfileActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 1:
                intent = new Intent(ctx,Main_OneActivity.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 2:
                if(sp.getString("fc","").equals("")) {
                    intent = new Intent(ctx, MySurvey_List.class);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Drawer.closeDrawers();
                    startActivity(intent);
                    finish();
                }
                break;
            case 3:

                Drawer.closeDrawers();
                break;


            case 4:
                intent = new Intent(ctx, SurveyActionActivity.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;

            case 5:
                intent = new Intent(ctx, CompletedSurveyActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 6:
                Toast.makeText(getApplicationContext(),"Still Under Construction",Toast.LENGTH_SHORT).show();
                Drawer.closeDrawers();
                break;
            case 7:
                intent = new Intent(ctx,LogIn.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
        }

    }

    private void dialog() {
        dialog = new ProgressDialog(this,
                AlertDialog.THEME_HOLO_LIGHT);
        dialog.setMessage("Please wait");
        dialog.setTitle("Loading ...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }

    private void setFormListAdapter() {

        FormDTO dto = new FormDTO();
        dto.setFormID(sp.getInt("formID", 0));
        //dto.setUserID(sp.getInt("userID",0));
        dto.setFormStatusID(sp.getInt("formStatusID", 0));
        dto.setName(sp.getString("formName", ""));
        dto.setDescription(sp.getString("description", ""));
        dto.setCreateDate(sp.getString("cdate", ""));
        dto.setLastUpdated(sp.getString("ludate", ""));
        formDTO = new FormDTO();

        fta.open();
        frmList = fta.getFormList(userID);
        fta.close();
        // Toast.makeText(getApplicationContext(),userID+" "+"List xx "+frmList,Toast.LENGTH_SHORT).show();
        if (frmList.size() > 0) {
            questionsDialog();
            setFormListAdapter(frmList);
            formLayout.setVisibility(View.GONE);
            lstView1.setVisibility(View.VISIBLE);
            dialog.dismiss();
        } else {
            formLayout.setVisibility(View.VISIBLE);
            lstView1.setVisibility(View.GONE);
          // dialog.dismiss();
        }
    }

    private void questionsDialog() {
        dialog = new ProgressDialog(List_Downloaded_Surveys.this,
                AlertDialog.THEME_HOLO_LIGHT);
        dialog.setTitle("Please wait");
        dialog.setMessage("Loading forms...");
        dialog.setIcon(R.drawable.form_small);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_surveys_activity, menu);
        return true;
    }

    private void setFormListAdapter(List<FormDTO> fl) {
        sla = new SurveyListdapter(getApplicationContext(), R.layout.sql_list, fl);
        lstView1.setAdapter(sla);
    }
    private String getUrl(int id_form){
        urlDemo = "http://www.insurvey.co.za/api?requestType=29&id_form_fk="+ id_form;
        return  urlDemo;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.main_menu) {
            Intent intent = getIntent();
            finish();
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class SurveyListdapter extends ArrayAdapter<FormDTO> {
        private SectionQuestionTableAdapter sqta;
        private LayoutInflater mInflate;
        private final int mResource;
        private Context context;
        private ProgressDialog dialog;
        private List<FormDTO> list;
        private int lastPosition = -1;
        private SharedPreferences sp;

        public SurveyListdapter(Context context, int textViewResourceId,
                                List<FormDTO> list) {
            super(context, textViewResourceId, list);
            this.context = context;
            mResource = textViewResourceId;
            this.list = list;
            mInflate = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final View view;
            ViewHolder vh;
            final FormDTO dto = (FormDTO) getItem(position);

            if (convertView == null) {
                view = mInflate.inflate(R.layout.sql_list, parent, false);
                setViewHolder(view);
            } else if (((ViewHolder) convertView.getTag()).needInflate) {
                view = mInflate.inflate(R.layout.sql_list, parent, false);
                setViewHolder(view);
            } else {
                view = convertView;
            }
            vh = (ViewHolder) view.getTag();
            vh.TV_descript.setText(dto.getDescription());
            vh.TV_dateCreated.setText("Created :" + dto.getCreateDate());
            vh.TV_dateUpdated.setText("Update:" + dto.getLastUpdated());
            vh.TV_name.setText(dto.getName());

            vh.TV_total.setText(""+dto.getSurvey_total());

            vh.TV_survey_limit.setText("" + dto.getSurvey_limit());

            if(dto.getSurvey_limit() == dto.getSurvey_total()){
                vh.TV_total.setBackgroundResource(R.drawable.square_red);
                //vh.imgGo.setEnabled(false);
            }

            vh.imgGo.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                  //  Log.d("urlDemo ",urlDemo);
//                    String text = "Loading ...";
//                    final extras.Dialog dialog = new extras.Dialog(List_Downloaded_Surveys.this,text);
//                    dialog.showProgressDialog();
//                    Log.d("URL ",getUrl(dto.getFormID()));
//                    RequestQueue queue = Volley
//                            .newRequestQueue(List_Downloaded_Surveys.this);
//                    StringRequest strReq = new StringRequest(Request.Method.GET,
//                            getUrl(dto.getFormID()), new Response.Listener<String>() {
//
//                               @Override
//                                public void onResponse(String response) {
//                                    Log.d("TAG YO", response.toString());
//                                    Log.d("URL", response.toString());
//                                    try {
//                                        JSONObject obj = new JSONObject(response);
//                                        if (obj.optString("success").equals("0")) {
                                            //In-Progress
                                            if (checkQuestions(dto.getFormID())) {
                                                Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                                                vibrator.vibrate(200);
                                                sp = PreferenceManager.getDefaultSharedPreferences(getContext());
                                                SharedPreferences.Editor ed = sp.edit();
                                                int formID = dto.getFormID();
                                                ed.putInt("formID1", formID);
                                                Log.i("FormID ", "" + dto.getFormID());
                                                // getDemographic(formID);
                                                ed.putString("frmName", dto.getName());
                                                ed.putString("sms", dto.getSmsDescription());
                                                String start = getCurrentTimeLong();
                                                getLocation();
                                                ed.putString("datetime", start);
                                                ed.commit();
                                                startActivity(new Intent(List_Downloaded_Surveys.this, do_survey.class));
                                                //setDialog();
                                            } else {
                                                Toast.makeText(List_Downloaded_Surveys.this, "Survey does not have questions", Toast.LENGTH_SHORT).show();
                                            }
//                                        } else {
//                                            //Completed
//                                            Toast.makeText(List_Downloaded_Surveys.this, obj.optString("message"), Toast.LENGTH_SHORT).show();
//                                        }
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                    //msgResponse.setText(response.toString());
//                                    dialog.hideProgressDialog();
//
//                                }
//                            }, new Response.ErrorListener() {
//
//                                @Override
//                                public void onErrorResponse(VolleyError error) {
//                                    VolleyLog.d("VolleyError", "Error: " + error.getMessage());
//                                    Toast.makeText(context, "Network connection error.", Toast.LENGTH_SHORT).show();
//                                    dialog.hideProgressDialog();
//                                }
//                            });
//
//                    queue.add(strReq);

                }
            });

            vh.img_delete.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(200);
                    deleteCell(view, position);
                }
            });
            Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
            view.startAnimation(animation);
            lastPosition = position;
            return view;
        }

        class ViewHolder {
            public boolean needInflate;
            TextView TV_descript;
            TextView TV_dateCreated;
            TextView TV_dateUpdated;
            TextView TV_total;
            TextView TV_survey_limit;
            TextView TV_name;

            Button imgGo;
            ImageView img_delete;
        }

        private void setViewHolder(View view) {
            ViewHolder vh = new ViewHolder();
            vh.TV_descript = (TextView) view.findViewById(R.id.tv_desc);
            vh.TV_survey_limit = (TextView) view.findViewById(R.id.tv_survey_limit);
            vh.TV_total = (TextView) view.findViewById(R.id.tv_survey_total);
            vh.TV_dateCreated = (TextView) view.findViewById(R.id.tv_created);
            vh.TV_dateUpdated = (TextView) view.findViewById(R.id.tv_updated);
            vh.TV_name = (TextView) view.findViewById(R.id.tv_names1);
            vh.imgGo = (Button) view.findViewById(R.id.img_do);
            vh.img_delete = (ImageView) view.findViewById(R.id.img_delete);
            vh.needInflate = false;
            view.setTag(vh);
        }

        private void deleteCell(final View v, final int index) {
            Animation.AnimationListener al = new Animation.AnimationListener() {
                @Override
                public void onAnimationEnd(Animation arg0) {
                    frmList.remove(index);
                    ViewHolder vh = (ViewHolder) v.getTag();
                    vh.needInflate = true;
                    sla.notifyDataSetChanged();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationStart(Animation animation) {
                }
            };

            collapse(v, al);
        }

        private void collapse(final View v, Animation.AnimationListener al) {
            final int initialHeight = v.getMeasuredHeight();
            final Animation out = AnimationUtils.loadAnimation(context, R.anim.push_left_out);
            Animation anim = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    if (interpolatedTime == 1) {
                        v.setVisibility(View.GONE);
                    } else {
                        v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                        v.requestLayout();
                    }
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };

            if (al != null) {
                out.setAnimationListener(al);
            }

            anim.setDuration(200);
            v.startAnimation(out);
        }

    }

    private boolean checkQuestions(int formID) {
        boolean hasValues;
        sqta.open();
        sqList = sqta.getListSectionQuestionFormID(formID);
        sqta.close();

        if (sqList.size() > 0) {

            hasValues = true;
        } else {
            hasValues = false;
        }

        return hasValues;
    }

    private void saveAnwers(String personID) {
    if(cd.isConnectingToInternet()) {
    if (saveAnswersDTOs.size() > 0) {
        dialog();
        for (SaveAnswersDTO al:saveAnswersDTOs) {

                    String url = "http://www.insurvey.co.za/api?requestType=17&id_user_fk="
                            + Integer.toString(al.getUserID())
                            + "&id_form_fk=" + Integer.toString(al.getFormID())
                            + "&id_activity_fk=" + al.getActivityID()
                            + "&answer=" + al.getAnswer()
                            + "&id_question_fk=" + Integer.toString(al.getQuestionID())
                            + "&id_activity_log_fk=" + Integer.toString(al.getActivityID())
                            + "&id_person_fk=" + personID;
                    Log.i("URLS ", url);
                    JSONObject js = new JSONObject();

                    try {

                        StringRequest request = new StringRequest(url,
                                new Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String arg0) {
                                        //  Log.i(LOG, arg0.toString());

                                        count++;
                                        myVolley1(arg0);

                                        Log.i("Count ", "" + count++);

                                        destroy = true;

                                    }
                                }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(
                                    VolleyError arg0) {
                                // TODO Auto-generated method stub
                                 dialog.dismiss();
                                // toast("Network Connection Error");
                                //  toast("Check your network connection");
                                // Log.i(LOG, arg0.toString());
                            }
                        });
                        request.setTag("saveAnswer");
                        queue.add(request);
                    } catch (Exception e) {
                        dialog.dismiss();
                        e.printStackTrace();
                    }

            }

        }

    }

}


    @Override
    protected void onStop() {

        super.onStop();
        Log.i("INFOMATION BATHO", "Stopped");
        if(destroy) {
            deleteAll();
        }

    }
    private void saveSurvey() {
        List<SurveyActionDTO> list = new ArrayList<SurveyActionDTO>();
        SurveyActionTableAdapter sta = new SurveyActionTableAdapter(getApplicationContext());
        sta.open();
        list = sta.getListSurveyActionID(sp.getInt("userID", 0));
        sta.close();
        if(list.size() > 0){
            dialog();
        for (SurveyActionDTO s : list) {
            //String url = "http://10.0.2.2/insurvey/index.php?requestType=19&userID="+s.getId_user_fk()+"&start_date_time=" + s.getStart_date_time().replace(" ", "%") + "&end_date_time=" + s.getEnd_date_time().replace(" ", "%") + "&start_latitude=" + s.getStart_latitude() + "&start_longitude=" + s.getStart_longitude() + "&end_latitude=" + s.getEnd_latitude() + "&end_longitude=" + s.getEnd_longitude() + "&id_form_fk="+s.getId_form_fk()+"&start_city=" + s.getStart_city() + "&end_city=" + s.getEnd_city();
            String url = "http://www.insurvey.co.za/api?requestType=19&userID="+ s.getId_user_fk()
                    +"&start_date_time=" + s.getStart_date_time().replace(" ", "_")
                    + "&end_date_time=" + s.getEnd_date_time().replace(" ", "_")
                    + "&start_latitude=" + s.getStart_latitude()
                    + "&start_longitude=" + s.getStart_longitude()
                    + "&end_latitude=" + s.getEnd_latitude()
                    + "&end_longitude=" + s.getEnd_longitude()
                    + "&id_form_fk="+s.getId_form_fk()
                    + "&start_city=" + s.getStart_city().replace(" ","_")
                    + "&end_city=" + s.getEnd_city().replace(" ","_");
            Log.i("BATHONG", url);
            thisFormID = s.getId_form_fk();
            StringRequest request2 = new StringRequest(url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String arg0) {
                            myVolleySaveSurveyActions(arg0);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(
                        VolleyError arg0) {
                    arg0.getStackTrace();
                    dialog.dismiss();// Se
                    // TODO Auto-generated method stub
                }
            });
            request2.setTag("saveAnswer");
            queue.add(request2);
            dialog.dismiss();
        }

        List<ActivityLogDTO> alist = new ArrayList<ActivityLogDTO>();
        final ActiveLogTableAdapter alt = new ActiveLogTableAdapter(getApplicationContext());
        alt.open();
        alist = alt.getActivityLogList(sp.getInt("userID", 0));
        sta.close();

        for (ActivityLogDTO a : alist) {
            Log.i("URL ",String.valueOf(a.getActivityID()));
            // String urlActivityLog = "http://10.0.2.2/insurvey/index.php?requestType=13&id_user_fk="+a.getUserID()+"&id_form_fk="+a.getFormID()+"&id_activity_fk=" + a.getActivityID();
            String urlActivityLog = "http://www.insurvey.co.za/api?requestType=13&id_user_fk="+a.getUserID()+"&id_form_fk="+a.getFormID()+"&id_activity_fk=" + a.getActivityID();
            //String url="http://10.0.2.2/insurvey/index.php?requestType=19&userID=2&start_date_time="+s.getStart_date_time()+"&end_date_time="+s.getEnd_date_time()+"&place="+s.getPlace()+"&latitude="+s.getLatitude()+"&longitude="+s.getLongitude()+"&id_form_fk=64";
            //String url = "http://www.insurvey.co.za/api?requestType=19&userID=" + sp.getInt("userID", 0) + "&start_date_time=" +s.getStart_date_time()
            //  +"&end_date_time="+s.getEnd_date_time()+"&place="+s.getPlace()+"&latitude="+s.getLatitude()+"&longitude="+s.getLongitude()+"&id_form_fk="+s.getId_form_fk();
            Log.i("URL ", urlActivityLog);
            // Toast.makeText(getApplicationContext(), "End " + s.getStart_date_time(), Toast.LENGTH_LONG).show();

            StringRequest requestActivityLog = new StringRequest(urlActivityLog,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String arg0) {
                            Log.i("Responce :",arg0);
                            myVolleyActivityLog(arg0);
                            alt.open();
                            alt.deleteActivityLogs(sp.getInt("userID", 0));
                            alt.close();
                            dialog.dismiss();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(
                        VolleyError arg0) {
                    dialog.dismiss();// Se
                    // TODO Auto-generated method stub
                }
            });
            requestActivityLog.setTag("saveAnswer");
            queue.add(requestActivityLog);

        }

    }

  }

    private void myVolley1(String arg0) {
        int x = 0;
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                Log.i("TAG", obj.getString("message"));
                JSONArray array = obj.getJSONArray("answers");
                JSONObject jso = new JSONObject();

            }
            else {
                // toast_error(obj.getString("message"));
                dialog.dismiss();
                //errorDialog();
                Log.i("TAG", obj.getString("message"));
            }
            dialog.dismiss();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            dialog.dismiss();
            e.printStackTrace();
        }
        //Toast.makeText(getApplicationContext(),"count "+count,Toast.LENGTH_SHORT).show();
    }

    private void getTotalSurvey(String arg0) {
        int x = 0;
        FormDTO  formDTO = null;
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("0")) {
                Log.i("TAG", obj.getString("message"));
                if(obj.has("demo")){
                    JSONObject jso = obj.getJSONObject("demo");

                      formDTO = new FormDTO(jso.getInt("id_form"),
                                    sp.getInt("userID", 0),
                                    jso.getString("name"),
                                    jso.getString("create_date"),
                                    jso.getString("last_updated"),
                                    jso.getInt("id_form_status_fk"),
                                    jso.getString("description"),
                                    jso.getString("message"),
                                    jso.getInt("survey_limit"),
                                    jso.getInt("total_surveys"));
                }
                fta.open();
                fta.updateForm(formDTO);
                fta.close();
            }
            else {
                // toast_error(obj.getString("message"));
                dialog.dismiss();
                //errorDialog();
                Log.i("TAG", obj.getString("message"));
            }
            dialog.dismiss();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            dialog.dismiss();
            e.printStackTrace();
        }
        //Toast.makeText(getApplicationContext(),"count "+count,Toast.LENGTH_SHORT).show();
    }

    private void myVolleySaveSurveyActions(String arg0) {

        try {
            Log.i("Res ", arg0);
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                JSONArray array = obj.getJSONArray("survey_actions");
                Log.i("TAG", obj.getString("message"));
                surveyActionTableAdapter.open();
                surveyActionTableAdapter.deleteSurveyActions(sp.getInt("userID", 0));
                surveyActionTableAdapter.close();

                dialog.dismiss();

            } else {
                Log.i("TAG", obj.getString("message"));
                dialog.dismiss();

                Log.i("TAG", obj.getString("message"));
            }
            dialog.dismiss();
        } catch (JSONException e) {

            // TODO Auto-generated catch block
            dialog.dismiss();
            e.printStackTrace();
        }
    }

    private void myVolleyActivityLog(String arg0) {

        try {
            Log.i("Res ", arg0);
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                // toast_error(obj.getString("message"));
                Log.i("TAG", obj.getString("message"));

            } else {

                dialog.dismiss();

                Log.i("TAG", obj.getString("message"));
            }
            dialog.dismiss();
        } catch (JSONException e) {

            // TODO Auto-generated catch block
            dialog.dismiss();
            e.printStackTrace();
        }
    }

    public static boolean CheckInternet(Context context){
        ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo mobile = connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        return wifi.isConnected() || mobile.isConnected();
    }

    private int sent = 0;
    private void addPerson() throws UnsupportedEncodingException {

    pList  = new ArrayList<>();
    pta.open();
    pList = pta.getPersonsList(sp.getInt("userID", 0));
    pta.close();

    //String urlDemo = "http://10.0.2.2/insurvey/index.php?requestType=12&name="+""+"&lastname="+""+"&phone="+edPhone.getText().toString()+"&age="+personDTO.getAge().replace(" ","_")+"&race="+personDTO.getRace()+"&gender="+personDTO.getGender().replace(" ","_")+"&marital="+personDTO.getMarital().replace(" ","_")+"&employment="+personDTO.getEmployment().replace(" ","_")+"&email="+edEmail.getText().toString()+"&id_service_provider_fk="+networkProvider;
    Log.i("Person size", " " + pList.size());
        final int count = 0;


        for(final PersonDTO p:pList){
        String age= "";
        String race= "";
        String employment= "";
        String marital = "";
        age = URLEncoder.encode(p.getAge(), "UTF-8");
        race = URLEncoder.encode(p.getRace(), "UTF-8");
        employment = URLEncoder.encode(p.getEmployment(), "UTF-8");
        marital = URLEncoder.encode(p.getMarital(), "UTF-8");



        Log.i("SMS Receive ",""+p.getSms_recieve());
        sent = p.getSms_recieve();
        String urlDemo =  "http://www.insurvey.co.za/api?requestType=12&name=" +p.getName()
                +"&lastname="+ p.getLastname()
                +"&phone="+p.getPhone()+"&age="+age
                +"&race="+p.getRace()+"&gender="+p.getGender()
                +"&marital="+marital
                +"&employment=" +employment
                +"&email="+p.getEmail()
                +"&sms_recieve="+p.getSms_recieve()
                +"&dateOfbirth="+ p.getDateOfbirth()
                +"&income="+p.getIncome()
                +"&noOfOccupants="+p.getNoOfOccupants()
                +"&companyPhone="+p.getCompanyPhone()
                +"&inhouseholds="+p.getInhousehold()
                +"&municipality="+p.getMunicipality()
                +"&uid="+p.getUid()
                +"&id_service_provider_fk="+p.getServiceProvideID()
                +"&address="+p.getAddress();





        Log.i("URL person adding bra ", urlDemo);

        final List<String> list = new ArrayList<>();

        list.add(age);
        list.add(race);
        list.add(marital);
        list.add(employment);
        list.add(p.getGender());
        //dialog(); ********************
        StringRequest requestAddDenographic = new StringRequest(urlDemo,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String arg0) {
                        Log.i("Responce Person :",arg0);
                        JSONObject obj = null;

                        try {
                            obj = new JSONObject(arg0);

                            String success = obj.optString("success","");

                            if (success.equals("1")) {
                                uptateSurveyTotal(sp.getInt("formID1", 0));
                                obj = obj.getJSONObject("persons");
                                personID = obj.getString("id_person");
                                recieve_sms  = obj.getString("sms_recieve");

                                Integer id_person = Integer.parseInt(obj.getString("id_person"));

                                saveAnswersDTOs  = new ArrayList<>();
                                sata.open();
                                saveAnswersDTOs = sata.getSavedAnswers(sp.getInt("userID", 0),p.getPersonID());
                                sata.close();

                                String phone = obj.getString("phone");
                               // int id_form = Integer.parseInt(obj.getString("phone"));
                                if(phone.equals("null")){
                                    phone = "";
                                    Log.i("Before :",phone);
                                }
                                Log.i("After :", phone);
                               // saveSurvey(personID);
                               // if(cd.isConnectingToInternet()) {
                                    if (saveAnswersDTOs.size() > 0) {
                                        dialog();
                                        for ( SaveAnswersDTO s:saveAnswersDTOs) {

                                            String url = "http://www.insurvey.co.za/api?requestType=17&id_user_fk="
                                                    + Integer.toString(s.getUserID())
                                                    + "&id_form_fk=" + Integer.toString(s.getFormID())
                                                    + "&id_activity_fk=" + s.getActivityID()
                                                    + "&answer=" + s.getAnswer()
                                                    + "&id_question_fk=" + Integer.toString(s.getQuestionID())
                                                    + "&id_activity_log_fk=" + Integer.toString(s.getActivityID())
                                                    + "&id_person_fk=" + personID;
                                            Log.i("URLS ", url);
                                            JSONObject js = new JSONObject();

                                            try {

                                                StringRequest request = new StringRequest(url,
                                                        new Response.Listener<String>() {

                                                            @Override
                                                            public void onResponse(String arg0) {
                                                                Log.i("Responce Save :",arg0);

                                                                myVolley1(arg0);

                                                                destroy = true;

                                                            }
                                                        }, new Response.ErrorListener() {

                                                    @Override
                                                    public void onErrorResponse(
                                                            VolleyError arg0) {
                                                        // TODO Auto-generated method stub
                                                        dialog.dismiss();
                                                        // toast("Network Connection Error");
                                                        //  toast("Check your network connection");
                                                        // Log.i(LOG, arg0.toString());
                                                    }
                                                });
                                                request.setTag("saveAnswer");
                                                queue.add(request);
                                            } catch (Exception e) {
                                                dialog.dismiss();
                                                e.printStackTrace();
                                            }

                                        }

                                  //  }

                                }


//                                for(String s:list){
//                                    addDemo(s, p.getFormID());
//                                }

                                try {
//
//                                    switch (sent){
//                                        case 1:
                                            sendSMS(recieve_sms,personID, phone);
//                                            break;
//                                    }

                                   // personCount;
                                    //setSurveyProgressCount();
                                    //sla.notifyDataSetChanged();
                                } catch (UnsupportedEncodingException e) {
                                    //
                                }

                                Log.i("counting ",""+counting);
                                Log.i("personCount ",""+personCount);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        dialog.dismiss();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(
                    VolleyError arg0) {
                dialog.dismiss();// Se
                // TODO Auto-generated method stub
            }

        });
        queue.add(requestAddDenographic);
        dialog.dismiss();
    }

 }

    private void sendSMS(final String recieve_sms,final String personNo,String numbers) throws UnsupportedEncodingException {

        String msg = URLEncoder.encode(sp.getString("sms",""), "UTF-8");//sp.getString("sms","");
        String nms = msg+"&Numbers="+numbers;
        Log.i("CHECK_LOGINs ", SendSMS.SEND_SMS + nms);
        StringRequest requestAddDenographic = new StringRequest(SendSMS.SEND_SMS+nms,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String arg0) {
                        Log.i("SEND_SMS :", arg0);

                         int badNum =  getStatus(arg0,"BADDEST");
                         int noCredit =  getStatus(arg0,"NOCREDITS");
                         int error =  getStatus(arg0,"Error=\"No numbers supplied\"");
                         int other = getStatus(arg0,"null=BADDEST&");
                        SMSReport smsReport = new SMSReport();

                        if(badNum > 0){

                            smsReport.set_id_smsreport_type_fk(SendSMS.INVALID_PHONE);

                        }else if(noCredit > 0){

                            smsReport.set_id_smsreport_type_fk(SendSMS.INCIFFICIENT_CREDIT);

                        }else if(error > 0){

                                smsReport.set_id_smsreport_type_fk(SendSMS.NO_NUMBERS_SUPPLIED);

                        }else{
                                smsReport.set_id_smsreport_type_fk(SendSMS.DELIVERED);
                            }

                        addSMSReport(personNo,Integer.toString(smsReport.get_id_smsreport_type_fk()));
                        Log.i("Bad Num :",""+badNum);
                        Log.i("No Credit :",""+noCredit);
                        Log.i("Creditor :", "" + amt);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(
                    VolleyError arg0) {
                arg0.getMessage();
                // TODO Auto-generated method stub
            }

        });
        queue.add(requestAddDenographic);

        //  }else
        //  {
        //    Log.i("Credit :",Integer.toString(getCredit()));
        //  }

    }
    private void addSMSReport(String personID,String smsreportID){

        String urlDemo = "http://www.insurvey.co.za/api?requestType=26&id_smsreport_type_fk="+ smsreportID
                +"&id_form_fk="+ sp.getInt("formID1", 0)
                +"&id_user_fk=" + sp.getInt("userID",0)
                +"&id_person_fk="+ personID ;

            Log.i("URL person addinf bra ", urlDemo);
            //dialog(); ********************
            StringRequest requestAddDenographic = new StringRequest(urlDemo,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String arg0) {
                            Log.i("Responce :", arg0);
                            JSONObject obj = null;

                            dialog.dismiss();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(
                        VolleyError arg0) {
                    dialog.dismiss();// Se
                    // TODO Auto-generated method stub
                }

            });
            queue.add(requestAddDenographic);
            dialog.dismiss();

    }
    private void uptateSurveyTotal(int id_form_fk){

        String urlDemo = "http://www.insurvey.co.za/api?requestType=28&id_form_fk="+ id_form_fk;
        Log.i(" Survey Total url ", urlDemo);
        //dialog(); ********************
        StringRequest request = new StringRequest(urlDemo,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String arg0) {
                        Log.i("Responce :",arg0);
                        JSONObject obj = null;
                        getTotalSurvey(arg0);
                        dialog.dismiss();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(
                    VolleyError arg0) {
                dialog.dismiss();// Se
                // TODO Auto-generated method stub
            }

        });
        queue.add(request);
        dialog.dismiss();

    }


    private int getStatus(String in,String out){
        int i = 0;
        Pattern p = Pattern.compile(out);
        Matcher m = p.matcher(in);
        while (m.find()) {
            i++;
        }
        return i;
    }

    private void deleteAll(){
        cta.open();
        cta.deleteContact(sp.getInt("userID", 0), sp.getInt("formID1", 0));
        cta.close();

        sata.open();
        sata.deleteAnswersDTO(sp.getInt("userID", 0));
        sata.close();

        pta.open();
        pta.deleteByPersonUserID(sp.getInt("userID", 0));
        pta.close();

        ftac.open();
        ftac.deleteFormCount(sp.getInt("userID", 0));
        ftac.close();

    }
    private String getCurrentTimeLong(){
        java.util.Date dt = new java.util.Date();

        java.text.SimpleDateFormat sdf =
                new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(dt);

        return currentTime;

    }
    private void getLocation() {
        GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {

            double Latitude = gpsTracker.latitude;

            double Longitude =gpsTracker.longitude;

            String cityName = null;
            Geocoder gcd = new Geocoder(getBaseContext(),
                    Locale.getDefault());
            List<Address> addresses = new ArrayList<>();
            try {
                addresses = gcd.getFromLocation(Latitude, Longitude, 1);
                if (addresses.size() > 0)
                  //  System.out.println("dresscode"+addresses.get(0).getLocality());
                    cityName = addresses.get(0).getLocality();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String Address = "";
            try{
                Address = addresses.get(0).getAddressLine(0);
              //  System.out.println("dresscode"+addresses.get(0).getLocality());
            }catch (Exception e){
                Toast.makeText(getApplicationContext(),"Not connected",Toast.LENGTH_SHORT).show();
            }

            System.out.println("dresscode "+Address);
            String start_place = gpsTracker.getLocality(this);

            String start_lat = String.valueOf(Latitude);
            String start_long = String.valueOf(Longitude);

            SharedPreferences.Editor ed = sp.edit();
            ed.putString("Latitude",start_lat);
            ed.putString("Longitude",start_long);
            ed.putString("place",Address);
            ed.commit();

        } else {

            gpsTracker.showSettingsAlert();
        }

    }
    private PersonDTO personDTO;
    private DemorgraphicDTO  demographicDTO;
    private List<DemorgraphicDTO> list = new ArrayList<DemorgraphicDTO>();
    List<DemorgraphicDTO> lstDemo;
    private DemorgraphicTableAdapter dta;

    private SurveyActionTableAdapter surveyActionTableAdapter;
    private SurveyActionDTO surveyActionDTO;

    private ActiveLogTableAdapter activeLogTableAdapter;
    private FormCountTableAdapter fcta;

    //
    private List<SaveAnswersDTO> saveAnswersDTOs = new ArrayList<SaveAnswersDTO>();
    private List<ActivityLogDTO> activityLogDTOs = new ArrayList<ActivityLogDTO>();
    private SaveAnswerTableAdapter sata;
    private ContactTableAdapter cta;
    private String personID;
    private List<PersonDTO> pList;
    private boolean  destroy = false;
    private boolean  saved = false;
    List<PersonDTO> p;
    List<ContactDTO> cList;
    private  AlertDialog deleteDialog;
    private  AlertDialog deleteShortDialog;
    private int totalContacts;
    boolean active;
    int amt = 0;
    int counting = 0;
    private int thisFormID;
    private FormDTO formDTO;

}
