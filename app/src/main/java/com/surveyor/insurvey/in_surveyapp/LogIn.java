package com.surveyor.insurvey.in_surveyapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dto.ConnectionDetector;
import dto.UserDTO;
import sql.UserTableAdapter;


public class LogIn extends ActionBarActivity {
    private Button btnLogin;
    private EditText LoginEmail;
    private EditText LoginPassword;
    private Context ctx;
    private SharedPreferences sp;
    private ProgressDialog dialog;
    private String uEmail;
    private String uPassword;
    private int userID;
    private Button btnGotoRegister;
    private List<UserDTO> userDTOs;
    private UserTableAdapter uta;
    private TextView forgorPassword;
    private boolean isInternetPresent = false;

    private EditText edtForgetPassword;
    private TextView tv_message;
    private boolean send = false;
    private Button btnSubmitForgotPassword;
    private Button btnCancelForgetPassword;
    private  AlertDialog deleteDialog;
    // private Vibrator v;
    // Connection detector class
    public Vibrator vibrator;
    ConnectionDetector cd;
    private static final String LOG = LogIn.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_login);
      //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setHomeButtonEnabled(true);

        setLoginFields();
    }

    private void setLoginFields() {
        ctx = getApplicationContext();
        sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        uta = new UserTableAdapter(ctx);
        //v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
        LoginEmail = (EditText) findViewById(R.id.edtEmail);
        LoginPassword = (EditText) findViewById(R.id.edtLoginPassword);
        btnGotoRegister = (Button)findViewById(R.id.btnSignUp);
        btnLogin = (Button) findViewById(R.id.btnRegisterButton);
        userID = sp.getInt("userID",0);
        userDTOs = new ArrayList<UserDTO>();
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        forgorPassword = (TextView)findViewById(R.id.forgortPassword);
        forgorPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

               uEmail = LoginEmail.getText().toString();
               uPassword = LoginPassword.getText().toString();
               // uEmail = "enquiries@insurvey.co.za";
              //  uPassword = "insurvey1";
                if (isInternetPresent) {
                    // Internet Connection is Present

                    // Vibrate for 500 milliseconds
                    Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(200);
                    getLoginMySQL();
                   //Toast.makeText(getApplicationContext(),"Internet ",Toast.LENGTH_SHORT).show();
                } else {
                    // Internet connection is not present
                    // Ask user to connect to Internet
                    LoginFromSqlLite1(uEmail, uPassword);
                   // Toast.makeText(getApplicationContext(),"SQLlite ",Toast.LENGTH_SHORT).show();
                }
                //

            }
        });
        btnGotoRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),RegisterActivity.class));
               // finish();
            }
        });
    }
    private void setDialog() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(
                R.layout.forget_pass, null);
        deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
       // deleteDialog.setCancelable(false);
        deleteDialog.setCanceledOnTouchOutside(true);
        //setFinishOnTouchOutside(false);

        edtForgetPassword = (EditText) deleteDialogView.findViewById(R.id.edtForgotPassword);
        btnSubmitForgotPassword = (Button) deleteDialogView.findViewById(R.id.btnForgotPassword);
       // btnCancelForgetPassword = (Button) deleteDialogView.findViewById(R.id.btnCancelForgetPassword);

        btnSubmitForgotPassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String email = edtForgetPassword.getText().toString();
                getForgetPassword(email,edtForgetPassword);
                if(send){
                    deleteDialog.dismiss();
                }
            }
        });

        deleteDialog.show();
    }
    private void LoginFromSqlLite1(String u,String pass) {
        uta.open();
        userDTOs = uta.getListUserID(u, pass);
        uta.close();
        if(uEmail.length() > 0) {
            if (isEmailValid(uEmail) == true) {

                if (uPassword.length() > 0) {
                    dialog();
                    if (userDTOs.size() > 0) {
                        for (UserDTO c : userDTOs) {
                            SharedPreferences.Editor ed = sp.edit();
                            ed.putInt("userID", c.getUserID());
                            ed.putString("name", c.getName());
                            ed.putString("lastname", c.getLastname());
                            ed.commit();
                            dialog.dismiss();
                            startActivity(new Intent(LogIn.this, Main_OneActivity.class));
                        }
                    } else {
                        dialog.dismiss();
                        errorDialog();
                    }
                } else {
                    LoginPassword.setError("Please Enter Password.");
                }
            } else {
                LoginEmail.setError("Invalid email address.");
            }
        }else {
            LoginEmail.setError("Please enter email address.");
        }
}

    private void dialog() {
        dialog = new ProgressDialog(LogIn.this,
                AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Please wait");
        dialog.setTitle("Loggin In...");
        dialog.setIcon(R.drawable.login);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }
    private void forgetPasswordDialog() {
        dialog = new ProgressDialog(LogIn.this,
                AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Recover Password");
        dialog.setTitle("Please wait ...");
        dialog.setIcon(R.drawable.login);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }
    private void errorDialog(){
    AlertDialog alertDialog = new AlertDialog.Builder(this).create();
    alertDialog.setTitle("Error");
    alertDialog.setMessage("Invalid username or password");
    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
// here you can add functions
        }
    });
    alertDialog.setIcon(R.drawable.ic_error);
    alertDialog.show();
}
    private void errorEmail(String msg){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage(msg);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
// here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.ic_error);
        alertDialog.show();
    }
    private void shapEmail(String msg){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Password Recovery");
        alertDialog.setMessage(msg);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
// here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.warning_blue);
        alertDialog.show();
    }
    private void getLoginMySQL(){
     String url = "http://www.insurvey.co.za/api?requestType=8&email="+uEmail+"&password="+uPassword;
       // String url = "http://10.0.2.2/insurvey/index.php?requestType=8&email="+uEmail+"&password="+uPassword;
        if (isEmailValid(uEmail) == true) {
            if (uPassword.length() > 0) {
                dialog();
                JSONObject js = new JSONObject();

                //Log.i(LOG, js.toString());
                RequestQueue queue = Volley
                        .newRequestQueue(getApplicationContext());
                try{
                    StringRequest request = new StringRequest(url,
                            new Response.Listener<String>() {

                                @Override
                                public void onResponse(String arg0) {
                                    Log.i(LOG, arg0.toString());
                                    myVolley(arg0);
                                    dialog.dismiss();
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(
                                VolleyError arg0) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            toast("Network Connection Error");
                            toast("Check your network connection");
                            Log.i(LOG, arg0.toString());
                        }
                    });
                    queue.add(request);
                }catch (Exception e){
                    e.printStackTrace();
                }
            } else {
                LoginPassword.setError("Please Enter Password.");
            }
        } else {
            LoginEmail.setError("Invalid Email Address.");

        }
    }
    private void myVolley(String arg0) {
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                //toast(obj.getString("message"));
                JSONArray array = obj.getJSONArray("user");
                JSONObject jso = new JSONObject();
                for (int i = 0; i < array.length(); ++i) {
                    jso = array.getJSONObject(i);
                     user = new UserDTO(jso.getInt("id_user"),
                            jso.getString("name"),
                            jso.getString("username"),
                            jso.getString("password"),
                            jso.getString("phone"),
                            jso.getString("email"),
                            jso.getString("physical_address"),

                            jso.getString("postal_address"),
                            jso.getInt("id_company_fk"),
                            jso.getInt("id_permission_fk"),jso.getString("lastname"));
                UserDTO u = new UserDTO();
                    u.setCompanyName(jso.getString("companyName"));
                    u.setDescription(jso.getString("description"));
                    u.setCompanyEmail(jso.getString("copanyEmail"));
                    u.setCompanyPhone(jso.getString("companyPhone"));
                    u.setPhysical_address(jso.getString("physical_address"));
                    u.setPostal_address(jso.getString("postal_address"));

                    SharedPreferences.Editor ed = sp.edit();
                    ed.putInt("userID",user.getUserID());
                    ed.putString("name",user.getName());
                    ed.putString("lastname",user.getLastname());
                    ed.putString("username",user.getUsername());
                    ed.putString("password",user.getPassword());
                    ed.putString("email",user.getEmail());
                    ed.putString("phone",user.getPhone());
                    ed.putString("companyName",u.getCompanyName());
                    ed.putString("description",u.getDescription());
                    ed.putString("companyPhone",u.getCompanyPhone());
                    ed.putString("copanyEmail",u.getCompanyEmail());
                    ed.putString("physical_address",u.getPhysical_address());
                    ed.putString("postal_address",u.getPostal_address());

                    ed.commit();
                    dialog.dismiss();
                  // Toast.makeText(ctx,"Image "+u.getImage(),Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LogIn.this,
                            Main_OneActivity.class));
                    finish();
                }
                try {
                    InsertUserIntoSQLlite(user);
                   // toast("user inserted");
                }catch (Exception e){
                    toast("user not inserted");
                }
            } else {
               // toast_error(obj.getString("message"));
               dialog.dismiss();
                errorDialog();
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private void getForgetPassword(String email,EditText editText) {
        String url = "http://www.insurvey.co.za/api?requestType=11&email=" + email;
       //  String url = "http://10.0.2.2/insurvey/index.php?requestType=11&email="+email;

        //  http://10.0.2.2/insurvey/index.php?
        if (!email.equals("")) {
            if (isEmailValid(email)) {
                forgetPasswordDialog();
                JSONObject js = new JSONObject();
                //Log.i(LOG, js.toString());
                RequestQueue queue = Volley
                        .newRequestQueue(getApplicationContext());
                try {
                    StringRequest request = new StringRequest(url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String arg0) {
                                    forgetPasswordVolley(arg0);
                                    dialog.dismiss();
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(
                                VolleyError arg0) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            internetConnectionError();
                        }
                    });
                    queue.add(request);
                } catch (Exception e) {
                    dialog.dismiss();
                    e.printStackTrace();
                }

            } else {
                editText.setError("Invalid Email Address.");

            }
        } else {
            editText.setError("Enter Email Address.");
        }
    }
    private void forgetPasswordVolley(String arg0) {
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                shapEmail(obj.getString("message"));
                send = true;
                JSONArray array = obj.getJSONArray("pass");
                JSONObject jso = new JSONObject();
                for (int i = 0; i < array.length(); ++i) {
                    jso = array.getJSONObject(i);
                    user = new UserDTO(jso.getInt("id_user"),
                            jso.getString("name"),
                            jso.getString("username"),
                            jso.getString("password"),
                            jso.getString("phone"),
                            jso.getString("email"),
                            jso.getString("physical_address"),
                            jso.getString("postal_address"),
                            jso.getInt("id_company_fk"),
                            jso.getInt("id_permission_fk"),jso.getString("lastname"));
                    dialog.dismiss();
                   // Toast.makeText(ctx,"Email Address "+user.getEmail(),Toast.LENGTH_SHORT).show();
                }
            }

            else {
                errorEmail("Invalid email address!");
                dialog.dismiss();
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_log_in, menu);
        return true;
    }

    private void InsertUserIntoSQLlite(UserDTO userDTO){
        try {
            uta.open();
            uta.insertUsers(userDTO);
            uta.close();
        }catch (Exception e){
            //Toast.makeText(ctx,"Not inserted",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //  Toast.makeText(getApplicationContext(),"Setting",Toast.LENGTH_SHORT).show();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
    private void toast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,
                (ViewGroup) findViewById(R.id.toast_layout_root));
        TextView textView = (TextView) layout.findViewById(R.id.txtTOASTMessage);
        textView.setText(message);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    private void internetConnectionError(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Network Connection Error.Please Check your network connection");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
              //  startActivity(new Intent(LogIn.this,list_surveys_activity.class));
            }
        });
        alertDialog.setIcon(R.drawable.error32);
        alertDialog.show();
    }
    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    @Override
    public void onBackPressed() {
       // deleteDialog.dismiss();
        moveTaskToBack(true);
    }
    private UserDTO user;
}
