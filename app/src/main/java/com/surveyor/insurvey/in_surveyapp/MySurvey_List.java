package com.surveyor.insurvey.in_surveyapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import MyLibrary.LruBitmapCache;
import VolleyUtil.CustomVolleyRequestQueue;
import WriteData.ImageFile;
import dto.DemorgraphicDTO;
import dto.FormCountDTO;
import dto.FormDTO;
import dto.QuestionDTO;
import dto.QuestionImage;
import dto.SectionDTO;
import dto.SectionQuestionDTO;
import sql.DemorgraphicTableAdapter;
import sql.FormCountTableAdapter;
import sql.FormTableAdapter;
import sql.QuestionImageTableAdapter;
import sql.QuestionTableAdapter;
import sql.SectionQuestionTableAdapter;
import sql.SectionTableAdapter;

public class MySurvey_List extends ActionBarActivity {
    private String TAG = this.getClass().getSimpleName();
    private ListView lstView;
    private RequestQueue mRequestQueue;
    private ArrayList<FormDTO> formDTOs;
    private ArrayList<FormDTO> formDTOsql;
    private ArrayList<SectionDTO> sectionDTOs;
    private ArrayList<QuestionDTO> questionDTOs;
    private ArrayList<SectionQuestionDTO> sectionQuestionDTOs;
    private SectionDTO sectionDTO;
    private QuestionDTO questionDTO;
    private SharedPreferences sp;
    private SimpleDateFormat sdf;
    private FormDTO nm;
    private FormTableAdapter fta;
    private SurveyList sla;
    private QuestionTableAdapter qta;
    private Bitmap myProfileImage;
    private SectionTableAdapter sta;
    private SectionQuestionTableAdapter sqta;
    private int userID, formID, pos;
    private FormDTO formDTO;
    private ProgressDialog dialog;
    private List<FormDTO> lst;
    private List<Integer> intLst;

    private QuestionImageTableAdapter qita;

    String TITLES[] = {"Main Menu", "Download", "Survey List", "Surveys Actions","Completed","About", "Logout"};
    int ICONS[] = {R.drawable.ic_home,
            R.drawable.ic_download,
            R.drawable.ic_list,
            R.drawable.ic_actions,
            R.drawable.ic_completed,
            R.drawable.ic_about,
            R.drawable.ic_logout};
    String NAME = "";
    String EMAIL = "";
    int PROFILE = R.drawable.pro;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;
               private Context context;// Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;
    private static final String LOG = MySurvey_List.class.getSimpleName();
    static final int ANIMATION_DURATION = 400;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_survey__list);

        context = getApplicationContext();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        NAME = sp.getString("name","") +" "+sp.getString("lastname","");
        EMAIL = sp.getString("email","");
        setFields();
        setNavigationDrawer();
        mRequestQueue = Volley.newRequestQueue(this);
        RequestQueue queue = Volley
                .newRequestQueue(getApplicationContext());
        dialog();
        // String url = "http://10.0.2.2/insurvey/index.php?requestType=1&userID=" + sp.getInt("userID",0);
        String url = "http://www.insurvey.co.za/api?requestType=1&userID=" + sp.getInt("userID",0);

        StringRequest request = new StringRequest(url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String arg0) {
                        Log.i(LOG, arg0.toString());
                        voly(arg0);
                        result = new ArrayList<FormDTO>();
                        Set<String> titles = new HashSet<String>();

                        for(FormDTO item : formDTOs) {
                            if( titles.add(item.getName())) {
                                result.add(item);
                            }
                        }
                        setFormListAdapter(result);

                        dialog.dismiss();
                        // sla.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(
                    VolleyError arg0) {
                // TODO Auto-generated method stub
                //dialog.dismiss();
                // toast("Network Connection Error");
                //toast("Check your network connection");
                Log.i(LOG, arg0.toString());
            }
        });
        queue.add(request);
    }

    private void setFields() {
        sdf = new SimpleDateFormat("yyyy-MMM-DD hh:mm: a");
       // sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        fta = new FormTableAdapter(getApplicationContext());
        //int userID = sp.getInt("userID",0);
        lst = new ArrayList<FormDTO>();
        intArrayList = new ArrayList<Integer>();
        formDTOs = new ArrayList<FormDTO>();
        formDTOsql =  new ArrayList<FormDTO>();
        sectionDTOs = new ArrayList<SectionDTO>();
        questionDTOs = new ArrayList<QuestionDTO>();
        lstView = (ListView) findViewById(R.id.lstSurvey);
        sta = new SectionTableAdapter(getApplicationContext());
        qta = new QuestionTableAdapter(getApplicationContext());
        sqta = new SectionQuestionTableAdapter(getApplicationContext());
        qita = new QuestionImageTableAdapter(context);
        intLst = new ArrayList<Integer>();
        userID = sp.getInt("userID", 0);
        formID = sp.getInt("formID", 0);
        demographicDTO = new DemorgraphicDTO();
        dta = new DemorgraphicTableAdapter(getApplicationContext());

        fta.open();
        lst = fta.getFormList(userID);
        fta.close();
        for(FormDTO f:lst){
            intArrayList.add(f.getFormID());
        }


    }
    private void voly(String arg0) {
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("0")) {

                JSONArray array = obj.getJSONArray("forms");
                JSONObject jso = new JSONObject();
                for (int i = 0; i < array.length(); ++i) {
                    jso = array.getJSONObject(i);

                    formDTO = new FormDTO(jso.getInt("id_form"),
                            sp.getInt("userID", 0),
                            jso.getString("name"),
                            jso.getString("create_date"),
                            jso.getString("last_updated"),
                            jso.getInt("id_form_status_fk"),
                            jso.getString("description"),
                            jso.getString("message"),
                            jso.getInt("survey_limit"),
                            jso.getInt("total_surveys")
                           );
                    Log.i("TAG", " " + formDTO.getName());


                    fta.open();
                   // fta.updateForm(formDTO);
                    fta.updateSurveyProgress(formDTO);
                    fta.close();


                    if(!intArrayList.contains(formDTO.getFormID())){
                        formDTOs.add(formDTO);
                    }
                  //finish();

                }
            } else {
                // toast(obj.getString("message"));
                //   dialog.dismiss();
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    private void setNavigationDrawer(){
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        FormTableAdapter fta = new FormTableAdapter(getApplicationContext());
        FormCountTableAdapter ftac = new FormCountTableAdapter(getApplicationContext());
        List<FormDTO> frmList = new ArrayList<FormDTO>();
        List<FormCountDTO> countDTOs = new ArrayList<FormCountDTO>();
        fta.open();
        frmList = fta.getFormList(sp.getInt("userID",0));
        fta.close();
        ftac.open();
        countDTOs = ftac.getFormCountList(sp.getInt("userID",0));
        ftac.close();
        myProfileImage = BitmapFactory.decodeResource(getResources(), R.drawable.pro);
        String encodedProfileImage = sp.getString("encodedProfileImage","");
        if( !encodedProfileImage.equalsIgnoreCase("") ){
            byte[] b = Base64.decode(encodedProfileImage, Base64.DEFAULT);
            myProfileImage = BitmapFactory.decodeByteArray(b, 0, b.length);
            // imageConvertResult.setImageBitmap(bitmap);
        }
        String cnt[];
        if(sp.getString("fc","").equals("")){
            cnt = new String[]{"-1",String.valueOf(sp.getInt("formCount",0) - frmList.size()),String.valueOf(frmList.size()),"-1","-1","-1","-1"};
        }else{
            cnt = new String[] {"-1",sp.getString("fc",""),String.valueOf(frmList.size()),"-1","-1","-1","-1"};
        }
        //int cnt[] = {-1,sp.getInt("formCount",0) - frmList.size(),frmList.size(),-1,countDTOs.size(),-1,-1};
        mAdapter = new MyAdapter(TITLES,ICONS,NAME,EMAIL,PROFILE,getApplicationContext(),cnt,myProfileImage);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        final GestureDetector mGestureDetector = new GestureDetector(MySurvey_List.this, new GestureDetector.SimpleOnGestureListener() {

            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });


        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(),motionEvent.getY());

                if(child!=null && mGestureDetector.onTouchEvent(motionEvent)){
                  // Drawer.closeDrawers();
                   // Toast.makeText(MySurvey_List.this,"The Item Clicked is: "+recyclerView.getChildPosition(child),Toast.LENGTH_SHORT).show();
                    navigate(recyclerView.getChildPosition(child));
                    return true;

                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }


        });




        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.openDrawer,R.string.closeDrawer){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }



        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

    }
    private void navigate(int number){
        Context cta = getApplicationContext();
        Intent intent;
        switch (number) {
            case 0:
                intent = new Intent(cta, ProfileActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 1:
                intent = new Intent(cta,Main_OneActivity.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 2:
                Drawer.closeDrawers();
                break;
            case 3:
                    intent = new Intent(cta, List_Downloaded_Surveys.class);
                    Drawer.closeDrawers();
                    startActivity(intent);
                    finish();
                break;
            case 4:
                intent = new Intent(cta, SurveyActionActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 5:
                intent = new Intent(cta, CompletedSurveyActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 6:
                Toast.makeText(getApplicationContext(),"Still Under Construction",Toast.LENGTH_SHORT).show();
                Drawer.closeDrawers();
                break;
            case 7:
                intent = new Intent(cta,LogIn.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
        }

    }

    private void dialog() {
        dialog = new ProgressDialog(MySurvey_List.this,
                AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Please wait");
        dialog.setTitle("loading ...");
        dialog.setIcon(R.drawable.form_small);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_survey__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.main_menu) {
            startActivity(new Intent(MySurvey_List.this, Main_OneActivity.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setFormListAdapter(List<FormDTO> fl) {
           // fl.removeAll(lst);
            sla = new SurveyList(getApplicationContext(), R.layout.form_list, fl);
            lstView.setAdapter(sla);
            lstView.setVisibility(View.VISIBLE);

    }

    List<FormDTO> frmList;

    public class SurveyList extends ArrayAdapter<FormDTO> {
        int i = 0;
        private LayoutInflater mInflate;
        private final int mResource;
        private Context context;
        private List<FormDTO> list;
        private FormTableAdapter fta;
        private SectionQuestionTableAdapter sqta;
        private int lastPosition = -1;

        public SurveyList(Context context, int textViewResourceId,
                          List<FormDTO> list) {
            super(context, textViewResourceId, list);
            this.context = context;
            mResource = textViewResourceId;
            this.list = list;
            mInflate = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final View view;
            ViewHolder vh;
            final FormDTO dto = (FormDTO)getItem(position);

            if (convertView==null) {
                view = mInflate.inflate(R.layout.form_list, parent, false);
                setViewHolder(view);
            }
            else if (((ViewHolder)convertView.getTag()).needInflate) {
                view = mInflate.inflate(R.layout.form_list, parent, false);
                setViewHolder(view);
            }
            else {
                view = convertView;
            }

                    vh = (ViewHolder)view.getTag();
                    vh.TV_descript.setText(dto.getDescription());
                    if(dto.getCreateDate().equals("null")){

                        dto.setCreateDate("Not Specified");
                    }
                     if(dto.getLastUpdated().equals("null")){
                              dto.setLastUpdated("Not Specified");
                    }

                    vh.TV_dateCreated.setText("Created :" + dto.getCreateDate());
                    vh.TV_dateUpdated.setText("Update:" + dto.getLastUpdated());
                    vh.TV_name.setText(dto.getName());
                    vh.TV_survey_total.setText(""+dto.getSurvey_total());

                    vh.imgGo.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            getQuestionImages(dto.getFormID());
                            FormTableAdapter fta = new FormTableAdapter(getApplicationContext());
                            FormCountTableAdapter ftac = new FormCountTableAdapter(getApplicationContext());
                            List<FormDTO> frmList = new ArrayList<FormDTO>();
                            List<FormCountDTO> countDTOs = new ArrayList<FormCountDTO>();
                            Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(200);
                            fta = new FormTableAdapter(getContext());
                            fta.open();
                            fta.insertForm(dto);
                            fta.close();
                            getVolley(dto.getFormID());
                            getDemographic(dto.getFormID());
                            Log.i("FormID ", "" + dto.getFormID());
                            deleteCell(view, position);
                            fta.open();
                            frmList = fta.getFormList(sp.getInt("userID",0));
                            fta.close();
                            ftac.open();
                            countDTOs = ftac.getFormCountList(sp.getInt("userID",0));
                            ftac.close();
                            String cnt[] = {"-1",String.valueOf(sp.getInt("formCount",0) - frmList.size()),String.valueOf(frmList.size()),"-1",String.valueOf(countDTOs.size()),"-1","-1"};
                            mAdapter = new MyAdapter(TITLES,ICONS,NAME,EMAIL,PROFILE,getApplicationContext(),cnt,myProfileImage);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
                            mRecyclerView.setAdapter(mAdapter);
                            SharedPreferences.Editor ed = sp.edit();
                            Toast.makeText(getContext(), dto.getName() + " downloaded successfully", Toast.LENGTH_SHORT).show();
                        }
                    });


            Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
            view.startAnimation(animation);
            lastPosition = position;
            return view;
        }

        class ViewHolder {
            public boolean needInflate;
            TextView TV_descript;
            TextView TV_dateCreated;
            TextView TV_dateUpdated;
            TextView TV_survey_total;
            TextView TV_name;
            ImageView imgGo;
        }
        private void setViewHolder(View view) {
            ViewHolder vh = new ViewHolder();
            vh.TV_descript = (TextView) view.findViewById(R.id.tv_desc);
            vh.TV_dateCreated = (TextView) view.findViewById(R.id.tv_created);
            vh.TV_dateUpdated = (TextView) view.findViewById(R.id.tv_updated);
            vh.TV_name = (TextView) view.findViewById(R.id.tv_names1);
            vh.TV_survey_total = (TextView) view.findViewById(R.id.tv_survey_total);
            vh.imgGo = (ImageView) view.findViewById(R.id.img_do);
            vh.needInflate = false;
            view.setTag(vh);
        }
        private void volySecQues(String arg0) {
            sqta = new SectionQuestionTableAdapter(context);
            SectionQuestionDTO dto = null;
            try {
                JSONObject obj = new JSONObject(arg0);
                if (obj.getString("success").equals("0")) {
                    Log.i("TAG IN", obj.getString("message"));

                    JSONArray array = obj.getJSONArray("questions");
                    JSONObject jso = new JSONObject();
                    for (int i = 0; i < array.length(); ++i) {
                        jso = array.getJSONObject(i);

                         dto = new SectionQuestionDTO(jso.getInt("id_section"),
                                jso.getInt("id_form_fk"),
                                jso.getInt("id_question"), jso.getString("name"),
                                jso.getString("json"), jso.getString("question"), jso.getString("description"));

                        Log.i("Questions ", " " + dto.getQuestion());
                        sqta.open();
                        sqta.insertSectionsQuestions(dto);
                        sqta.close();
                    }



                } else {
                    // toast(obj.getString("message"));
                    //dialog.dismiss();
                    Log.i("TAG Out", obj.getString("message"));

                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();


            }
        }
        private void deleteCell(final View v, final int index) {
            AnimationListener al = new AnimationListener() {
                @Override
                public void onAnimationEnd(Animation arg0) {
                    result.remove(index);
                    ViewHolder vh = (ViewHolder)v.getTag();
                    vh.needInflate = true;
                    sla.notifyDataSetChanged();
                }
                @Override public void onAnimationRepeat(Animation animation) {}
                @Override public void onAnimationStart(Animation animation) {}
            };

            collapse(v, al);
        }

        private void collapse(final View v, AnimationListener al) {
            final int initialHeight = v.getMeasuredHeight();
            final Animation out = AnimationUtils.loadAnimation(context,R.anim.push_left_out);
            Animation anim = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    if (interpolatedTime == 1) {
                        v.setVisibility(View.GONE);
                    }
                    else {
                        v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                        v.requestLayout();
                    }
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };

            if (al!=null) {

                out.setAnimationListener(al);
            }

            anim.setDuration(ANIMATION_DURATION);
            v.startAnimation(out);
        }
        private void insertSectionQuestionsToSqlLite(SectionQuestionDTO s) {
            //FormDTO f  = new FormDTO();
            sqta.open();
            sqta.insertSectionsQuestions(s);
            sqta.close();
        }

        private void getVolley(int formID) {
            RequestQueue queue = Volley
                    .newRequestQueue(context);
            //dialog();
            String urlSecQues = "http://www.insurvey.co.za/api?requestType=4&formID=" + formID;
           //String urlSecQues = "http://10.0.2.2/insurvey/index.php?requestType=4&formID="+formID;
            StringRequest requestSecQues = new StringRequest(urlSecQues,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String arg0) {
                            //Log.i(LOG, arg0.toString());
                            volySecQues(arg0);
                            //dialog.dismiss();
                            // sla.notifyDataSetChanged();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(
                        VolleyError arg0) {
                    // TODO Auto-generated method stub
                    // dialog.dismiss();
                    // toast("Network Connection Error");
                    //toast("Check your network connection");
                    // Log.i(LOG, arg0.toString());
                }
            });
            queue.add(requestSecQues);
        }

        private void dialog() {
            dialog = new ProgressDialog(context,
                    AlertDialog.THEME_HOLO_DARK);
            dialog.setMessage("Please wait");
            dialog.setMessage("Loading...");
            dialog.setIndeterminate(true);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(true);
            dialog.show();
        }

        private ProgressDialog dialog;

    }
    private void getDemographic(int formID){

        //String url = "http://10.0.2.2/insurvey/index.php?requestType=21&formID="+formID;
        String url = "http://www.insurvey.co.za/api?requestType=21&formID="+formID;
        //dialog();
        JSONObject js = new JSONObject();

        //Log.i(LOG, js.toString());
        RequestQueue queue = Volley
                .newRequestQueue(getApplicationContext());
        try{
            StringRequest request = new StringRequest(url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String arg0) {
                            Log.i("LOG", arg0.toString());
                            getDemorgraphicVolley(arg0);
                            dialog.dismiss();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(
                        VolleyError arg0) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                    Log.i("LOG", arg0.toString());
                }
            });
            queue.add(request);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    private void getDemorgraphicVolley(String arg0) {

        DemorgraphicDTO d = new DemorgraphicDTO();
        lstDemo = new ArrayList<>();
        SharedPreferences.Editor ed = sp.edit();
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("0")) {
                JSONArray array = obj.getJSONArray("demographic");

                for (int i = 0; i < array.length(); ++i) {

                    JSONObject jso = array.getJSONObject(i);

                    // (int demorgraphicID, int formID, String race, String age, String gender)

                    demographicDTO = new DemorgraphicDTO(jso.getInt("id_form_fk"),
                            jso.getString("name"),
                            jso.getString("demograph"));

                    if(demographicDTO.getName().equals("Gender")){
                        Log.i("Gender %%%% ",""+i);
                        ed.putString("gender" + i, demographicDTO.getDemograpg());
                        ed.commit();
                    }

                    if(demographicDTO.getName().equals("Age")){
                        Log.i("Age ",""+i);
                        ed.putString("age"+i,demographicDTO.getDemograpg());
                        ed.commit();
                    }
                    if(demographicDTO.getName().equals("Race")){
                        Log.i("Race ",""+i);
                        ed.putString("race"+i,demographicDTO.getDemograpg());
                        ed.commit();
                    }

                    if(demographicDTO.getName().equals("Marital ")){
                        Log.i("Marital ",""+i);
                        ed.putString("marital"+i,demographicDTO.getDemograpg());
                        ed.commit();
                    }

                    if(demographicDTO.getName().equals("Employment")){
                        Log.i("Employment ",""+i);
                        ed.putString("employment"+i,demographicDTO.getDemograpg());
                        ed.commit();
                    }

                    if(demographicDTO.getName().equals("Income ")){
                        Log.i("Income ",""+demographicDTO.getDemograpg());
                        ed.putString("income"+i,demographicDTO.getDemograpg());
                        ed.commit();
                    }

                    if(demographicDTO.getName().equals("PersonalData")){
                        Log.i("PersonalData ",""+i);
                        ed.putString("personalData"+i,demographicDTO.getDemograpg());
                        ed.commit();
                    }

                    if(demographicDTO.getName().equals("Inhousehold")){
                        Log.i("Inhousehold ",""+i);
                        ed.putString("Inhousehold"+i,""+demographicDTO.getDemograpg());
                        ed.commit();
                    }

                    if(demographicDTO.getName().equals("Municipality")){
                        Log.i("Municipality ",""+i);
                        ed.putString("Municipality"+i,""+demographicDTO.getDemograpg());
                        ed.commit();
                    }

                }


                for(int i = 0;i < array.length();++i){
                    Log.i("Employment :"+i,sp.getString("employment"+i,""));
                    Log.i("Marital  :"+i,sp.getString("marital"+i,""));

                    d.setFormID(demographicDTO.getFormID());
                    d.setRace(sp.getString("race" + i, ""));
                    d.setAge(sp.getString("age" + i, ""));
                    d.setGender(sp.getString("gender" + i, ""));
                    d.setMarital(sp.getString("marital" + i, ""));
                    d.setEmployment(sp.getString("employment" + i, ""));

                    d.setIncome(sp.getString("income" + i, ""));
                    d.setPersonalData(sp.getString("personalData" + i, ""));

                    d.setInhousehold(sp.getString("Inhousehold" + i, ""));
                    d.setMunicipality(sp.getString("Municipality" + i, ""));

                    InsertDermographic(d);

                }
                for(int i = 0;i < array.length();++i){
                    Log.i("Employment :"+i,sp.getString("employment"+i,""));
                    Log.i("Marital  :"+i,sp.getString("marital"+i,""));

                    d.setFormID(demographicDTO.getFormID());
                    d.setRace(sp.getString("race" + i, ""));
                    d.setAge(sp.getString("age" + i, ""));
                    d.setGender(sp.getString("gender" + i, ""));
                    d.setMarital(sp.getString("marital" + i, ""));
                    d.setEmployment(sp.getString("employment" + i, ""));
                    d.setIncome(sp.getString("income" + i, ""));
                    d.setPersonalData(sp.getString("personalData" + i, ""));

                    d.setInhousehold(sp.getString("Inhousehold" + i, ""));
                    d.setMunicipality(sp.getString("Municipality" + i, ""));
                    InsertDermographic(d);

                } for(int i = 0;i < array.length();++i){

                    sp.edit().remove("race" + i).commit();
                    sp.edit().remove("age" + i).commit();
                    sp.edit().remove("gender" + i).commit();
                    sp.edit().remove("marital" + i).commit();
                    sp.edit().remove("income" + i).commit();
                    sp.edit().remove("personalData" + i).commit();
                    sp.edit().remove("Inhousehold" + i).commit();
                    sp.edit().remove("Municipality" + i).commit();
                }
            } else {
                // dialogError(obj.getString("message"));
                dialog.dismiss();

                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private void InsertDermographic(DemorgraphicDTO d){
        dta.open();
        dta.insertDemographic(d);
        dta.close();

    }

    private void downloadImages(final String fileName){

        RequestQueue mQueue = CustomVolleyRequestQueue.getInstance(getApplicationContext()).getRequestQueue();

        ImageLoader imageLoader = new ImageLoader(mQueue,new LruBitmapCache());

        String url = "http://surveys.insurvey.co.za/includes/uploads/question_images/thump/" + fileName;

        Log.i("Baes ",url);

        imageLoader.get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {

                ImageFile.saveImage(getApplicationContext(),response.getBitmap(),fileName);
             //   ImageFile.saveImageQuestionImage(getApplicationContext(), response.getBitmap(), fileName);

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    public  void  getQuestionImages(final int formID) {

        RequestQueue mQueue = CustomVolleyRequestQueue.getInstance(context).getRequestQueue();
        String url = "http://www.insurvey.co.za/api/index.php";
            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.i("balesa ", "" + response);

                            try {
                                JSONObject obj = new JSONObject(response);

                                if(obj.optString("success").equals("0")){

                                    JSONArray item = obj.getJSONArray("question_images");

                                    for(int i = 0;i < item.length();i++){

                                        JSONObject singleObj = item.getJSONObject(i);

                                        int id_question_image = singleObj.optInt("id_question_image", 0);

                                        String image = singleObj.optString("image", "");

                                        int id_question_fk = singleObj.optInt("id_question_fk", 0);

                                        int id_form_fk = singleObj.optInt("id_form_fk",0);

                                        image = image.replace("./uploads/","");

                                        JSONObject imageObject= new JSONObject(image);

                                        JSONObject imageContent = imageObject.getJSONObject("content");

                                        JSONArray thumbsArray = imageContent.getJSONArray("thumbs");

                                        for(int k = 0;k < thumbsArray.length(); k++){

                                            String imageLocation = thumbsArray.getString(k);

                                            downloadImages(imageLocation);

                                            QuestionImage qimage = new QuestionImage(id_question_image
                                                    ,imageLocation
                                                    ,id_question_fk
                                                    ,id_form_fk);

                                            qita.open();
                                            qita.insertQuestionImage(qimage);
                                            qita.close();
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();

                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams()
                {
                    Log.i("balesa formID", "" + formID);
                    Map<String, String>  params = new HashMap<>();
                    params.put("requestType", "30");
                    params.put("id_form_fk",String.valueOf(formID));
                    return params;
                }
            };

            mQueue.add(postRequest);


    }

    private List<Integer> intArrayList;
    private DemorgraphicDTO  demographicDTO;
    private List<DemorgraphicDTO> list = new ArrayList<DemorgraphicDTO>();
    List<DemorgraphicDTO> lstDemo;
    private DemorgraphicTableAdapter dta;
    private List<FormDTO> result = new ArrayList<>();
}

