package com.surveyor.insurvey.in_surveyapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dto.ActivityLogDTO;
import dto.FormCountDTO;
import dto.FormDTO;
import dto.PersonDTO;
import dto.SaveAnswersDTO;
import sql.ActiveLogTableAdapter;
import sql.FormCountTableAdapter;
import sql.FormTableAdapter;
import sql.PersonTableAdapter;
import sql.SaveAnswerTableAdapter;
import sql.SurveyActionTableAdapter;


public class Main_OneActivity extends ActionBarActivity {
    private Button btnProfile,startWork;
    private Button btnDownloadSurvey;
    private Button btnSubmitSurvey;
    private Button btnAbout;
    private Button surveyList;
    private boolean saved = false;
    private ProgressDialog dialog;
    private PersonDTO personDTO;
    private PersonTableAdapter pta;
    private ActiveLogTableAdapter atlta;
    private List<PersonDTO> p;
    private List<ActivityLogDTO> activityLogDTOs;
    private SharedPreferences sp;
    private FormTableAdapter ftac;
    private RequestQueue mRequestQueue;
    private FormDTO formDTO;
    private Bitmap myProfileImage;
    private List<FormDTO> lst;
    private int cnt[];
    private boolean found = false;
     List<FormDTO> frmList;

    String TITLES[] = {"Main Menu", "Download", "Survey List", "Surveys Actions","Completed","About", "Logout"};
    int ICONS[] = {R.drawable.ic_home,
            R.drawable.ic_download,
            R.drawable.ic_list,
            R.drawable.ic_actions,
            R.drawable.ic_completed,
            R.drawable.ic_about,
            R.drawable.ic_logout};

    //Similarly we Create a String Resource for the name and email in the header view
    //And we also create a int resource for profile picture in the header view

    String NAME = "";
    String EMAIL = "";
    int PROFILE = R.drawable.pro;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dash);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        NAME = sp.getString("name","") +" "+sp.getString("lastname","");
        EMAIL = sp.getString("email","");
        lst = new ArrayList<FormDTO>();
        getFormsCount();
        setFields();
        //getFormsCount();

    }
    private void setFields() {


        getFormsCount();

        btnProfile = (Button) findViewById(R.id.profileButton);
        btnDownloadSurvey = (Button) findViewById(R.id.downloadButton);
        btnSubmitSurvey = (Button) findViewById(R.id.submitCompleted);
        btnAbout = (Button) findViewById(R.id.aboutButton);
        startWork =(Button) findViewById(R.id.startWork);
        pta = new PersonTableAdapter(getApplicationContext());
        sta = new SaveAnswerTableAdapter(getApplicationContext());
        atlta = new ActiveLogTableAdapter(getApplicationContext());
        surveyList = (Button) findViewById(R.id.surveylistButton);
        fta = new FormCountTableAdapter(getApplicationContext());
        stat = new SurveyActionTableAdapter(getApplicationContext());
        ftac = new FormTableAdapter(getApplicationContext());
        myProfileImage = BitmapFactory.decodeResource(getResources(), R.drawable.pro);

        ftac.open();
        frmList  = ftac.getFormList(sp.getInt("userID",0));
        ftac.close();
        fta.open();
        countDTOs = fta.getFormCountList(sp.getInt("userID",0));

        SharedPreferences.Editor ed = sp.edit();
        ed.putInt("SQLFCount",countDTOs.size()).commit();
        fta.close();

        pta.open();
        p = pta.getPersonList();
        pta.close();

        atlta.open();
        activityLogDTOs = atlta.getList();
        atlta.close();
        Log.i("ActivityLogDTOs :",activityLogDTOs.toString());
//        sta.open();
//        saveAnswersDTOs = sta.getSavedAnswers(sp.getInt("userID",0));
//        sta.close();

       // Toast.makeText(getApplicationContext(),"activityLogDTOs size "+activityLogDTOs.size(),Toast.LENGTH_SHORT).show();
        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                //Toast.makeText(getApplicationContext(), "Still Under Construction", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Main_OneActivity.this, ProfileActivity.class));
                finish();
            }
        });
        btnDownloadSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                startActivity(new Intent(Main_OneActivity.this, MySurvey_List.class));
            }
        });
        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                Toast.makeText(getApplicationContext(), "Still Under Construction", Toast.LENGTH_SHORT).show();
                // startActivity(new Intent(Main_OneActivity.this,.class));
            }
        });
        surveyList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                startActivity(new Intent(Main_OneActivity.this, List_Downloaded_Surveys.class));
                finish();
            }
        });
        startWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                startActivity(new Intent(Main_OneActivity.this, SurveyActionActivity.class));
                finish();
            }
        });
        btnSubmitSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),CompletedSurveyActivity.class));
                finish();
            }
        });

        setNavigationDrawer();
    }
    private void getFormsCount(){

    }
    private void voly(String arg0) {

        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("0")) {
                found = true;
                JSONArray array = obj.getJSONArray("forms");
                JSONObject jso = new JSONObject();
                for (int i = 0; i < array.length(); ++i) {
                    jso = array.getJSONObject(i);

                    formDTO = new FormDTO(jso.getInt("id_form"),
                            sp.getInt("userID",0),
                            jso.getString("name"),
                            jso.getString("create_date"),
                            jso.getString("last_updated"),
                            jso.getInt("id_form_status_fk"),
                            jso.getString("description"),
                            jso.getString("message"),
                            jso.getInt("survey_limit"),
                            jso.getInt("total_surveys"));
                    c++;
                    Log.i("TAG", " " + formDTO.getName());
                    Log.i("c", " " + c);
                    lst.add(formDTO);
                    Log.i("size ", " " +lst.size());

                   ftac.open();
                    ftac.updateForm(formDTO);
                    ftac.close();
                }
                dialog.dismiss();
            } else {
                // toast(obj.getString("message"));
                //   dialog.dismiss();
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    private void setNavigationDrawer(){
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);
        mRequestQueue = Volley.newRequestQueue(this);
        RequestQueue queue = Volley
                .newRequestQueue(getApplicationContext());
        dialog();
        //String url = "http://10.0.2.2/insurvey/index.php?requestType=1&userID=" + sp.getInt("userID",0);
        String url = "http://www.insurvey.co.za/api?requestType=1&userID=" + sp.getInt("userID",0);

        StringRequest request = new StringRequest(url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String arg0) {
                        voly(arg0);
                        //  Toast.makeText(getApplicationContext(),"Count "+lst.size(),Toast.LENGTH_SHORT).show();

                        String encodedProfileImage = sp.getString("encodedProfileImage","");
                        if( !encodedProfileImage.equalsIgnoreCase("") ){
                            byte[] b = Base64.decode(encodedProfileImage, Base64.DEFAULT);
                            myProfileImage = BitmapFactory.decodeByteArray(b, 0, b.length);
                           // imageConvertResult.setImageBitmap(bitmap);
                        }

                        String cnt[] = {"-1",String.valueOf(lst.size() - frmList.size()),String.valueOf(frmList.size()),"-1","-1","-1","-1"};
                        mAdapter = new MyAdapter(TITLES,ICONS,NAME,EMAIL,PROFILE,getApplicationContext(),cnt,myProfileImage);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
                        // And passing the titles,icons,header view name, header view email,
                        // and header view profile picture
                        SharedPreferences.Editor ed = sp.edit();
                        ed.putInt("formCount",lst.size());

                        ed.remove("fc");
                        ed.commit();
                        mRecyclerView.setAdapter(mAdapter);
                        dialog.dismiss();// Se
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(
                    VolleyError arg0) {

                String encodedProfileImage = sp.getString("encodedProfileImage","");
                if( !encodedProfileImage.equalsIgnoreCase("") ){
                    byte[] b = Base64.decode(encodedProfileImage, Base64.DEFAULT);
                    myProfileImage = BitmapFactory.decodeByteArray(b, 0, b.length);
                    // imageConvertResult.setImageBitmap(bitmap);
                }
                String fc = "?";
                SharedPreferences.Editor ed = sp.edit();
                ed.putString("fc",fc);
                ed.commit();
                String cnt[] = {"-1",fc,String.valueOf(frmList.size()),"-1","-1","-1","-1"};
                mAdapter = new MyAdapter(TITLES,ICONS,NAME,EMAIL,PROFILE,getApplicationContext(),cnt,myProfileImage);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
                // And passing the titles,icons,header view name, header view email,
                // and header view profile picture

                ed.putInt("formCount",lst.size());
                ed.commit();
                mRecyclerView.setAdapter(mAdapter);
                dialog.dismiss();// Se
                // TODO Auto-generated method stub
            }
        });
        queue.add(request);
                                // Letting the system know that the list objects are of fixed size
       // Toast.makeText(getApplicationContext(),"My flop "+lst.size(),Toast.LENGTH_SHORT).show();

        final GestureDetector mGestureDetector = new GestureDetector(Main_OneActivity.this, new GestureDetector.SimpleOnGestureListener() {

            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });

        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(),motionEvent.getY());

                if(child!=null && mGestureDetector.onTouchEvent(motionEvent)){
                   // Drawer.closeDrawers();
                   // Toast.makeText(Main_OneActivity.this,"The Item Clicked is: "+recyclerView.getChildPosition(child),Toast.LENGTH_SHORT).show();
                    navigate(recyclerView.getChildPosition(child));
                    return true;
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }


        });


        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.openDrawer,R.string.closeDrawer){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }



        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

    }
    private void navigate(int number){

        Context cta = getApplicationContext();
        Intent intent;
        switch (number) {
            case 0:
                intent = new Intent(cta, ProfileActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 1:
                Drawer.closeDrawers();
                break;
            case 2:

                intent = new Intent(cta, MySurvey_List.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;

            case 3:
                    intent = new Intent(cta, List_Downloaded_Surveys.class);
                    // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Drawer.closeDrawers();
                    startActivity(intent);
                    finish();

                break;


            case 4:
                intent = new Intent(cta, CompletedSurveyActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();

            case 5:
                intent = new Intent(cta, CompletedSurveyActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 6:
                Toast.makeText(getApplicationContext(),"Still Under Construction",Toast.LENGTH_SHORT).show();
                Drawer.closeDrawers();
                break;
            case 7:
                intent = new Intent(cta,LogIn.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
        }

    }

    private void vibrate(){
    Vibrator x = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    x.vibrate(200);
}

    private void myVolleySaveSurveyActions(String arg0) {

        try {
            Log.i("Res ",arg0);
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                JSONArray array = obj.getJSONArray("survey_actions");
                Log.i("TAG", obj.getString("message"));
               // toast_error(obj.getString("message"));

            } else {
                toast_error(obj.getString("message"));
                dialog.dismiss();


            }
        } catch (JSONException e) {

            // TODO Auto-generated catch block
            dialog.dismiss();
            e.printStackTrace();
        }
    }

    private void infoDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Arlet");
        alertDialog.setMessage("Answers Saved Successfully");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
// here you can add functions
            }
        });
        alertDialog.setIcon(R.drawable.info32);
        alertDialog.show();
    }
    private void dialog() {
        dialog = new ProgressDialog(Main_OneActivity.this,
                AlertDialog.THEME_HOLO_DARK);
        dialog.setMessage("Please wait");
        dialog.setTitle("loading ...");
        dialog.setIcon(R.drawable.form_small);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }
    private void saveProgressDialog() {
        dialog = new ProgressDialog(Main_OneActivity.this,
                AlertDialog.THEME_HOLO_DARK);
        dialog.setTitle("Please wait");
        dialog.setMessage("Saving surveys...");
        dialog.setIcon(R.drawable.form_small);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main__one, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



//    private void saveOrView(){
//        final String[] option;
//       // Toast.makeText(getApplicationContext(),"Answers "+countDTOs.size(),Toast.LENGTH_SHORT).show();
//        if(saveAnswersDTOs.size() == 0){
//            option = new String[]{"Close"};
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item,option);
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("No Saved Surveys");
//            builder.setIcon(R.drawable.form_small);
//            builder.setAdapter(adapter,new DialogInterface.OnClickListener(){
//                public void onClick(DialogInterface dialog,int which){
//                    switch (which){
//                        case 0:
//                            dialog.dismiss();
//                            break;
//                    }
//                }
//            });
//            final AlertDialog dialog1 = builder.create();
//            dialog1.show();
//        }else{
//          option = new String[]{"Save","Save Later"};
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item,option);
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle(countDTOs.size()+" Surveys");
//            builder.setIcon(R.drawable.form_small);
//            builder.setAdapter(adapter,new DialogInterface.OnClickListener(){
//                public void onClick(DialogInterface dialog,int which){
//                    switch (which){
//                        case 0:
//                            try{
//                                saveSurvey();
//                            }catch(Exception e){
//                                dialog.dismiss();
//                            }
//
//                            break;
//                        case 1:
//                            dialog.dismiss();
//                            break;
//                    }
//                }
//            });
//            final AlertDialog dialog1 = builder.create();
//            dialog1.show();
//        }
//    }
//
//    private void saveSurvey() {
//
//        RequestQueue queue = Volley
//                .newRequestQueue(getApplicationContext());
//        if (saveAnswersDTOs.size() > 0) {
//            if (CheckInternet(getApplicationContext())) {
//                try {
//                    boolean saved = false;
//                    saveProgressDialog();
//                    for (SaveAnswersDTO al : saveAnswersDTOs) {
//
//                        // String url = "http://10.0.2.2/insurvey/index.php?requestType=5&answer=" + cn.getAnswer() + "&id_question_fk=" + Integer.toString(cn.getQuestionID()) + "&id_activity_log_fk=" + Integer.toString(cn.getActivityLogID()) + "&id_person_fk=" + Integer.toString(cn.getPersonID());
//
//                        String url = "http://www.insurvey.co.za/api?requestType=17&id_user_fk=" + Integer.toString(al.getUserID()) + "&id_form_fk=" + Integer.toString(al.getFormID()) + "&id_activity_fk=" + al.getActivityID() + "&answer=" + al.getAnswer() + "&id_question_fk=" + Integer.toString(al.getQuestionID()) + "&id_activity_log_fk=1&id_person_fk=" + Integer.toString(al.getPersonID());
//                        //  String url = "http://10.0.2.2/insurvey/index.php?requestType=17&id_user_fk="+Integer.toString(al.getUserID())+"&id_form_fk="+Integer.toString(al.getFormID())+"&id_activity_fk="+al.getActivityID()+"&answer=" + al.getAnswer() + "&id_question_fk=" + Integer.toString(al.getQuestionID()) + "&id_activity_log_fk=1&id_person_fk=" + Integer.toString(al.getPersonID());
//                        //   String u = "http://10.0.2.2/insurvey/index.php?requestType=17&id_user_fk="+Integer.toString(al.getUserID())+"&id_form_fk="+Integer.toString(al.getFormID())+"&id_activity_fk="+al.getActivityID()+"&answer="+al.getAnswer() +"&id_question_fk="+Integer.toString(al.getQuestionID())+"&id_activity_log_fk=1&id_person_fk="+Integer.toString(al.getPersonID());
//                        // Log.i("",u);
//                        JSONObject js = new JSONObject();
//
//                        //Log.i(LOG, js.toString());
//
//                        try {
//                            StringRequest request = new StringRequest(url,
//                                    new Response.Listener<String>() {
//
//                                        @Override
//                                        public void onResponse(String arg0) {
//                                            //  Log.i(LOG, arg0.toString());
//                                            count++;
//                                            myVolley(arg0);
//                                            try {
//                                                if (count == saveAnswersDTOs.size()) {
//                                                    sta.open();
//                                                    sta.deleteAnswersDTO(sp.getInt("userID", 0));
//                                                    sta.close();
//                                                    fta.open();
//                                                    fta.deleteFormCount(sp.getInt("userID", 0));
//                                                    fta.close();
//                                                    countDTOs.clear();
//                                                    saveAnswersDTOs.clear();
//
//                                                    Toast.makeText(getApplicationContext(), "Surveys saved successfully", Toast.LENGTH_SHORT).show();
//                                                }
//                                            } catch (Exception e) {
//                                                e.printStackTrace();
//                                            }
//
//                                            //
//                                        }
//                                    }, new Response.ErrorListener() {
//
//                                @Override
//                                public void onErrorResponse(
//                                        VolleyError arg0) {
//                                    // TODO Auto-generated method stub
//                                    dialog.dismiss();
//                                    toast("Network Connection Error");
//                                    toast("Check your network connection");
//                                    // Log.i(LOG, arg0.toString());
//                                }
//                            });
//                            queue.add(request);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                } catch (Exception e) {
//                    dialog.dismiss();
//                }
//            } else {
//                toast("Network Connection Error");
//                toast("Check your network connection");
//            }
//        }
//        List<SurveyActionDTO> list = new ArrayList<SurveyActionDTO>();
//        SurveyActionTableAdapter sta = new SurveyActionTableAdapter(getApplicationContext());
//        sta.open();
//        list = sta.getListSurveyActionID(sp.getInt("userID", 0));
//        sta.close();
//
//        for (SurveyActionDTO s : list) {
//
//            String url = "http://10.0.2.2/insurvey/index.php?requestType=19&userID=2&start_date_time="+s.getStart_date_time().replace(" ","%")+"&end_date_time="+s.getEnd_date_time().replace(" ","%")+"&start_latitude="+s.getStart_latitude()+"&start_longitude="+s.getStart_longitude()+"&end_latitude="+s.getEnd_latitude()+"&end_longitude="+s.getEnd_longitude()+"&id_form_fk=64&start_city="+s.getStart_city()+"&end_city="+s.getEnd_city();
//            //String url="http://10.0.2.2/insurvey/index.php?requestType=19&userID=2&start_date_time="+s.getStart_date_time()+"&end_date_time="+s.getEnd_date_time()+"&place="+s.getPlace()+"&latitude="+s.getLatitude()+"&longitude="+s.getLongitude()+"&id_form_fk=64";
//            //String url = "http://www.insurvey.co.za/api?requestType=19&userID=" + sp.getInt("userID", 0) + "&start_date_time=" +s.getStart_date_time()
//            //  +"&end_date_time="+s.getEnd_date_time()+"&place="+s.getPlace()+"&latitude="+s.getLatitude()+"&longitude="+s.getLongitude()+"&id_form_fk="+s.getId_form_fk();
//            Log.i("URL ", url);
//            Toast.makeText(getApplicationContext(), "End " + s.getStart_date_time(), Toast.LENGTH_LONG).show();
//
//            StringRequest request = new StringRequest(url,
//                    new Response.Listener<String>() {
//
//                        @Override
//                        public void onResponse(String arg0) {
//                            myVolleySaveSurveyActions(arg0);
//                            stat.open();
//                            stat.deleteSurveyActions(sp.getInt("userID", 0));
//                            stat.close();
//                            dialog.dismiss();
//                        }
//                    }, new Response.ErrorListener() {
//
//                @Override
//                public void onErrorResponse(
//                        VolleyError arg0) {
//
//
//                    dialog.dismiss();// Se
//                    // TODO Auto-generated method stub
//                }
//            });
//            queue.add(request);
//            dialog.dismiss();
//        }
//    }
    private void myVolley(String arg0) {
        int x = 0;
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                JSONArray array = obj.getJSONArray("answers");
                JSONObject jso = new JSONObject();
                Log.i("TAG", obj.getString("message"));
            }

            else {
                // toast_error(obj.getString("message"));
                dialog.dismiss();
                //errorDialog();
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //Toast.makeText(getApplicationContext(),"count "+count,Toast.LENGTH_SHORT).show();
    }
    private void toast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,
                (ViewGroup) findViewById(R.id.toast_layout_root));
        TextView textView = (TextView) layout.findViewById(R.id.txtTOASTMessage);
        textView.setText(message);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
    private void toast_error(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_error,
                (ViewGroup) findViewById(R.id.toast_layout_root1));
        TextView textView = (TextView) layout.findViewById(R.id.txtTOASTMessage1);
        textView.setText(message);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.warning_blue)
                .setTitle("Exit Application")
                .setMessage("Are you sure you want to exit the Application?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      //System.exit(0);
                        Intent intent = new Intent(getApplicationContext(),LogIn.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    private List<Integer> list = new ArrayList<Integer>();
    private List<FormCountDTO> countDTOs;
    private SaveAnswerTableAdapter sta;
    private SurveyActionTableAdapter stat;
    private List<SaveAnswersDTO> saveAnswersDTOs;
    private FormCountTableAdapter fta;
    private boolean done = false;
    private int count = 0;
    int c = 0;
}
