package com.surveyor.insurvey.in_surveyapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import adapter.SurveyFormCountAdapter;
import dto.ActivityLogDTO;
import dto.ConnectionDetector;
import dto.ContactDTO;
import dto.DemorgraphicDTO;
import dto.FormCountDTO;
import dto.FormDTO;
import dto.PersonDTO;
import dto.SaveAnswersDTO;
import dto.SurveyActionDTO;
import sql.ActiveLogTableAdapter;
import sql.ContactTableAdapter;
import sql.DemorgraphicTableAdapter;
import sql.FormCountTableAdapter;
import sql.FormTableAdapter;
import sql.PersonTableAdapter;
import sql.SaveAnswerTableAdapter;
import sql.SurveyActionTableAdapter;

public class CompletedSurveyActivity extends ActionBarActivity {
    private ListView formCountlistView;
    private Context ctx;
    private SharedPreferences sp;
    private Bitmap myProfileImage;
    private SurveyFormCountAdapter sfca;
    private FormCountTableAdapter fta;
    private List<FormCountDTO> lst;
    private ProgressDialog dialog;
    private ActiveLogTableAdapter atlta;
    private FormTableAdapter ftac;
    private PersonTableAdapter pta;
    private ConnectionDetector cd;
    private  List<FormDTO> frmList;
    private RelativeLayout LayoutCompleted;
    RequestQueue queue;

    String TITLES[] = {"Main Menu","Download","Survey List","Surveys Actions","Completed","About","Logout"};
    int ICONS[] = {R.drawable.ic_home,R.drawable.ic_download,R.drawable.ic_list,R.drawable.ic_actions,R.drawable.ic_completed,R.drawable.ic_about,R.drawable.ic_logout};

    //And we also create a int resource for profile picture in the header view

    String NAME = "";
    String EMAIL = "";
    int PROFILE = R.drawable.pro;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_survey);
        setFields();
    }

    private void setFields(){
        ctx = getApplicationContext();
        sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        sata = new SaveAnswerTableAdapter(getApplicationContext());
        fcta = new FormCountTableAdapter(getApplicationContext());
        dta = new DemorgraphicTableAdapter(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        pta = new PersonTableAdapter(getApplicationContext());
        cta = new ContactTableAdapter(getApplicationContext());
        // RelLayout = (RelativeLayout)findViewById(R.id.RelLayout);
        LayoutCompleted = (RelativeLayout) findViewById(R.id.LayoutCompleted);
        Button btnClose = (Button) findViewById(R.id.btnCompletedClose);
        queue = Volley
                .newRequestQueue(getApplicationContext());

        NAME = sp.getString("name", "") + " " + sp.getString("lastname", "");
        EMAIL = sp.getString("email", "");
        surveyActionTableAdapter = new SurveyActionTableAdapter(getApplicationContext());
        surveyActionDTO = new SurveyActionDTO();

        formCountlistView = (ListView)findViewById(R.id.formCountlistView);
        NAME = sp.getString("name","") +" "+sp.getString("lastname","");
        EMAIL = sp.getString("email","");

        atlta = new ActiveLogTableAdapter(getApplicationContext());
        stat = new SurveyActionTableAdapter(getApplicationContext());
        ftac = new FormTableAdapter(getApplicationContext());
        sta = new SaveAnswerTableAdapter(getApplicationContext());
        stat = new SurveyActionTableAdapter(getApplicationContext());
        cta = new ContactTableAdapter(getApplicationContext());

        fta = new FormCountTableAdapter(ctx);
        lst = new ArrayList<FormCountDTO>();
        fta.open();
        lst = fta.getFormCountList(sp.getInt("userID",0));
        fta.close();

        ftac.open();
        frmList  = ftac.getFormList(sp.getInt("userID",0));
        ftac.close();
        fta.open();
        countDTOs = fta.getFormCountList(sp.getInt("userID",0));
        fta.close();

        atlta.open();
        activityLogDTOs = atlta.getList();
        atlta.close();

        if(lst.size() > 0)
        {
            LayoutCompleted.setVisibility(View.GONE);
            setFormListAdapter(lst);
        }else{
            LayoutCompleted.setVisibility(View.VISIBLE);
        }
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CompletedSurveyActivity.this,Main_OneActivity.class));
                finish();
            }
        });

        setNavigationDrawer();

    }
    private void setNavigationDrawer(){
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size
        FormTableAdapter fta = new FormTableAdapter(getApplicationContext());
        FormCountTableAdapter ftac = new FormCountTableAdapter(getApplicationContext());
        List<FormDTO> frmList = new ArrayList<FormDTO>();
        List<FormCountDTO> countDTOs = new ArrayList<FormCountDTO>();
        fta.open();
        frmList = fta.getFormList(sp.getInt("userID",0));
        fta.close();
        ftac.open();
        countDTOs = ftac.getFormCountList(sp.getInt("userID",0));
        ftac.close();
        String cnt[];
        if(sp.getString("fc","").equals("")){
            cnt = new String[]{"-1",String.valueOf(sp.getInt("formCount",0) - frmList.size()),String.valueOf(frmList.size()),"-1",String.valueOf(countDTOs.size()),"-1","-1"};
        }else{
            cnt = new String[] {"-1",sp.getString("fc",""),String.valueOf(frmList.size()),"-1",String.valueOf(countDTOs.size()),"-1","-1"};
        }
        myProfileImage = BitmapFactory.decodeResource(getResources(), R.drawable.pro);
        String encodedProfileImage = sp.getString("encodedProfileImage","");
        if( !encodedProfileImage.equalsIgnoreCase("") ){
            byte[] b = Base64.decode(encodedProfileImage, Base64.DEFAULT);
            myProfileImage = BitmapFactory.decodeByteArray(b, 0, b.length);
            // imageConvertResult.setImageBitmap(bitmap);
        }
        mAdapter = new MyAdapter(TITLES,ICONS,NAME,EMAIL,PROFILE,getApplicationContext(),cnt,myProfileImage);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        final GestureDetector mGestureDetector = new GestureDetector(ctx, new GestureDetector.SimpleOnGestureListener() {

            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });


        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(),motionEvent.getY());

                if(child!=null && mGestureDetector.onTouchEvent(motionEvent)){
                    // Drawer.closeDrawers();
                    //Toast.makeText(ctx, "The Item Clicked is: " + recyclerView.getChildPosition(child), Toast.LENGTH_SHORT).show();
                    navigate(recyclerView.getChildPosition(child));
                    return true;

                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });


        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.openDrawer,R.string.closeDrawer){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }



        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

    }
    private void navigate(int number){

        Intent intent;
        switch (number) {
            case 0:
                intent = new Intent(ctx, ProfileActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 1:
                intent = new Intent(getApplicationContext(),Main_OneActivity.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 2:
                if(sp.getString("fc","").equals("")) {
                    intent = new Intent(getApplicationContext(), MySurvey_List.class);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
                break;
            case 3:
                    intent = new Intent(getApplicationContext(), List_Downloaded_Surveys.class);
                    // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Drawer.closeDrawers();
                    startActivity(intent);
                    finish();

                break;
            case 4:
                intent = new Intent(getApplicationContext(),SurveyActionActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
            case 5:
                Drawer.closeDrawers();
                break;
            case 6:
                Drawer.closeDrawers();
                break;
            case 7:
                intent = new Intent(getApplicationContext(),LogIn.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Drawer.closeDrawers();
                startActivity(intent);
                finish();
                break;
        }

    }
    private void setFormListAdapter(List<FormCountDTO> fl) {
        sfca = new SurveyFormCountAdapter(getApplicationContext(), R.layout.form_count, fl);
        formCountlistView.setAdapter(sfca);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_completed_survey, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        if (id == R.id.action_save) {
//            try {
//              //  saveSurvey();
//             //   addPerson();
//             //   fta.open();
//             //   fta.deleteFormCount(sp.getInt("userID", 0));
//              //  fta.close();
//              //  startActivity(new Intent(CompletedSurveyActivity.this, Main_OneActivity.class));
//               // finish();
//
//            } catch (Exception e) {
//                Toast.makeText(getApplicationContext(), "Unable to save survey", Toast.LENGTH_SHORT).show();
//            }
//
//            return true;
//        }
//
       return super.onOptionsItemSelected(item);
    }

    private void dialog() {
        dialog = new ProgressDialog(CompletedSurveyActivity.this,
                AlertDialog.THEME_HOLO_LIGHT);
        dialog.setMessage("Please wait");
        dialog.setTitle("loading ...");
        dialog.setIcon(R.drawable.form_small);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }


    private void toast_error(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_error,
                (ViewGroup) findViewById(R.id.toast_layout_root1));
        TextView textView = (TextView) layout.findViewById(R.id.txtTOASTMessage1);
        textView.setText(message);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(),Main_OneActivity.class);
        startActivity(intent);
        finish();

    }
    private void saveAnwers(String personID) {
//        sata.open();
//        saveAnswersDTOs = sata.getSavedAnswers(sp.getInt("userID", 0));
//        sata.close();
        if(cd.isConnectingToInternet()) {
            if (saveAnswersDTOs.size() > 0) {
                dialog();
                int firstpersonID = saveAnswersDTOs.get(0).getPersonID();
                for (int i = 0; i < saveAnswersDTOs.size(); ++i) {
                    Log.i("firstpersonID ", "" + firstpersonID);
                    Log.i("saveAnswersDTOs.size() ", "" + saveAnswersDTOs.size());

                    Log.i("firstpersonIDs ", "" + saveAnswersDTOs.get(i).getPersonID());
                    SaveAnswersDTO al = saveAnswersDTOs.get(i);
                    int person = saveAnswersDTOs.get(i).getPersonID();
                    try {
                        if (person == firstpersonID) {

                            String url = "http://www.insurvey.co.za/api?requestType=17&id_user_fk="
                                    + Integer.toString(al.getUserID())
                                    + "&id_form_fk=" + Integer.toString(al.getFormID())
                                    + "&id_activity_fk=" + al.getActivityID()
                                    + "&answer=" + al.getAnswer()
                                    + "&id_question_fk=" + Integer.toString(al.getQuestionID())
                                    + "&id_activity_log_fk=" + Integer.toString(al.getActivityID())
                                    + "&id_person_fk=" + personID;
                            Log.i("URLS ", url);
                            JSONObject js = new JSONObject();

                            try {

                                StringRequest request = new StringRequest(url,
                                        new Response.Listener<String>() {

                                            @Override
                                            public void onResponse(String arg0) {
                                                //  Log.i(LOG, arg0.toString());

                                                count++;
                                                myVolley1(arg0);


                                                Log.i("Count ", "" + count++);

                                                destroy = true;

                                            }
                                        }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(
                                            VolleyError arg0) {
                                        // TODO Auto-generated method stub
                                        // dialog.dismiss();
                                        // toast("Network Connection Error");
                                        //  toast("Check your network connection");
                                        // Log.i(LOG, arg0.toString());
                                    }
                                });
                                request.setTag("saveAnswer");
                                queue.add(request);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            List<SaveAnswersDTO> lst = new ArrayList<SaveAnswersDTO>();
                            sata.open();
                            sata.deleteAnswerByPersonID(firstpersonID);
                            sata.close();
                            pta.open();
                            pta.deleteByPersonID(firstpersonID);
                            pta.close();
                            Log.i("breaking ", "ya");
                            Log.i("breaking ", "" + firstpersonID);
                            break;
                        }
                    } catch (Exception e) {

                    }

                }

            }
            p = new ArrayList<>();
            pta.open();
            p = pta.getPersonsList(sp.getInt("userID",0));
            pta.close();
            if(p.size() == 1){
                Toast.makeText(getApplicationContext(), "Surveys Submitted", Toast.LENGTH_SHORT).show();
                sendSMS();

            }
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(destroy) {
            sata.open();
            sata.deleteAnswersDTO(sp.getInt("userID", 0));
            sata.close();
            pta.open();
            pta.deleteByPersonUserID(sp.getInt("userID", 0));
            pta.close();

            Log.i("Des ", "Destroyes");

        }

    }
    private void saveSurvey(String personID) {
//        sata.open();
//        saveAnswersDTOs = sata.getSavedAnswers(sp.getInt("userID", 0));
//        sata.close();

        if (saveAnswersDTOs.size() > 0) {
            if (CheckInternet(getApplicationContext())) {
                try {
                    dialog();
                    boolean saved = false;

                    for (final SaveAnswersDTO al : saveAnswersDTOs) {
                        count++;
                        Log.i("Count  ",""+ count);
                        Log.i("saveAnswersDTOs.size()  ",""+ saveAnswersDTOs.size());
                        if(count == saveAnswersDTOs.size()/2){
                            Log.i("Breaked ","Breaked");
                            break;
                        }
                        // String url = "http://10.0.2.2/insurvey/index.php?requestType=17&id_user_fk=" + Integer.toString(al.getUserID()) + "&id_form_fk=" + Integer.toString(al.getFormID()) + "&id_activity_fk=" + al.getActivityID() + "&answer=" + al.getAnswer() + "&id_question_fk=" + Integer.toString(al.getQuestionID()) + "&id_activity_log_fk=1&id_person_fk=" + Integer.toString(al.getPersonID());
                        String url = "http://www.insurvey.co.za/api?requestType=17&id_user_fk="
                                + Integer.toString(al.getUserID()) + "&id_form_fk="
                                + Integer.toString(al.getFormID()) + "&id_activity_fk="
                                + al.getActivityID() + "&answer=" + al.getAnswer()
                                + "&id_question_fk=" + Integer.toString(al.getQuestionID())
                                + "&id_activity_log_fk=" + Integer.toString(al.getActivityID())
                                + "&id_person_fk=" +personID;
                        Log.i("URLS ", url);
                        JSONObject js = new JSONObject();
                        try {
                            StringRequest request = new StringRequest(url,
                                    new Response.Listener<String>() {

                                        @Override
                                        public void onResponse(String arg0) {
                                            //  Log.i(LOG, arg0.toString());


                                            myVolley1(arg0);
                                            dialog.dismiss();
                                            try {
                                                if (count == saveAnswersDTOs.size()) {
                                                    sata.open();
                                                    sata.deleteAnswersDTO(sp.getInt("userID", 0));
                                                    sata.close();
                                                    fcta.open();
                                                    fcta.deleteFormCount(sp.getInt("userID", 0));
                                                    fcta.close();
                                                    //countDTOs.clear();
                                                    SharedPreferences.Editor editor = sp.edit();
                                                    // editor.putInt("SQLFCount",countDTOs.size());
                                                    editor.commit();
                                                    saveAnswersDTOs.clear();
                                                    Toast.makeText(getApplicationContext(), "Surveys saved successfully", Toast.LENGTH_SHORT).show();
                                                    dialog.dismiss();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                dialog.dismiss();
                                            }

                                            //
                                        }
                                    }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(
                                        VolleyError arg0) {
                                    // TODO Auto-generated method stub
                                    dialog.dismiss();

                                }
                            });
                            request.setTag("saveAnswer");
                            queue.add(request);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                } catch (Exception e) {
                    dialog.dismiss();
                }
            } else {

            }
        }
    }
    private void saveSurvey() {
        List<SurveyActionDTO> list = new ArrayList<SurveyActionDTO>();
        SurveyActionTableAdapter sta = new SurveyActionTableAdapter(getApplicationContext());
        sta.open();
        list = sta.getListSurveyActionID(sp.getInt("userID", 0));
        sta.close();
        if(list.size() > 0){
            dialog();
            for (SurveyActionDTO s : list) {
                //String url = "http://10.0.2.2/insurvey/index.php?requestType=19&userID="+s.getId_user_fk()+"&start_date_time=" + s.getStart_date_time().replace(" ", "%") + "&end_date_time=" + s.getEnd_date_time().replace(" ", "%") + "&start_latitude=" + s.getStart_latitude() + "&start_longitude=" + s.getStart_longitude() + "&end_latitude=" + s.getEnd_latitude() + "&end_longitude=" + s.getEnd_longitude() + "&id_form_fk="+s.getId_form_fk()+"&start_city=" + s.getStart_city() + "&end_city=" + s.getEnd_city();
                String url = "http://www.insurvey.co.za/api?requestType=19&userID="+ s.getId_user_fk()
                        +"&start_date_time=" + s.getStart_date_time().replace(" ", "%")
                        + "&end_date_time=" + s.getEnd_date_time().replace(" ", "%")
                        + "&start_latitude=" + s.getStart_latitude()
                        + "&start_longitude=" + s.getStart_longitude()
                        + "&end_latitude=" + s.getEnd_latitude()
                        + "&end_longitude=" + s.getEnd_longitude()
                        + "&id_form_fk="+s.getId_form_fk()
                        + "&start_city=" + s.getStart_city()
                        + "&end_city=" + s.getEnd_city();
                Log.i("BATHONG", url);

                StringRequest request2 = new StringRequest(url,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String arg0) {
                                myVolleySaveSurveyActions(arg0);
                                surveyActionTableAdapter.open();
                                surveyActionTableAdapter.deleteSurveyActions(sp.getInt("userID", 0));
                                surveyActionTableAdapter.close();
                                dialog.dismiss();
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(
                            VolleyError arg0) {


                        dialog.dismiss();// Se
                        // TODO Auto-generated method stub
                    }
                });
                request2.setTag("saveAnswer");
                queue.add(request2);
                dialog.dismiss();
            }

            List<ActivityLogDTO> alist = new ArrayList<ActivityLogDTO>();
            final ActiveLogTableAdapter alt = new ActiveLogTableAdapter(getApplicationContext());
            alt.open();
            alist = alt.getActivityLogList(sp.getInt("userID", 0));
            sta.close();

            for (ActivityLogDTO a : alist) {
                Log.i("URL ",String.valueOf(a.getActivityID()));
                // String urlActivityLog = "http://10.0.2.2/insurvey/index.php?requestType=13&id_user_fk="+a.getUserID()+"&id_form_fk="+a.getFormID()+"&id_activity_fk=" + a.getActivityID();
                String urlActivityLog = "http://www.insurvey.co.za/api?requestType=13&id_user_fk="+a.getUserID()+"&id_form_fk="+a.getFormID()+"&id_activity_fk=" + a.getActivityID();
                //String url="http://10.0.2.2/insurvey/index.php?requestType=19&userID=2&start_date_time="+s.getStart_date_time()+"&end_date_time="+s.getEnd_date_time()+"&place="+s.getPlace()+"&latitude="+s.getLatitude()+"&longitude="+s.getLongitude()+"&id_form_fk=64";
                //String url = "http://www.insurvey.co.za/api?requestType=19&userID=" + sp.getInt("userID", 0) + "&start_date_time=" +s.getStart_date_time()
                //  +"&end_date_time="+s.getEnd_date_time()+"&place="+s.getPlace()+"&latitude="+s.getLatitude()+"&longitude="+s.getLongitude()+"&id_form_fk="+s.getId_form_fk();
                Log.i("URL ", urlActivityLog);
                // Toast.makeText(getApplicationContext(), "End " + s.getStart_date_time(), Toast.LENGTH_LONG).show();

                StringRequest requestActivityLog = new StringRequest(urlActivityLog,
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String arg0) {
                                Log.i("Responce :",arg0);
                                myVolleyActivityLog(arg0);
                                alt.open();
                                alt.deleteActivityLogs(sp.getInt("userID", 0));
                                alt.close();
                                dialog.dismiss();
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(
                            VolleyError arg0) {
                        dialog.dismiss();// Se
                        // TODO Auto-generated method stub
                    }
                });
                requestActivityLog.setTag("saveAnswer");
                queue.add(requestActivityLog);
                dialog.dismiss();
            }

        }

    }

    private void myVolley1(String arg0) {
        int x = 0;
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                Log.i("TAG", obj.getString("message"));
                JSONArray array = obj.getJSONArray("answers");
                JSONObject jso = new JSONObject();
            }

            else {
                // toast_error(obj.getString("message"));
                dialog.dismiss();
                //errorDialog();
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //Toast.makeText(getApplicationContext(),"count "+count,Toast.LENGTH_SHORT).show();
    }


    private void myVolleySaveSurveyActions(String arg0) {

        try {
            Log.i("Res ", arg0);
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                JSONArray array = obj.getJSONArray("survey_actions");
                Log.i("TAG", obj.getString("message"));

            } else {
                Log.i("TAG", obj.getString("message"));
                dialog.dismiss();

                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {

            // TODO Auto-generated catch block
            dialog.dismiss();
            e.printStackTrace();
        }
    }
    private void myVolleyActivityLog(String arg0) {

        try {
            Log.i("Res ", arg0);
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("1")) {
                // toast_error(obj.getString("message"));
                Log.i("TAG", obj.getString("message"));

            } else {

                dialog.dismiss();

                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {

            // TODO Auto-generated catch block
            dialog.dismiss();
            e.printStackTrace();
        }
    }

    public static boolean CheckInternet(Context context)
    {
        ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo mobile = connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        return wifi.isConnected() || mobile.isConnected();
    }
    private void addPerson(){
        pList  = new ArrayList<PersonDTO>();
        pta.open();
        pList = pta.getPersonsList(sp.getInt("userID",0));
        pta.close();
        //String urlDemo = "http://10.0.2.2/insurvey/index.php?requestType=12&name="+""+"&lastname="+""+"&phone="+edPhone.getText().toString()+"&age="+personDTO.getAge().replace(" ","_")+"&race="+personDTO.getRace()+"&gender="+personDTO.getGender().replace(" ","_")+"&marital="+personDTO.getMarital().replace(" ","_")+"&employment="+personDTO.getEmployment().replace(" ","_")+"&email="+edEmail.getText().toString()+"&id_service_provider_fk="+networkProvider;
        Log.i("Person size", " " + pList.size());
        for(PersonDTO p:pList){
            String urlDemo =  "http://www.insurvey.co.za/api?requestType=12&name="+""+"&lastname="+""
                    +"&phone="+p.getPhone()+"&age="+p.getAge().replace(" ","_")
                    +"&race="+p.getRace()+"&gender="+p.getGender().replace(" ","_")
                    +"&marital="+p.getMarital().replace(" ","_")+"&employment="
                    +p.getEmployment().replace(" ","_")+"&email="+p.getEmail()
                    +"&id_service_provider_fk="+p.getServiceProvideID();
            Log.i("URL person addinf bra ",urlDemo);
            dialog();
            StringRequest requestAddDenographic = new StringRequest(urlDemo,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String arg0) {
                            Log.i("Responce :",arg0);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(arg0);

                                if (obj.getString("success").equals("1")) {
                                    obj = obj.getJSONObject("persons");
                                    personID = obj.getString("id_person");

                                    // saveSurvey(personID);
                                    saveAnwers(personID);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            dialog.dismiss();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(
                        VolleyError arg0) {
                    dialog.dismiss();// Se
                    // TODO Auto-generated method stub
                }

            });
            queue.add(requestAddDenographic);
            dialog.dismiss();
        }

    }
    private void sendSMS(){
        cList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        char amp = ';';
        cta.open();
        cList = cta.getContactList(sp.getInt("userID",0));
        cta.close();
        String numbers = "";

        for(ContactDTO c : cList){
            if(cList.size() > 1){
                totalContacts++;
                numbers += sb.append(c.getPhone()).append(amp);
            }else{
                totalContacts = 1;
                numbers = c.getPhone();
            }

        }
        if(cList.size() > 1){
            numbers = numbers.substring(0, numbers.length()-1);
        }
        final int cnt = numbers.length() - numbers.replace(";", "").length() + 1;
        //  String sendURL = "http://www.insurvey.co.za/api?requestType=24&nums=";//http://www.insurvey.co.za/api?requestType
        String sendURL = "http://10.0.2.2/insurvey/index.php?requestType=24&msg=Yman&nums=";
        String url = sendURL+numbers;
        Log.i("url", url);
        StringRequest requestAddDenographic = new StringRequest(url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String arg0) {
                        int badPhone = 0;
                        String Credit = "0";
                        int NoCredit = 0;
                        Log.i("Responce :",arg0);
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(arg0);
                            Credit = obj.getString("credit");
                            if(obj.has("results")){
                                JSONObject objResults = obj.getJSONObject("results");
                                if(objResults.has("BADDEST")){
                                    badPhone =  Integer.parseInt(objResults.getString("BADDEST"));
                                    Log.i("badPhone :",""+badPhone);
                                }
                                if(objResults.has("NOCREDITS")){
                                    NoCredit = Integer.parseInt(objResults.getString("NOCREDITS"));
                                    Log.i("NoCredit :",""+NoCredit);
                                }
                            }else{
                                Toast.makeText(CompletedSurveyActivity.this,"Your Messages are send.",Toast.LENGTH_SHORT).show();
                            }
                            int Delivered  = cnt - (badPhone +  NoCredit);
                            Log.i("Delivered :",""+Delivered);
                            setDeliveryReport(String.valueOf(cnt), String.valueOf(badPhone), String.valueOf(Delivered), String.valueOf(NoCredit), Credit);

                            cta.open();
                            cta.deleteContact(sp.getInt("userID", 0),sp.getInt("formID1",0));
                            cta.close();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        dialog.dismiss();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(
                    VolleyError arg0) {
                arg0.getMessage();
                dialog.dismiss();// Se
                // TODO Auto-generated method stub
            }

        });
        queue.add(requestAddDenographic);
        dialog.dismiss();
    }

    private void setDeliveryReport(String tot,String badp,String del,String cre,String availableCredit){

        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(
                R.layout.person, null);
        deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);
        // deleteDialog.setCancelable(false);
        deleteDialog.setCanceledOnTouchOutside(true);
        //setFinishOnTouchOutside(false);
        TextView total = (TextView) deleteDialogView.findViewById(R.id.tv_total);
        TextView badphone = (TextView) deleteDialogView.findViewById(R.id.tv_badphone);
        TextView delivered = (TextView) deleteDialogView.findViewById(R.id.tv_delivered);
        TextView credit = (TextView) deleteDialogView.findViewById(R.id.tv_credit);
        TextView tv_creditavailable = (TextView) deleteDialogView.findViewById(R.id.tv_creditavailable);

        total.setText(tot);
        badphone.setText(badp);
        delivered.setText(del);
        credit.setText(cre);
        tv_creditavailable.setText(availableCredit);

        Button dialogButton = (Button) deleteDialogView.findViewById(R.id.btnCloseSMSDialog);// btnCancelForgetPassword = (Button) deleteDialogView.findViewById(R.id.btnCancelForgetPassword);

        dialogButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                deleteDialog.dismiss();
                startActivity(new Intent(CompletedSurveyActivity.this,List_Downloaded_Surveys.class));
                finish();

            }
        });

        deleteDialog.show();

    }
    private PersonDTO personDTO;
    private DemorgraphicDTO demographicDTO;
    private List<DemorgraphicDTO> list = new ArrayList<DemorgraphicDTO>();
    List<DemorgraphicDTO> lstDemo;
    private DemorgraphicTableAdapter dta;

    private SurveyActionTableAdapter surveyActionTableAdapter;
    private SurveyActionDTO surveyActionDTO;

    private ActiveLogTableAdapter activeLogTableAdapter;
    private FormCountTableAdapter fcta;

    //
    private List<ActivityLogDTO> activityLogDTOs = new ArrayList<ActivityLogDTO>();
    private SaveAnswerTableAdapter sata;
    private ContactTableAdapter cta;
    private String personID;
    private List<PersonDTO> pList;
    private boolean  destroy = false;
    private boolean  saved = false;
    List<PersonDTO> p;
    List<ContactDTO> cList;
    private  AlertDialog deleteDialog;
    private int totalContacts;

    private List<FormCountDTO> countDTOs;
    private SaveAnswerTableAdapter sta;
    private SurveyActionTableAdapter stat;
    private List<SaveAnswersDTO> saveAnswersDTOs;
    private boolean done = false;
    private int count = 0;
    int c = 0;
}
