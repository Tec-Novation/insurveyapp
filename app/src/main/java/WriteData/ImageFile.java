package WriteData;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;


/**
 * Created by iFluid-Dev on 2016-04-15.
 */
public class ImageFile {

    public static void saveImage(Context context, Bitmap b,String name){
        FileOutputStream out;
        try {
            out = context.openFileOutput(name, Context.MODE_PRIVATE);
            b.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.close();
            Log.i("Saving Image ","Image Saved");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap getImageBitmap(Context context,String name){

        try{
            FileInputStream fis = context.openFileInput(name);
            Bitmap b = BitmapFactory.decodeStream(fis);
            fis.close();
            return b;
        }
        catch(Exception e){
        }
        return null;
    }
}
