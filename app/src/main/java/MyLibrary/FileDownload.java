package MyLibrary;

/**
 * Created by iFluid-Dev on 2016-04-15.
 */
import android.content.Context;
import android.content.ContextWrapper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by iFluid-Dev on 2015-06-11.
 *
 * Class that creates the storage directory for a files to download
 *
 * Method that saves files to the created directory
 */
public class FileDownload {

    //region Variables

    private Context context;

    private String directoryName;

    private String fullDirectoryName;

    private String hostUrl;
    //endregion

    /**
     *
     * Constructor to create a instance to download files
     *
     * @param context the context of the application
     * @param directoryName the directory to create for the file to be saved in
     * @param hostUrl the URL to download the files from
     */
    public FileDownload(Context context,
                        String directoryName,
                        String hostUrl){

        this.context = context;

        this.directoryName = directoryName;

        this.hostUrl = hostUrl;

        createStorageDirectory(directoryName);


    }

    /**
     * Method that creates the directory to save the files in if it has not yet been created
     * @param directoryName the name of the directory
     */
    private void createStorageDirectory(String directoryName){

        fullDirectoryName = directoryName;

        File fileStorageDirectory = new File(directoryName);
        Boolean directoryExists = fileStorageDirectory.exists();
        if (!directoryExists){
            fileStorageDirectory.mkdir();
            fileStorageDirectory.setWritable(true);
        }


    }

    /**
     * Method that downloads the file and saves it to the directory passed in the constructor
     * this method also return an object that has the filename stored in it and the status
     * of the download
     * @param filename
     * @return the DownloadResult object with the filename and status of download
     */
    public DownloadResult downloadFile(String filename) {

        DownloadResult downloadResult = new DownloadResult(filename,false);


        try {

            String filenameOnly = filename;
            String search = "images_campany";

            if(filename.toLowerCase().indexOf(search.toLowerCase()) != -1) {

                filenameOnly = filenameOnly.replace("includes/uploads/images_campany/","");

            }else{

                filenameOnly = filenameOnly.replace("includes/uploads/images/","");

            }


            String filePath = fullDirectoryName
                    + "/"
                    + filenameOnly;

            File file = new File(filePath);
            file.setWritable(true);
            Boolean exists = file.exists();
            //String urlToConnect = hostUrl + file.getName();
            String urlToConnect = hostUrl + filename;
            URL server = new URL(urlToConnect);


            HttpURLConnection connection = (HttpURLConnection) server.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.addRequestProperty("Accept",
                    "audio/wav, " +
                            "audio/x-wav, " +
                            "image/gif, " +
                            "image/x-xbitmap, " +
                            "image/jpeg, " +
                            "image/pjpeg, " +
                            "image/png, " +
                            "application/msword, " +
                            "application/vnd.ms-excel, " +
                            "application/vnd.ms-powerpoint, " +
                            "application/x-shockwave-flash, */*");

            connection.addRequestProperty("Accept-Language", "en-us,zh-cn;q=0.5");

            connection.addRequestProperty("Accept-Encoding", "gzip, deflate");

            connection.connect();

            InputStream is = connection.getInputStream();

            ContextWrapper cw = new ContextWrapper(context);

            FileOutputStream os = new FileOutputStream(file);

            byte[] buffer = new byte[1024];
            int byteReaded = is.read(buffer);
            while (byteReaded != -1) {
                os.write(buffer, 0, byteReaded);
                byteReaded = is.read(buffer);
            }

            os.close();
            downloadResult.setSuccessfullDownload(true);
            downloadResult.setFileName(filenameOnly);

        } catch (IOException e) {
            e.printStackTrace();
            downloadResult.setSuccessfullDownload(false);
        }

        return downloadResult;

    }






}

