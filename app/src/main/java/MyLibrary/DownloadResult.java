package MyLibrary;

/**
 * Created by iFluid-Dev on 2016-04-15.
 */
public class DownloadResult {



    private String fileName;

    private Boolean successfullDownload;


    /**
     * The constructor for the file to be downloaded
     * @param fileName the name of the file
     * @param successfullDownload status of the download
     */
    public DownloadResult(String fileName,
                          Boolean successfullDownload){

        this.fileName = fileName;

        this.successfullDownload = successfullDownload;




    }

    //region Getters
    public Boolean getSuccessfullDownload() {
        return successfullDownload;
    }

    public String getFileName() {
        return fileName;
    }
    //endregion

    //region Setters
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setSuccessfullDownload(Boolean successfullDownload) {
        this.successfullDownload = successfullDownload;
    }
    //endregion




}
