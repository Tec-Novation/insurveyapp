package dto;

/**
 * Created by Vic on 1/26/2015.
 */
public class FormDTO {

    //region Variables

    //IDs
    private int formID;
    private int userID;
    private int id;

    //Counters
    private int survey_limit;
    private int survey_total;
    private int formStatusID;


    private String name;
    private String createDate;
    private String lastUpdated;
    private String description;
    private String smsDescription;

    //endregion

    public FormDTO(){

    }
    public FormDTO(int formID,
                   int userID,String name,
                   String createDate,
                   String lastUpdated,
                   int formStatusID,
                   String description,
                   String smsDescription,
                   int survey_limit,
                   int survey_total){

        this.userID = userID;
        this.formID = formID;
        this.name = name;
        this.createDate = createDate;
        this.lastUpdated = lastUpdated;
        this.formStatusID = formStatusID;
        this.description = description;
        this.smsDescription = smsDescription;
        this.survey_limit = survey_limit;
        this.survey_total = survey_total;
    }

    public int getSurvey_total() {
        return survey_total;
    }

    public void setSurvey_total(int survey_total) {
        this.survey_total = survey_total;
    }

    public int getSurvey_limit() {
        return survey_limit;
    }

    public void setSurvey_limit(int survey_limit) {
        this.survey_limit = survey_limit;
    }

    public String getSmsDescription() {
        return smsDescription;
    }

    public void setSmsDescription(String smsDescription) {
        this.smsDescription = smsDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFormID() {
        return formID;
    }

    public void setFormID(int formID) {
        this.formID = formID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public int getFormStatusID() {
        return formStatusID;
    }

    public void setFormStatusID(int formStatusID) {
        this.formStatusID = formStatusID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @Override
    public String toString() {
        return "Id : " + this.id;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof FormDTO) && this.id == ((FormDTO)obj).getId();
    }
}
