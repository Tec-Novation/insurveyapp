package dto;

/**
 * Created by Vic on 1/29/2015.
 */
public class SectionDTO {
    private int sectionID;
    private int formID;
    private String name;
    private String description;

    public SectionDTO(){

    }
    public SectionDTO(int sectionID,int formID,String name,String description){
        this.sectionID = sectionID;
        this.formID = formID;
        this.name = name;
        this.description = description;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public int getFormID() {
        return formID;
    }

    public void setFormID(int formID) {
        this.formID = formID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJsonCode() {
        return this.description;
    }

    public void setJsonCode(String description) {
        this.description = description;
    }
}
