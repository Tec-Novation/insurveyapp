package dto;

/**
 * Created by iFluid-Dev on 2015-03-11.
 */
public class FormCountDTO {

    private int frmCountID;
    private String description;
    private int userID;
    private String date;

    public FormCountDTO() {
    }

    public FormCountDTO(int frmCountID, String description, int userID, String date) {
        this.frmCountID = frmCountID;
        this.description = description;
        this.userID = userID;
        this.date = date;
    }
    public FormCountDTO(String description, int userID, String date) {
        this.frmCountID = frmCountID;
        this.description = description;
        this.userID = userID;
        this.date = date;
    }
    public FormCountDTO(String description, int userID) {
        this.description = description;
        this.userID = userID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getFrmCountID() {
        return frmCountID;
    }

    public void setFrmCountID(int frmCountID) {
        this.frmCountID = frmCountID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
