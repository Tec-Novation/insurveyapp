package dto;

/**
 * Created by Charmaine on 2015/04/20.
 */
public class SurveyActionDTO {

    private int id_survey_action;
    private String start_date_time;
    private String end_date_time;
    private String start_city;
    private String end_city;
    private String survey_name;
    private String start_latitude;
    private String start_longitude;
    private String end_latitude;
    private String end_longitude;
    private int id_user_fk;
    private int id_form_fk;


    public SurveyActionDTO() {
    }

    public SurveyActionDTO(String start_date_time, String end_date_time, String start_city, String end_city, String survey_name) {
        this.start_date_time = start_date_time;
        this.end_date_time = end_date_time;
        this.start_city = start_city;
        this.end_city = end_city;
        this.survey_name = survey_name;
    }

    public int getId_survey_action() {
        return id_survey_action;
    }

    public void setId_survey_action(int id_survey_action) {
        this.id_survey_action = id_survey_action;
    }

    public String getStart_date_time() {
        return start_date_time;
    }

    public void setStart_date_time(String start_date_time) {
        this.start_date_time = start_date_time;
    }

    public String getEnd_date_time() {
        return end_date_time;
    }

    public void setEnd_date_time(String end_date_time) {
        this.end_date_time = end_date_time;
    }

    public String getStart_city() {
        return start_city;
    }

    public void setStart_city(String start_city) {
        this.start_city = start_city;
    }

    public String getEnd_city() {
        return end_city;
    }

    public void setEnd_city(String end_city) {
        this.end_city = end_city;
    }

    public String getSurvey_name() {
        return survey_name;
    }

    public void setSurvey_name(String survey_name) {
        this.survey_name = survey_name;
    }

    public String getStart_latitude() {
        return start_latitude;
    }

    public void setStart_latitude(String start_latitude) {
        this.start_latitude = start_latitude;
    }

    public String getStart_longitude() {
        return start_longitude;
    }

    public void setStart_longitude(String start_longitude) {
        this.start_longitude = start_longitude;
    }

    public String getEnd_latitude() {
        return end_latitude;
    }

    public void setEnd_latitude(String end_latitude) {
        this.end_latitude = end_latitude;
    }

    public String getEnd_longitude() {
        return end_longitude;
    }

    public void setEnd_longitude(String end_longitude) {
        this.end_longitude = end_longitude;
    }

    public int getId_user_fk() {
        return id_user_fk;
    }

    public void setId_user_fk(int id_user_fk) {
        this.id_user_fk = id_user_fk;
    }

    public int getId_form_fk() {
        return id_form_fk;
    }

    public void setId_form_fk(int id_form_fk) {
        this.id_form_fk = id_form_fk;
    }
}
