package dto;

import java.util.ArrayList;

/**
 * Created by Vic on 1/31/2015.
 */
public class AnswersDTO {
    private int answerID;
    private String answer;
    private int questionID;
    private int activityLogID;
    private int personID;
    private ArrayList<AnswersDTO> answersDTOs;

    public AnswersDTO() {
    }

    public AnswersDTO(int answerID, String answer, int questionID, int activityLogID, int personID) {
        this.answerID = answerID;
        this.answer = answer;
        this.questionID = questionID;
        this.activityLogID = activityLogID;
        this.personID = personID;
    }

    public ArrayList<AnswersDTO> getAnswersDTOs() {
        return answersDTOs;
    }

    public void setAnswersDTOs(ArrayList<AnswersDTO> answersDTOs) {
        this.answersDTOs = answersDTOs;
    }

    public int getAnswerID() {
        return answerID;
    }

    public void setAnswerID(int answerID) {
        this.answerID = answerID;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public int getActivityLogID() {
        return activityLogID;
    }

    public void setActivityLogID(int activityLogID) {
        this.activityLogID = activityLogID;
    }

    public int getPersonID() {
        return personID;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }
}
