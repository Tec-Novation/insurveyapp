package dto;

/**
 * Created by iFluid-Dev on 2015-02-19.
 */
public class PersonDTO {
    private int userID;
    private int formID;
    private int personID;
    private int serviceProvideID;
    private int sms_recieve;

    private String name;
    private String lastname;
    private String phone;
    private String email;
    private String age;
    private String race;
    private String gender;
    private String marital;
    private String employment;
    private String uid;
    private String dateOfbirth;
    private String income;
    private String noOfOccupants;
    private String companyPhone;
    private String address;
    private String inhousehold;
    private String municipality;

    public PersonDTO() {
    }

    public PersonDTO(int userID,
                     int serviceProvideID,
                     String phone,
                     String email,
                     String age,
                     String race,
                     String gender,
                     String marital,
                     String employment,
                     int formID,
                     int sms_recieve,
                     String uid,
                     String dateOfbirth,
                     String income,
                     String noOfOccupants,
                     String companyPhone,
                     String name,
                     String surname,
                     String address,
                     String inhousehold,
                     String municipality
                     ) {
        this.userID = userID;
        this.serviceProvideID = serviceProvideID;
        this.phone = phone;
        this.email = email;
        this.age = age;
        this.race = race;
        this.gender = gender;
        this.marital = marital;
        this.employment = employment;
        this.formID = formID;
        this.sms_recieve = sms_recieve;
        this.uid  = uid;
        this.dateOfbirth = dateOfbirth;
        this.income = income;
        this.noOfOccupants = noOfOccupants;
        this.companyPhone = companyPhone;
        this.name = name;
        this.lastname = surname;
        this.address = address;
        this.inhousehold = inhousehold;
        this.municipality = municipality;

    }


    public int getSms_recieve() {
        return sms_recieve;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setSms_recieve(int sms_recieve) {
        this.sms_recieve = sms_recieve;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getFormID() {
        return formID;
    }

    public void setFormID(int formID) {
        this.formID = formID;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getPersonID() {
        return personID;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }

    public int getServiceProvideID() {
        return serviceProvideID;
    }

    public void setServiceProvideID(int serviceProvideID) {
        this.serviceProvideID = serviceProvideID;
    }

    public String getMarital() {
        return marital;
    }

    public void setMarital(String marital) {
        this.marital = marital;
    }

    public String getEmployment() {
        return employment;
    }

    public void setEmployment(String employment) {
        this.employment = employment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateOfbirth() {
        return dateOfbirth;
    }

    public void setDateOfbirth(String dateOfbirth) {
        this.dateOfbirth = dateOfbirth;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getNoOfOccupants() {
        return noOfOccupants;
    }

    public void setNoOfOccupants(String noOfOccupants) {
        this.noOfOccupants = noOfOccupants;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInhousehold() {
        return inhousehold;
    }

    public void setInhousehold(String inhousehold) {
        this.inhousehold = inhousehold;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }
}
