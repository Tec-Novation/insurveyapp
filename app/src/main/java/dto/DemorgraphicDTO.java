package dto;

import java.util.ArrayList;

/**
 * Created by Charmaine on 2015/05/09.
 */
public class DemorgraphicDTO {
    private int demorgraphicID;
    private int formID;
    private String race;
    private String age;
    private String gender;
    private String name;
    private String marital;
    private String employment;
    private String demograpg;
    private String income;
    private String personalData;

    private String dob;
    private String surname;
    private String companyco;
    private String email;
    private String address;

    private String inhousehold;
    private String municipality;


    public DemorgraphicDTO() {
    }

    public DemorgraphicDTO(int formID, String name, String demograpg) {
        this.formID = formID;
        this.name = name;
        this.demograpg = demograpg;
    }

    public String getMarital() {
        return marital;
    }

    public void setMarital(String marital) {
        this.marital = marital;
    }

    public String getEmployment() {
        return employment;
    }

    public void setEmployment(String employment) {
        this.employment = employment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDemograpg() {
        return demograpg;
    }

    public int getDemorgraphicID() {
        return demorgraphicID;
    }


    public int getFormID() {
        return formID;
    }

    public void setFormID(int formID) {
        this.formID = formID;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getPersonalData() {
        return personalData;
    }

    public void setPersonalData(String personalData) {
        this.personalData = personalData;
    }

    public String getCompanyco() {
        return companyco;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInhousehold() {
        return inhousehold;
    }

    public String getMunicipality() {
        return municipality;
    }


    public void setInhousehold(String inhousehold) {
        this.inhousehold = inhousehold;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

}


