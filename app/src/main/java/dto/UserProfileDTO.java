package dto;

/**
 * Created by iFluid-Dev on 2015-02-16.
 */
public class UserProfileDTO {
    private int profileID;
    private int userID;
    private byte[] profileImage;

    public UserProfileDTO() {
    }

    public UserProfileDTO(int profileID, byte[] profileImage ,int userID) {
        this.profileID = profileID;
        this.userID = userID;
        this.profileImage = profileImage;
    }

    public int getProfileID() {
        return profileID;
    }

    public void setProfileID(int profileID) {
        this.profileID = profileID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public byte[] getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(byte[] profileImage) {
        this.profileImage = profileImage;
    }
}
