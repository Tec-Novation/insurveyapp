package dto;

/**
 * Created by iFluid-Dev on 2015-02-03.
 */
public class UserDTO {
    private int userID;
    private String name;
    private String username;
    private String password;
    private String lastname;
    private String phone;
    private String email;
    private String physical_address;
    private String postal_address;
    private byte[] image;
    private int companyID;
    private int permissionID;

    private String companyName;
    private String description;
    private String CompanyPhone;
    private String companyEmail;


    public UserDTO() {
    }



    public UserDTO(int userID, String name, String username, String password, String phone, String email, String physical_address, String postal_address, int companyID, int permissionID,String lastname) {
        this.userID = userID;
        this.name = name;
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.email = email;
        this.physical_address = physical_address;
        this.postal_address = postal_address;
        this.companyID = companyID;
        this.permissionID = permissionID;
        this.lastname = lastname;
    }

    public UserDTO(int userID, String name, String username, String password, String lastname, String phone, String email, String physical_address, String postal_address, int companyID, int permissionID, String companyName, String description, String companyPhone, String companyEmail) {
        this.userID = userID;
        this.name = name;
        this.username = username;
        this.password = password;
        this.lastname = lastname;
        this.phone = phone;
        this.email = email;
        this.physical_address = physical_address;
        this.postal_address = postal_address;
        this.companyID = companyID;
        this.permissionID = permissionID;
        this.companyName = companyName;
        this.description = description;
        CompanyPhone = companyPhone;
        this.companyEmail = companyEmail;
    }
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompanyPhone() {
        return CompanyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        CompanyPhone = companyPhone;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhysical_address() {
        return physical_address;
    }

    public void setPhysical_address(String physical_address) {
        this.physical_address = physical_address;
    }

    public String getPostal_address() {
        return postal_address;
    }

    public void setPostal_address(String postal_address) {
        this.postal_address = postal_address;
    }

    public int getCompanyID() {
        return companyID;
    }

    public void setCompanyID(int companyID) {
        this.companyID = companyID;
    }

    public int getPermissionID() {
        return permissionID;
    }

    public void setPermissionID(int permissionID) {
        this.permissionID = permissionID;
    }
}
