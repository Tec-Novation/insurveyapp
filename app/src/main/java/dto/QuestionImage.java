package dto;

/**
 * Created by iFluid-Dev on 2016-04-15.
 */
public class QuestionImage {
    private Integer id_question_image;
    private String location;
    private Integer id_question_fk;
    private Integer id_form_fk;

    public QuestionImage() {
    }

    public QuestionImage(Integer id_question_image,
                         String location,
                         Integer id_question_fk,
                         Integer id_form_fk) {
        this.id_question_image = id_question_image;
        this.location = location;
        this.id_question_fk = id_question_fk;
        this.id_form_fk = id_form_fk;
    }

    public Integer getId_form_fk() {
        return id_form_fk;
    }

    public void setId_form_fk(Integer id_form_fk) {
        this.id_form_fk = id_form_fk;
    }

    public Integer getId_question_image() {
        return id_question_image;
    }

    public void setId_question_image(Integer id_question_image) {
        this.id_question_image = id_question_image;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getId_question_fk() {
        return id_question_fk;
    }

    public void setId_question_fk(Integer id_question_fk) {
        this.id_question_fk = id_question_fk;
    }
}
