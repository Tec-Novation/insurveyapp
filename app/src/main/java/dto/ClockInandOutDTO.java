package dto;

/**
 * Created by iFluid-Dev on 2015-02-27.
 */
public class ClockInandOutDTO {
    private String date;
    private String clockIn;
    private String clockOut;

    public ClockInandOutDTO() {
    }

    public ClockInandOutDTO(String date, String clockIn, String clockOut) {
        this.date = date;
        this.clockIn = clockIn;
        this.clockOut = clockOut;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClockIn() {
        return clockIn;
    }

    public void setClockIn(String clockIn) {
        this.clockIn = clockIn;
    }

    public String getClockOut() {
        return clockOut;
    }

    public void setClockOut(String clockOut) {
        this.clockOut = clockOut;
    }
}
