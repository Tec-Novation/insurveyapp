package dto;

/**
 * Created by Charmaine on 2015/06/04.
 */
public class SMSReport {

    private int _id_smsreport_type_fk;
    private int _id_form_fk;
    private int _id_user_fk;
    private int _id_person_fk;

    public SMSReport() {
    }

    public SMSReport(int _id_smsreport_type_fk,
                     int _id_person_fk,
                     int _id_user_fk,
                     int _id_form_fk) {
        this._id_smsreport_type_fk = _id_smsreport_type_fk;
        this._id_person_fk = _id_person_fk;
        this._id_user_fk = _id_user_fk;
        this._id_form_fk = _id_form_fk;
    }

    public int get_id_smsreport_type_fk() {
        return _id_smsreport_type_fk;
    }

    public void set_id_smsreport_type_fk(int _id_smsreport_type_fk) {
        this._id_smsreport_type_fk = _id_smsreport_type_fk;
    }

    public int get_id_form_fk() {
        return _id_form_fk;
    }

    public void set_id_form_fk(int _id_form_fk) {
        this._id_form_fk = _id_form_fk;
    }

    public int get_id_user_fk() {
        return _id_user_fk;
    }

    public void set_id_user_fk(int _id_user_fk) {
        this._id_user_fk = _id_user_fk;
    }

    public int get_id_person_fk() {
        return _id_person_fk;
    }

    public void set_id_person_fk(int _id_person_fk) {
        this._id_person_fk = _id_person_fk;
    }
}
