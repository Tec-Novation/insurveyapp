package dto;

/**
 * Created by Charmaine on 2015/05/28.
 */
public class ContactDTO {

    private int userID;
    private String phone;
    private int formID;

    public ContactDTO() {
    }

    public ContactDTO(int userID,
                      String phone,
                      int formID) {
        this.userID = userID;
        this.phone = phone;
        this.formID = formID;
    }

    public int getFormID() {
        return formID;
    }

    public void setFormID(int formID) {
        this.formID = formID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
