package dto;

/**
 * Created by iFluid-Dev on 2015-02-19.
 */
public class ActivityLogDTO {
    private int activitylogID;
    private int userID;
    private int activityID;
    private  int formID;
    public ActivityLogDTO() {
    }

    public ActivityLogDTO(int activitylogID, int userID, int activityID, int formID) {
        this.activitylogID = activitylogID;
        this.userID = userID;
        this.activityID = activityID;
        this.formID = formID;
    }

    public int getFormID() {
        return formID;
    }

    public void setFormID(int formID) {
        this.formID = formID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getActivityID() {
        return activityID;
    }

    public void setActivityID(int activityID) {
        this.activityID = activityID;
    }
}
