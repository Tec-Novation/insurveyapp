package dto;

/**
 * Created by Vic on 1/29/2015.
 */
public class SectionQuestionDTO {
    private int SectionID;
    private int FormID;
    private int  QuestionID;
    private String Name;
    private String JasonCode;
    private String Question;
    private String description;

    public SectionQuestionDTO(){

    }
    public SectionQuestionDTO(int sectionID,int formID,int questionID,String name,String jasonCode,String question,String description){
        this.SectionID = sectionID;
        this.FormID = formID;
        this.QuestionID = questionID;
        this.Name = name;
        this.JasonCode = jasonCode;
        this.Question = question;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSectionID() {
        return SectionID;
    }

    public void setSectionID(int sectionID) {
        SectionID = sectionID;
    }

    public int getFormID() {
        return FormID;
    }

    public void setFormID(int formID) {
        FormID = formID;
    }

    public int getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(int questionID) {
        QuestionID = questionID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getJasonCode() {
        return JasonCode;
    }

    public void setJasonCode(String jasonCode) {
        JasonCode = jasonCode;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }
}
