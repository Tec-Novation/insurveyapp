package dto;

/**
 * Created by Vic on 1/29/2015.
 */
public class QuestionDTO {
    private int questionID;
    private int sectionID;
    private String question;
    private String json;

    public  QuestionDTO(){

    }
    public QuestionDTO(int questionID,int sectionID,String question,String json){
        this.questionID = questionID;
        this.sectionID = sectionID;
        this.question = question;
        this.json = json;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
