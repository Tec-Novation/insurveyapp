package dto;

/**
 * Created by iFluid-Dev on 2015-03-11.
 */
public class SaveAnswersDTO {
    private int activitylogID;
    private int userID;
    private int activityID;
    private  int formID;
    private int answerID;
    private String answer;
    private int questionID;
    private int personID;
    private int serviceProvideID;
    private String name;
    private String lastname;
    private String phone;
    private String email;

    public SaveAnswersDTO() {
    }

    public SaveAnswersDTO(int activitylogID, int userID, int activityID, int formID, int answerID,
                          String answer, int questionID, int personID, int serviceProvideID, String name,
                          String lastname, String phone, String email) {
        this.activitylogID = activitylogID;
        this.userID = userID;
        this.activityID = activityID;
        this.formID = formID;
        this.answerID = answerID;
        this.answer = answer;
        this.questionID = questionID;
        this.personID = personID;
        this.serviceProvideID = serviceProvideID;
        this.name = name;
        this.lastname = lastname;
        this.phone = phone;
        this.email = email;
    }

    public int getActivitylogID() {
        return activitylogID;
    }

    public void setActivitylogID(int activitylogID) {
        this.activitylogID = activitylogID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getActivityID() {
        return activityID;
    }

    public void setActivityID(int activityID) {
        this.activityID = activityID;
    }

    public int getFormID() {
        return formID;
    }

    public void setFormID(int formID) {
        this.formID = formID;
    }

    public int getAnswerID() {
        return answerID;
    }

    public void setAnswerID(int answerID) {
        this.answerID = answerID;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public int getPersonID() {
        return personID;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }

    public int getServiceProvideID() {
        return serviceProvideID;
    }

    public void setServiceProvideID(int serviceProvideID) {
        this.serviceProvideID = serviceProvideID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
