package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/26/2015.
 */
public class FormTable {
    private static final String DATABASE_CREATE = "create table " + FD.FORM_TABLE
            + " (" + FD.FORM_ID + " integer, "
            + FD.FORM_CREATED_DATE + " text, "
            + FD.FORM_LAST_UPDATE + " text, "
            + FD.FORMSTATUS_ID + " integer, "
            + FD.FORM_DESCRIPTION + " text, "
            + FD.USER_ID + " integer, "
            + FD.FORM_SMS_DESCRIPTIOPN + " text, "
            + FD.FORM_SURVEY_LIMIT + " integer, "
            + FD.FORM_SURVEY_TOTAL + " integer, "
            + FD.FORM_NAME + " text  );";

    private static final String CREATE_INDEX = "create index indexForm on "
            + FD.FORM_TABLE + " ( " + FD.USER_ID + ")";
    private static final String CREATE_INDEX_1 = "create index indexForm1 on "
            + FD.FORM_TABLE + " ( " + FD.FORMSTATUS_ID + ")";
    public static void onCreate(SQLiteDatabase database) {
        Log.i("Form Table", "############# --> onCreate FormTable\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);
        database.execSQL(CREATE_INDEX_1);

        Log.d("Form Table", "--> Done creating indexes on FormTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.FORM_TABLE);
        onCreate(database);
    }
}
