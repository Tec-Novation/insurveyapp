package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/26/2015.
 */
public class ActivityTable {
    private static final String DATABASE_CREATE = "create table " + FD.ACTIVITY_TABLE
            + " (" + FD.ACTIVITY_ID + " integer, "
            + FD.ACTIVITY_DESCRIPTION + " text, "
            + FD.ACTIVITY_NAME + " text  );";
    public static void onCreate(SQLiteDatabase database) {
        Log.i("ActivityTable", "############# --> onCreate ActivityTable\n");

        Log.d("ActivityTable", "--> Done creating indexes on ActivityTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.ACTIVITY_TABLE);
        onCreate(database);
    }
}
