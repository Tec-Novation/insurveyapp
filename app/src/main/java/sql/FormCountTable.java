package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by iFluid-Dev on 2015-03-11.
 */
public class FormCountTable {
    private static final String DATABASE_CREATE = "create table " + FD.TABLE_FORM_COUNT
            + " (" + FD.FRM_COUNTID + " INTEGER PRIMARY KEY, "
            + FD.USER_ID + " integer, "
            + FD.FRM_COUNT_DATE + " text, "
            + FD.FRM_COUNT_DESCRIPTION + " text  );";

    private static final String CREATE_INDEX = "create index FormCountTable on "
            + FD.TABLE_FORM_COUNT + " ( " + FD.USER_ID + ")";

    public static void onCreate(SQLiteDatabase database) {
        Log.i("FormCountTable", "############# --> onCreate FormCountTable\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);

        Log.d("FormCountTable", "--> Done creating indexes on FormCountTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.TABLE_FORM_COUNT);
        onCreate(database);
    }
}
