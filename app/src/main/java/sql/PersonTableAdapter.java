package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.PersonDTO;

/**
 * Created by iFluid-Dev on 2015-02-19.
 */
public class PersonTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "PesronTableAdapter";

    public PersonTableAdapter(Context context) {
        this.context = context;
    }

    public PersonTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.i(TAG, "PesronTableAdapter table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.i(TAG, "PesronTableAdapter table closed");
    }
    private void setPersonID(){

    }
    public long insertPerson(PersonDTO f) throws SQLException {

        ContentValues v = createContentValues(f);
        long id = db.insert(FD.PERSON_TABLE, null, v);
        Log.i(TAG, "Person inserted,: " + f.getEmployment()+ " id: " + f.getUid());
        return id;
    }

    public int deletePersonDTO() throws SQLException {
        int deleted = db.delete(FD.PERSON_TABLE, null, null);
        Log.i(TAG, "Person deleted, count: "
                + deleted);
        return deleted;
    }

    public List<PersonDTO> getPersonsList(int personID) throws SQLException {

        Cursor cur = fetchFormByFormID(personID);
        ArrayList<PersonDTO> list = getList(cur);
        Log.i(TAG, "Person found: " + list.size());
        return list;
    }

    private ArrayList<PersonDTO> getList(Cursor cur) {
        ArrayList<PersonDTO> list = new ArrayList<PersonDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                PersonDTO c = getAnswers(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }

    public List<PersonDTO> getPersonList() throws SQLException {

        Cursor cur = fetchAnswersList();
        ArrayList<PersonDTO> list = getList(cur);
        Log.i(TAG, "personID found: " + list.size());
        return list;
    }
    private PersonDTO getAnswers(Cursor cur) {
        PersonDTO a = new PersonDTO();

        a.setPersonID(cur.getInt(cur.getColumnIndex(FD.PERSON_ID)));
        a.setUserID(cur.getInt(cur.getColumnIndex(FD.USER_ID)));
        a.setServiceProvideID(cur.getInt(cur.getColumnIndex(FD.SERVICE_PROVIDER_ID)));
        a.setName(cur.getString(cur.getColumnIndex(FD.PERSON_NAME)));
        a.setPhone(cur.getString(cur.getColumnIndex(FD.PERSON_PHONE)));
        a.setEmail(cur.getString(cur.getColumnIndex(FD.PERSON_EMAIL)));
        a.setLastname(cur.getString(cur.getColumnIndex(FD.PERSON_LAST_NAME)));
        a.setAge(cur.getString(cur.getColumnIndex(FD.PERSON_AGE)));
        a.setRace(cur.getString(cur.getColumnIndex(FD.PERSON_RACE)));
        a.setGender(cur.getString(cur.getColumnIndex(FD.PERSON_GENDER)));
        a.setMarital(cur.getString(cur.getColumnIndex(FD.PERSON_MARITAL)));
        a.setEmployment(cur.getString(cur.getColumnIndex(FD.PERSON_EMPLOYMENT)));
        a.setSms_recieve(cur.getInt(cur.getColumnIndex(FD.SMS_RECIEVE)));
        a.setUid(cur.getString(cur.getColumnIndex(FD.UID)));
        a.setDateOfbirth(cur.getString(cur.getColumnIndex(FD.PERSON_DATE_OF_BIRTH)));
        a.setIncome(cur.getString(cur.getColumnIndex(FD.PERSON_INCOME)));
        a.setNoOfOccupants(cur.getString(cur.getColumnIndex(FD.PERSON_NO_OCCUPANTS)));
        a.setCompanyPhone(cur.getString(cur.getColumnIndex(FD.PERSON_COMPANY_PHONE)));
        a.setAddress(cur.getString(cur.getColumnIndex(FD.ADDRESS)));
        a.setInhousehold(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_NUMBER_HOUSEHOLD)));
        a.setMunicipality(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_MUNICIPALITY)));

        return a;
    }

    private Cursor fetchFormByFormID(int userID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.PERSON_TABLE, cols, FD.USER_ID
                + " = " + userID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.i(TAG, "Persons: " + count);
        return mCursor;
    }
    private Cursor fetchAnswersList() throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.PERSON_TABLE, cols,
                null, null, null,
                null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.i(TAG, "classes: " + count);
        return mCursor;
    }
    public int updateUserDTO(PersonDTO c) throws SQLException {
        int updated = db.update(FD.PERSON_TABLE, createContentValues(c),
                FD.PERSON_ID + "= " + c.getPersonID(), null);
        Log.i(TAG, "Person updated, count: "
                + updated);
        return updated;
    }
    public int deletePerson(int userID) throws SQLException {
        int deleted = db.delete(FD.PERSON_TABLE, FD.USER_ID + "="
                + userID, null);
        Log.i(TAG, "Persons deleted, count: "
                + deleted);
        return deleted;
    }
    public int deleteByPersonID(int personID) throws SQLException {
        int deleted = db.delete(FD.PERSON_TABLE, FD.PERSON_ID + "="
                + personID, null);
        Log.i(TAG, "Persons deleted **********, count: "
                + deleted);
        return deleted;
    }
    public int deleteByPersonUserID(int userID) throws SQLException {
        int deleted = db.delete(FD.PERSON_TABLE, FD.USER_ID + "="
                + userID, null);
        Log.i(TAG, "Persons deleted**, count: "
                + deleted);
        return deleted;
    }
    private ContentValues createContentValues(PersonDTO a) {

        ContentValues v = new ContentValues();
        v.put(FD.SERVICE_PROVIDER_ID, a.getServiceProvideID());
        v.put(FD.USER_ID, a.getUserID());
        v.put(FD.PERSON_NAME, a.getName());
        v.put(FD.PERSON_PHONE, a.getPhone());
        v.put(FD.PERSON_EMAIL, a.getEmail());
        v.put(FD.PERSON_LAST_NAME, a.getLastname());
        v.put(FD.PERSON_AGE, a.getAge());
        v.put(FD.PERSON_RACE, a.getRace());
        v.put(FD.PERSON_GENDER, a.getGender());
        v.put(FD.PERSON_EMPLOYMENT, a.getEmployment());
        v.put(FD.PERSON_MARITAL, a.getMarital());
        v.put(FD.FORM_ID, a.getFormID());
        v.put(FD.SMS_RECIEVE, a.getSms_recieve());
        v.put(FD.UID, a.getUid());
        v.put(FD.PERSON_DATE_OF_BIRTH, a.getDateOfbirth());
        v.put(FD.PERSON_INCOME, a.getIncome());
        v.put(FD.PERSON_NO_OCCUPANTS, a.getNoOfOccupants());
        v.put(FD.PERSON_COMPANY_PHONE, a.getCompanyPhone());
        v.put(FD.ADDRESS, a.getAddress());
        v.put(FD.DERMOGRAPHIC_NUMBER_HOUSEHOLD, a.getInhousehold());
        v.put(FD.DERMOGRAPHIC_MUNICIPALITY, a.getMunicipality());

        return v;
    }

}
