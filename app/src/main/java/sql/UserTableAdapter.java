package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.UserDTO;

/**
 * Created by iFluid-Dev on 2015-02-05.
 */
public class UserTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "UserTableAdapter";

    public UserTableAdapter(Context context) {
        this.context = context;
    }

    public UserTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "UserTableAdapter table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG, "UserTableAdapter table closed");
    }
    public int updateUserPassword(UserDTO c) throws SQLException {
        db.update(FD.USER_TABLE, createContentValues(c),FD.USER_PASSWORD +" = ?"+c.getPassword(), null);
        int updated = db.update(FD.USER_TABLE, createContentValues(c),
                FD.USER_ID + "= " + c.getUserID(), null);
        Log.i(TAG, "USER updated, count: "
                + updated);
        return updated;
    }
    public int updateProfileImage(UserDTO c) throws SQLException {
      //  db.update(FD.USER_TABLE, createContentValues(c),FD.USER_IMAGE +" = ?"+c.getImage(), null);
        int updated = db.update(FD.USER_TABLE, createContentValues(c),
                FD.USER_ID + "= " + c.getUserID(), null);
        Log.i(TAG, "USER updated, count: "
                + updated);
        return updated;
    }
    public long insertUsers(UserDTO f) throws SQLException {

        ContentValues v = createContentValues(f);
        long id = db.insert(FD.USER_TABLE, null, v);
        Log.i(TAG, "Users inserted,: " + f.getName() + " id: " + id);
        return id;
    }
    public List<UserDTO> getList() throws SQLException {

        Cursor cur = fetchUserList();
        ArrayList<UserDTO> list = getList(cur);
        Log.i(TAG, "users found: " + list.size());
        return list;
    }
    public List<UserDTO> getListUserID(int userID) throws SQLException {

        Cursor cur = fetchUserID(userID);
        ArrayList<UserDTO> list = getList(cur);
        Log.i(TAG, "Users found: " + list.size());
        return list;
    }
    public List<UserDTO> getListUserID(String email,String password) throws SQLException {

        Cursor cur = fetchUserByEmailAndPassword(email,password);
        ArrayList<UserDTO> list = getList(cur);
        Log.i(TAG, "Users found by email and password: " + list.size());
        return list;
    }

    private ArrayList<UserDTO> getList(Cursor cur) {
        ArrayList<UserDTO> list = new ArrayList<UserDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                UserDTO c = getForms(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }
    private Cursor fetchUserList() throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.USER_TABLE, cols,
                null, null, null,
                null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "classes: " + count);
        return mCursor;
    }
    public int updateUserDTO(UserDTO c) throws SQLException {
        int updated = db.update(FD.USER_TABLE, createContentValues(c),
                FD.USER_ID + "= " + c.getUserID(), null);
        Log.i(TAG, "UserDTO updated, count: "
                + updated);
        return updated;
    }
    private UserDTO getForms(Cursor cur) {
        UserDTO a = new UserDTO();
        a.setUserID(cur.getInt(cur.getColumnIndex(FD.USER_ID)));
        a.setName(cur.getString(cur.getColumnIndex(FD.USER_NAME)));
        a.setLastname(cur.getString(cur.getColumnIndex(FD.USER_LASTNAME)));
        a.setUsername(cur.getString(cur.getColumnIndex(FD.USER_USERNAME)));
        a.setPassword(cur.getString(cur.getColumnIndex(FD.USER_PASSWORD)));
        a.setPhone(cur.getString(cur.getColumnIndex(FD.USER_PHONE)));
        a.setEmail(cur.getString(cur.getColumnIndex(FD.USER_EMAIL)));
        a.setPhysical_address(cur.getString(cur.getColumnIndex(FD.USER_PHYSICALADDRESS)));
        a.setPostal_address(cur.getString(cur.getColumnIndex(FD.USER_POSTALADDRESS)));
        a.setCompanyID(cur.getInt(cur.getColumnIndex(FD.USER_COMPANYID)));
        a.setPermissionID(cur.getInt(cur.getColumnIndex(FD.USER_PERMISSIONID)));
        a.setImage(cur.getBlob(cur.getColumnIndex(FD.USER_IMAGE)));
        return a;
    }

    private Cursor fetchUserID(int userID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.USER_TABLE, cols, FD.USER_ID
                + " = " + userID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "Users: " + count);
        return mCursor;
    }
    private Cursor fetchUserByEmailAndPassword(String email,String password) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.USER_TABLE, cols, FD.USER_EMAIL + " =? " + " and "+ FD.USER_PASSWORD + " =?",   new String[] {email,password}, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "Users: " + count);
        return mCursor;
    }
    private ContentValues createContentValues(UserDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.USER_ID, a.getUserID());
        v.put(FD.USER_NAME, a.getName());
        v.put(FD.USER_LASTNAME, a.getLastname());
        v.put(FD.USER_USERNAME, a.getUsername());
        v.put(FD.USER_PASSWORD, a.getPassword());
        v.put(FD.USER_PHONE, a.getPhone());
        v.put(FD.USER_EMAIL, a.getEmail());
        v.put(FD.USER_PHYSICALADDRESS, a.getPhysical_address());
        v.put(FD.USER_POSTALADDRESS, a.getPostal_address());
        v.put(FD.USER_PERMISSIONID, a.getPermissionID());
        v.put(FD.USER_COMPANYID, a.getCompanyID());
        v.put(FD.USER_IMAGE,a.getImage());

        return v;
    }
}

