package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.FormDTO;
import dto.QuestionImage;

/**
 * Created by Vic on 1/26/2015.
 */
public class QuestionImageTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "QuestionImageTableAdapter";

    public QuestionImageTableAdapter(Context context) {
        this.context = context;
    }

    public QuestionImageTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "QuestionImageTableAdapter table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG, "QuestionImageTableAdapter table closed");
    }

    public long insertQuestionImage(QuestionImage q) throws SQLException {

        ContentValues v = createContentValues(q);
        long id = db.insert(FD.QUESTION_IMAGE_TABLE, null, v);
        Log.i(TAG, "QuestionImage inserted,: " + q.getLocation() + " id: " + q.getId_question_fk());
        return id;
    }


    public ArrayList<QuestionImage> getFormList(int userID) throws SQLException {

        Cursor cur = fetchFormByUserID(userID);
        ArrayList<QuestionImage> list = getList(cur);
        Log.i(TAG, "QuestionImage found: " + list.size());
        return list;
    }
    public ArrayList<QuestionImage> getFormListByFormID(int formID) throws SQLException {

        Cursor cur = fetchFormByFormID(formID);
        ArrayList<QuestionImage> list = getList(cur);
        Log.i(TAG, "QuestionImage found: " + list.size());
        return list;
    }

    private ArrayList<QuestionImage> getList(Cursor cur) {
        ArrayList<QuestionImage> list = new ArrayList<>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                QuestionImage c = getForms(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }

    private QuestionImage getForms(Cursor cur) {

        QuestionImage a = new QuestionImage();

        a.setId_question_image(cur.getInt(cur.getColumnIndex(FD.ID_QUESTION_IMAGE)));
        a.setLocation(cur.getString(cur.getColumnIndex(FD.QUESTION_IMAGE_LOCAION)));
        a.setId_question_fk(cur.getInt(cur.getColumnIndex(FD.QUESTION_ID)));
        a.setId_form_fk(cur.getInt(cur.getColumnIndex(FD.FORM_ID)));

        return a;
    }

    private Cursor fetchFormByUserID(int formID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.QUESTION_IMAGE_TABLE, cols, FD.QUESTION_ID
                + " = " + formID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "forms: " + count);
        return mCursor;
    }
    private Cursor fetchFormByFormID(int formID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.QUESTION_IMAGE_TABLE, cols, FD.QUESTION_ID
                + " = " + formID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "QuestionImage: " + count);
        return mCursor;
    }
    private ContentValues createContentValues(QuestionImage a) {
        ContentValues v = new ContentValues();
        v.put(FD.ID_QUESTION_IMAGE, a.getId_question_image());
        v.put(FD.QUESTION_IMAGE_LOCAION, a.getLocation());
        v.put(FD.QUESTION_ID, a.getId_question_fk());
        v.put(FD.FORM_ID, a.getId_form_fk());

        return v;
    }

}
