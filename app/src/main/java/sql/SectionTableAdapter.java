package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.SectionDTO;

/**
 * Created by Vic on 1/29/2015.
 */
public class SectionTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "SectionTableAdapter";

    public SectionTableAdapter(Context context) {
        this.context = context;
    }

    public SectionTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "SectionTableAdapter table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG, "SectionTableAdapter table closed");
    }

    public long insertSections(SectionDTO f) throws SQLException {

        ContentValues v = createContentValues(f);
        long id = db.insert(FD.SECTION_TABLE, null, v);
        Log.i(TAG, "Sections inserted,: " + f.getName() + " id: " + id);
        return id;
    }

    public List<SectionDTO> getListFormID(int formID) throws SQLException {

        Cursor cur = fetchFormByFormID(formID);
        ArrayList<SectionDTO> list = getList(cur);
        Log.i(TAG, "Section found: " + list.size());
        return list;
    }

    private ArrayList<SectionDTO> getList(Cursor cur) {
        ArrayList<SectionDTO> list = new ArrayList<SectionDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                SectionDTO c = getForms(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }

    private SectionDTO getForms(Cursor cur) {
        SectionDTO a = new SectionDTO();
        a.setSectionID(cur.getInt(cur.getColumnIndex(FD.SECTION_ID)));
        a.setName(cur.getString(cur.getColumnIndex(FD.SECTION_NAME)));
        a.setFormID(cur.getInt(cur.getColumnIndex(FD.FORM_ID)));
        a.setJsonCode(cur.getString(cur.getColumnIndex(FD.SECTION_JSON_CODE)));
        return a;
    }

    private Cursor fetchFormByFormID(int formID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.SECTION_TABLE, cols, FD.FORM_ID
                + " = " + formID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "section: " + count);
        return mCursor;
    }

    private ContentValues createContentValues(SectionDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.SECTION_ID, a.getSectionID());
        v.put(FD.SECTION_NAME, a.getName());
        v.put(FD.FORM_ID, a.getFormID());
        v.put(FD.SECTION_JSON_CODE, a.getJsonCode());

        return v;
    }
}
