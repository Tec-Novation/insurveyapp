package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Charmaine on 2015/04/20.
 */
public class SurveyActionTable {
    private static final String DATABASE_CREATE = "create table " + FD.SURVEY_ACTION_TABLE
            + " (" + FD.SURVEY_ACTION_ID + " integer, "
            + FD.SURVEY_ACTION_PLACE + " text, "
            + FD.SURVEY_ACTION_START_DATE_TIME + " text, "
            + FD.SURVEY_ACTION_END_DATE_TIME + " text, "
            + FD.SURVEY_ACTION_START_CITY + " text, "
            + FD.SURVEY_ACTION_END_CITY + " text, "
            + FD.SURVEY_ACTION_START_LAT + " double, "
            + FD.SURVEY_ACTION_START_LONG + " double, "
            + FD.SURVEY_ACTION_END_LAT + " double, "
            + FD.SURVEY_ACTION_END_LONG + " double, "
            + FD.USER_ID + " integer, "
            + FD.FORM_ID + " integer  );";

    private static final String CREATE_INDEX = "create index indexsurveyactionUser on "
            + FD.SURVEY_ACTION_TABLE + " ( " + FD.USER_ID + ")";
    private static final String CREATE_INDEX_1 = "create index indexsurveyactionForm on "
            + FD.SURVEY_ACTION_TABLE + " ( " + FD.FORM_ID + ")";
    public static void onCreate(SQLiteDatabase database) {
        Log.i("SurveyActionTable", "############# --> onCreate SurveyActionTable\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);
        database.execSQL(CREATE_INDEX_1);

        Log.d("SurveyActionTable", "--> Done creating indexes on SurveyActionTable");
    }
    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.SURVEY_ACTION_TABLE);
        onCreate(database);
    }
}
