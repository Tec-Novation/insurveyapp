package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.FormDTO;

/**
 * Created by Vic on 1/26/2015.
 */
public class FormTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "FormTableAdapter";

    public FormTableAdapter(Context context) {
        this.context = context;
    }

    public FormTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "FormTableAdapter table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG, "FormTableAdapter table closed");
    }

    public long insertForm(FormDTO f) throws SQLException {

        ContentValues v = createContentValues(f);
        long id = db.insert(FD.FORM_TABLE, null, v);
        Log.i(TAG, "Forms inserted,: " + f.getName() + " id: " + id);
        return id;
    }
    public int updateForm(FormDTO c) throws SQLException {
        int updated = db.update(FD.FORM_TABLE, createContentValues(c),
                FD.FORM_ID + " = " + c.getFormID(), null);
        Log.i(TAG, "form updated, count: "
                + updated);
        return updated;
    }
    public int updateSurveyProgress(FormDTO c) throws SQLException {
        int updated = 0;
        List<FormDTO> lst = new ArrayList<>();
        lst = this.getFormListByFormID(c.getUserID());
        for(FormDTO f:lst){

            if(c.getFormID() == f.getFormID()){
                 updated = db.update(FD.FORM_TABLE, updsateTotalConentValue(c),
                        FD.FORM_ID + " = " + c.getFormID(), null);
                Log.i(TAG, "form progress updated, count: "
                        + updated);
            }
        }
        return updated;

    }

    public List<FormDTO> getFormList(int userID) throws SQLException {

        Cursor cur = fetchFormByUserID(userID);
        ArrayList<FormDTO> list = getList(cur);
        Log.i(TAG, "Forms found: " + list.size());
        return list;
    }
    public List<FormDTO> getFormListByFormID(int formID) throws SQLException {

        Cursor cur = fetchFormByFormID(formID);
        ArrayList<FormDTO> list = getList(cur);
        Log.i(TAG, "Forms found: " + list.size());
        return list;
    }

    private ArrayList<FormDTO> getList(Cursor cur) {
        ArrayList<FormDTO> list = new ArrayList<FormDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                FormDTO c = getForms(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }

    private FormDTO getForms(Cursor cur) {
        FormDTO a = new FormDTO();
        a.setFormID(cur.getInt(cur.getColumnIndex(FD.FORM_ID)));
        a.setName(cur.getString(cur.getColumnIndex(FD.FORM_NAME)));
        a.setLastUpdated(cur.getString(cur.getColumnIndex(FD.FORM_LAST_UPDATE)));
        a.setCreateDate(cur.getString(cur.getColumnIndex(FD.FORM_CREATED_DATE)));
        a.setDescription(cur.getString(cur.getColumnIndex(FD.FORM_DESCRIPTION)));
        a.setUserID(cur.getInt(cur.getColumnIndex(FD.USER_ID)));
        a.setFormStatusID(cur.getInt(cur.getColumnIndex(FD.FORMSTATUS_ID)));
        a.setFormStatusID(cur.getInt(cur.getColumnIndex(FD.FORMSTATUS_ID)));
        a.setSmsDescription(cur.getString(cur.getColumnIndex(FD.FORM_SMS_DESCRIPTIOPN)));
        a.setSurvey_limit(cur.getInt(cur.getColumnIndex(FD.FORM_SURVEY_LIMIT)));
        a.setSurvey_total(cur.getInt(cur.getColumnIndex(FD.FORM_SURVEY_TOTAL)));


        return a;
    }

    private Cursor fetchFormByUserID(int formID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.FORM_TABLE, cols, FD.USER_ID
                + " = " + formID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "forms: " + count);
        return mCursor;
    }
    private Cursor fetchFormByFormID(int formID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.FORM_TABLE, cols, FD.FORM_ID
                + " = " + formID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "forms: " + count);
        return mCursor;
    }
    private ContentValues createContentValues(FormDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.FORM_ID, a.getFormID());
        v.put(FD.FORM_NAME, a.getName());
        v.put(FD.FORM_LAST_UPDATE, a.getLastUpdated());
        v.put(FD.FORM_CREATED_DATE, a.getCreateDate());
        v.put(FD.FORM_DESCRIPTION, a.getDescription());
        v.put(FD.USER_ID, a.getUserID());
        v.put(FD.FORMSTATUS_ID, a.getFormStatusID());
        v.put(FD.FORM_SMS_DESCRIPTIOPN, a.getSmsDescription());
        v.put(FD.FORM_SURVEY_LIMIT, a.getSurvey_limit());
        v.put(FD.FORM_SURVEY_TOTAL, a.getSurvey_total());
        return v;
    }

    private ContentValues updsateTotalConentValue(FormDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.FORM_SURVEY_TOTAL, a.getSurvey_total());
        v.put(FD.FORM_SURVEY_LIMIT, a.getSurvey_limit());
        return v;
    }
}
