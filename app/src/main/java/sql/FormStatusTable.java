package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/26/2015.
 */
public class FormStatusTable {
    private static final String DATABASE_CREATE = "create table " + FD.FORM_STATUS_TABLE
            + " (" + FD.FORMSTATUS_ID + " integer, "
            + FD.FORM_STATUS_NAME + " text, "
            + FD.FORM_STATUS_DESCRIPTION + " text  );";
    public static void onCreate(SQLiteDatabase database) {
        Log.i("FormStatusTable", "############# --> onCreate FormStatusTable\n");

        Log.d("FormStatusTable", "--> Done creating indexes on FormStatusTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.FORM_STATUS_TABLE);
        onCreate(database);
    }
}
