package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.SaveAnswersDTO;

/**
 * Created by iFluid-Dev on 2015-03-11.
 */
public class SaveAnswerTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "SaveAnswerTableAdapter";

    public SaveAnswerTableAdapter(Context context) {
        this.context = context;
    }

    public SaveAnswerTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "SaveAnswerTableAdapter DB table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG, "SaveAnswerTableAdapter DB table closed");
    }

    public long insertSaveAnswers(SaveAnswersDTO c)
            throws SQLException {

        ContentValues v = createContentValues(c);
        long id = db.insert(FD.TABLE_SAVE_SURVEY, null, v);
        Log.i(TAG, "SaveAnswerTableAdapter log inserted,: " + c.getUserID() + " id: " + id);
        return id;
    }
    public int deleteSavedAnswers() throws SQLException {
        int deleted = db.delete(FD.TABLE_SAVE_SURVEY, null, null);
        Log.i(TAG, "SaveAnswerTableAdapter logs deleted, count: "
                + deleted);
        return deleted;
    }
    public int deleteAnswersDTO(int userID) throws SQLException {
        int deleted = db.delete(FD.TABLE_SAVE_SURVEY, FD.USER_ID + "="
                + userID, null);
        Log.i(TAG, "Answers deleted, count: "
                + deleted);
        return deleted;
    }
    public int deleteAnswerByPersonID(int personID) throws SQLException {
        int deleted = db.delete(FD.TABLE_SAVE_SURVEY, FD.PERSON_ID + "="
                + personID, null);
        Log.i(TAG, "Answers deleted, count: "
                + deleted);
        return deleted;
    }
    /*public boolean recordExists(ProvinceDTO c) {
        String[] cols = { FD.CLASS_YEAR };
        Cursor mCursor = db.query(true, FD.CLASS_TABLE, cols,
                FD.CLASS_NAME + "= '" + c.getClassName() + "' and " +
                FD.CLASS_YEAR + "=" + c.getClassYear(),
                null, null, null, null, null);
        int count = mCursor.getCount();
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }
    public int deleteProvinceDTO(long classKeyID) throws SQLException {
        int deleted = db.delete(FD.CLASS_TABLE, FD.KEY_ID + "="
                + classKeyID, null);
        Log.i(TAG, "ProvinceDTO deleted, count: "
                + deleted);
        return deleted;
    }
    public int updateProvinceDTO(ProvinceDTO c) throws SQLException {
        int updated = db.update(FD.CLASS_TABLE, createContentValues(c),
                FD.KEY_ID + "= " + c.getClassKeyID(), null);
        Log.i(TAG, "ProvinceDTO updated, count: "
                + updated);
        return updated;
    }
    */
    public List<SaveAnswersDTO> getSavedAnswers(int userID,int personID) throws SQLException {

        Cursor cur = fetchActivityLogByuserID(userID,personID);
        ArrayList<SaveAnswersDTO> list = getList(cur);
        Log.i(TAG, "Forms found: " + list.size());
        return list;
    }
    public List<SaveAnswersDTO> getList() throws SQLException {

        Cursor cur = fetchProvinceList();
        ArrayList<SaveAnswersDTO> list = getList(cur);
        Log.i(TAG, "avtivity log found: " + list.size());
        return list;
    }
    private Cursor fetchActivityLogByuserID(int userID,int personID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.TABLE_SAVE_SURVEY, cols, FD.USER_ID
                + " = " + userID +" AND "+ FD.PERSON_ID +" = "+ personID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "forms: " + count);
        return mCursor;
    }

    private ArrayList<SaveAnswersDTO> getList(Cursor cur) {
        ArrayList<SaveAnswersDTO> list = new ArrayList<SaveAnswersDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                SaveAnswersDTO c = getProvince(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }
    private SaveAnswersDTO getProvince(Cursor cur) {
        SaveAnswersDTO a = new SaveAnswersDTO();
        a.setActivitylogID(cur.getInt(cur.getColumnIndex(FD.ACTIVITY_LOG_ID)));
        a.setUserID(cur.getInt(cur.getColumnIndex(FD.USER_ID)));
        a.setActivityID(cur.getInt(cur.getColumnIndex(FD.ACTIVITY_ID)));
        a.setFormID(cur.getInt(cur.getColumnIndex(FD.FORM_ID)));
        a.setAnswerID(cur.getInt(cur.getColumnIndex(FD.ANSWER_ID)));
        a.setAnswer(cur.getString(cur.getColumnIndex(FD.ANSWER)));
        a.setQuestionID(cur.getInt(cur.getColumnIndex(FD.QUESTION_ID)));
        a.setPersonID(cur.getInt(cur.getColumnIndex(FD.PERSON_ID)));

        return a;
    }

    private Cursor fetchProvinceList() throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.TABLE_SAVE_SURVEY, cols,
                null, null, null,
                null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "classes: " + count);
        return mCursor;
    }

    private ContentValues createContentValues(SaveAnswersDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.USER_ID, a.getUserID());
        v.put(FD.ACTIVITY_ID, a.getActivityID());
        v.put(FD.FORM_ID, a.getFormID());
        v.put(FD.ANSWER_ID, a.getAnswerID());
        v.put(FD.ANSWER, a.getAnswer());
        v.put(FD.QUESTION_ID, a.getQuestionID());
        v.put(FD.PERSON_ID, a.getPersonID());
        return v;
    }

}
