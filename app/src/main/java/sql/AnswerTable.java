package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/26/2015.
 */
public class AnswerTable {
    private static final String DATABASE_CREATE = "create table " + FD.ANSWER_TABLE
            + " (" + FD.ANSWER_ID + " integer, "
            + FD.ANSWER + " text, "
            + FD.QUESTION_ID + " integer, "
            + FD.ACTIVITY_ID + " integer, "
            + FD.PERSON_ID + " integer  );";

    private static final String CREATE_INDEX = "create index indexAnswer1 on "
            + FD.ANSWER_TABLE + " ( " + FD.QUESTION_ID + ")";
    private static final String CREATE_INDEX_1 = "create index indexAnswer2 on "
            + FD.ANSWER_TABLE + " ( " + FD.ACTIVITY_ID + ")";

    private static final String CREATE_INDEX_2 = "create index indexAnswer3 on "
            + FD.ANSWER_TABLE + " ( " + FD.PERSON_ID + ")";

    public static void onCreate(SQLiteDatabase database) {
        Log.i("AnswerTable", "############# --> onCreate AnswerTable\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);
        database.execSQL(CREATE_INDEX_1);
        database.execSQL(CREATE_INDEX_2);

        Log.i("AnswerTable", "--> Done creating indexes on AnswerTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.i(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.ANSWER_TABLE);
        onCreate(database);
    }
}
