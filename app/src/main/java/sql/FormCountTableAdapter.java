package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.FormCountDTO;

/**
 * Created by iFluid-Dev on 2015-03-11.
 */
public class FormCountTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "FormCountTableAdapter";

    public FormCountTableAdapter(Context context) {
        this.context = context;
    }

    public FormCountTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "FormCountTableAdapter table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG, "FormCountTableAdapter table closed");
    }

    public long insertFormCount(FormCountDTO f) throws SQLException {

        ContentValues v = createContentValues(f);
        long id = db.insert(FD.TABLE_FORM_COUNT, null, v);
        Log.i(TAG, "FormCountTableAdapter inserted,: " + f.getDescription() + " id: " + id);
        return id;
    }
    public List<FormCountDTO> getFormCountList(int userID) throws SQLException {

        Cursor cur = fetchFormByFormID(userID);
        ArrayList<FormCountDTO> list = getList(cur);
        Log.i(TAG, "FormCountTableAdapter found: " + list.size());
        return list;
    }
    public ArrayList<FormCountDTO> getFormListArrayList(int userID) throws SQLException {

        Cursor cur = fetchFormByFormID(userID);
        ArrayList<FormCountDTO> list = getList(cur);
        Log.i(TAG, "FormCountTableAdapter found: " + list.size());
        return list;
    }
    public int deleteFormCount() throws SQLException {
        int deleted = db.delete(FD.TABLE_FORM_COUNT, null, null);
        Log.i(TAG, "formCount deleted, count: "
                + deleted);
        return deleted;
    }
    public int deleteFormCount(int userID) throws SQLException {
        int deleted = db.delete(FD.TABLE_FORM_COUNT, FD.USER_ID + "="
                + userID, null);
        Log.i(TAG, "form Count deleted, count: "
                + deleted);
        return deleted;
    }
    private ArrayList<FormCountDTO> getList(Cursor cur) {
        ArrayList<FormCountDTO> list = new ArrayList<FormCountDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                FormCountDTO c = getForms(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }

    private FormCountDTO getForms(Cursor cur) {
        FormCountDTO a = new FormCountDTO();
        a.setUserID(cur.getInt(cur.getColumnIndex(FD.USER_ID)));
        a.setFrmCountID(cur.getInt(cur.getColumnIndex(FD.FRM_COUNTID)));
        a.setDescription(cur.getString(cur.getColumnIndex(FD.FRM_COUNT_DESCRIPTION)));
        a.setDate(cur.getString(cur.getColumnIndex(FD.FRM_COUNT_DATE)));

        return a;
    }

    private Cursor fetchFormByFormID(int formID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.TABLE_FORM_COUNT, cols, FD.USER_ID
                + " = " + formID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "forms: " + count);
        return mCursor;
    }

    private ContentValues createContentValues(FormCountDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.USER_ID, a.getUserID());
        v.put(FD.FRM_COUNT_DESCRIPTION, a.getDescription());
        v.put(FD.FRM_COUNT_DATE, a.getDate());


        return v;
    }
}
