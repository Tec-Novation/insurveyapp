package sql;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.DemorgraphicDTO;

/**
 * Created by Charmaine on 2015/05/09.
 */
@SuppressLint("LongLogTag")
public class DemorgraphicTableAdapter {

    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "DemorgraphicTableAdapter";

    public DemorgraphicTableAdapter(Context context) {
        this.context = context;
    }

    public DemorgraphicTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.i(TAG, "Demorgraphic table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.i(TAG, "DemorgraphicTableAdapter table closed");
    }

    public long insertDemographic(DemorgraphicDTO f) throws SQLException {

        ContentValues v = createContentValues(f);
        long id = db.insert(FD.DERMOGRAPHIC_DATA_TABLE, null, v);
        Log.i(TAG, "demo inserted,: " + f.getRace() + " id: " + id);
        return id;
    }
    public List<DemorgraphicDTO> getDemoList(int formID) throws SQLException {

        ArrayList<DemorgraphicDTO> lst = new ArrayList<>();
        Cursor cur = fetchFormByFormID(formID);
        ArrayList<DemorgraphicDTO> list = getList(cur);
        Log.i(TAG, "demo found: " + list.size());

        int x = 0;

        for(DemorgraphicDTO d:list){

            if(d.getMarital().toString().length()  > 0){

                lst.add(d);
            }

            if(d.getEmployment().toString().length()  > 0){
                lst.add(d);
            }

            if(d.getRace().toString().length() > 0){
                lst.add(d);
            }

            if(d.getGender().toString().length()  > 0){
                lst.add(d);
            }

            if(d.getAge().toString().length()  > 0){
                lst.add(d);
            }

            if(d.getIncome().toString().length()  > 0){
                lst.add(d);
            }

            if(d.getPersonalData().toString().length()  > 0){
                lst.add(d);
            }

            if(d.getInhousehold().toString().length()  > 0){
                lst.add(d);
            }

            if(d.getMunicipality().toString().length()  > 0){
                lst.add(d);
            }
        }
        x++;
//        demographicDTO = new DemorgraphicDTO(jso.getInt("id_form_fk"),
//                jso.getString("name"),
//                jso.getString("demograph"));
//
//        if(demographicDTO.getName().equals("Gender")){
//            Log.i("Gender %%%% ",""+i);
//            ed.putString("gender" + i, demographicDTO.getDemograpg());
//            ed.commit();
//        }
//
//        if(demographicDTO.getName().equals("Age")){
//            Log.i("Age ",""+i);
//            ed.putString("age"+i,demographicDTO.getDemograpg());
//            ed.commit();
//        }
//        if(demographicDTO.getName().equals("Race")){
//            Log.i("Race ",""+i);
//            ed.putString("race"+i,demographicDTO.getDemograpg());
//            ed.commit();
//        }
//
//        if(demographicDTO.getName().equals("Marital ")){
//            Log.i("Marital ",""+i);
//            ed.putString("marital"+i,demographicDTO.getDemograpg());
//            ed.commit();
//        }
//
//        if(demographicDTO.getName().equals("Employment")){
//            Log.i("Employment ",""+i);
//            ed.putString("employment"+i,demographicDTO.getDemograpg());
//            ed.commit();
//        }
//
//        if(demographicDTO.getName().equals("Income ")){
//            Log.i("Income ",""+demographicDTO.getDemograpg());
//            ed.putString("income"+i,demographicDTO.getDemograpg());
//            ed.commit();
//        }
//
//        if(demographicDTO.getName().equals("PersonalData")){
//            Log.i("PersonalData ",""+i);
//            ed.putString("personalData"+i,demographicDTO.getDemograpg());
//            ed.commit();
//        }
//
//    }
//
        return lst;
    }

    private ArrayList<DemorgraphicDTO> getList(Cursor cur) {
        ArrayList<DemorgraphicDTO> list = new ArrayList<DemorgraphicDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                DemorgraphicDTO c = getAnswers(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }
    public List<DemorgraphicDTO> getList() throws SQLException {

        Cursor cur = fetchAnswersList();
        ArrayList<DemorgraphicDTO> list = getList(cur);
        Log.i(TAG, "demo found: " + list.size());
        return list;
    }
    public int deleteAnswersDTO(int formID) throws SQLException {
        int deleted = db.delete(FD.DERMOGRAPHIC_DATA_TABLE, FD.FORM_ID + "="
                + formID, null);
        Log.i(TAG, "demo deleted, count: "
                + deleted);
        return deleted;
    }

    public int deleteDemos(int formID) throws SQLException {
        int deleted = db.delete(FD.DERMOGRAPHIC_DATA_TABLE, null, null);
        Log.i(TAG, "DERMOGRAPHIC_TABLE logs deleted, count: "
                + deleted);
        return deleted;
    }
    private DemorgraphicDTO getAnswers(Cursor cur) {
        DemorgraphicDTO a = new DemorgraphicDTO();

        a.setAge(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_AGE)));
        a.setRace(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_RACE)));
        a.setGender(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_GENDER)));
        a.setMarital(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_MARITAL)));
        a.setEmployment(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_EMPLOYMENT)));
        a.setIncome(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_INCOME)));
        a.setPersonalData(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_PERSONAL_DATA)));
        a.setInhousehold(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_NUMBER_HOUSEHOLD)));
        a.setMunicipality(cur.getString(cur.getColumnIndex(FD.DERMOGRAPHIC_MUNICIPALITY)));

        a.setFormID(cur.getInt(cur.getColumnIndex(FD.FORM_ID)));

        return a;
    }

    private Cursor fetchFormByFormID(int formID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.DERMOGRAPHIC_DATA_TABLE, cols, FD.FORM_ID
                + " = " + formID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.i(TAG, "demo: " + count);
        return mCursor;
    }
    private Cursor fetchAnswersList() throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.DERMOGRAPHIC_DATA_TABLE, cols,
                null, null, null,
                null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.i(TAG, "classes: " + count);
        return mCursor;
    }
    private ContentValues createContentValues(DemorgraphicDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.FORM_ID, a.getFormID());
        v.put(FD.DERMOGRAPHIC_AGE, a.getAge());
        v.put(FD.DERMOGRAPHIC_RACE, a.getRace());
        v.put(FD.DERMOGRAPHIC_MARITAL, a.getMarital());
        v.put(FD.DERMOGRAPHIC_EMPLOYMENT, a.getEmployment());
        v.put(FD.DERMOGRAPHIC_GENDER, a.getGender());
        v.put(FD.DERMOGRAPHIC_INCOME, a.getIncome());
        v.put(FD.DERMOGRAPHIC_PERSONAL_DATA, a.getPersonalData());
        v.put(FD.DERMOGRAPHIC_NUMBER_HOUSEHOLD, a.getInhousehold());
        v.put(FD.DERMOGRAPHIC_MUNICIPALITY, a.getMunicipality());

        return v;
    }
}
