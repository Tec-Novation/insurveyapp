package sql;

public class FD {
//Tables
	public static final String DATABASE_NAME = "dbinsurvey";
	public static final String FORM_TABLE = "form";
	public static final String SECTION_TABLE = "section";
    public static final String QUESTION_TABLE = "question";
	public static final String ANSWER_TABLE = "answer";
    public static final String PERSON_TABLE = "person";
    public static final String SERVICE_PROVIDER_TABLE = "service_provider";
    public static final String FORM_STATUS_TABLE = "form_status";
    public static final String ACTIVITY_LOG_TABLE = "activity_log";
    public static final String ACTIVITY_TABLE = "activity";
    public static final String USER_TABLE = "user";
    public static final String TABLE_FORM_COUNT = "table_count";
    public static final String TABLE_SAVE_SURVEY= "save_survey";
    public static final String TABLE_PROFILE = "profile";
    public static  final  String SECTION_QUESTION_TABLE = "sectionquestion";
    public static final String SURVEY_ACTION_TABLE = "survey_action_table";
    public static final String DERMOGRAPHIC_TABLE = "demographic";
    public static final String DERMOGRAPHIC_DATA_TABLE = "demographicdata";
    public static final String CONTACT_TABLE = "contact";
    public static final String QUESTION_IMAGE_TABLE = "question_image";




    //Cloumns FOR FORM TABLE
	public static final String FORM_ID = "FormID";
	public static final String USER_ID = "UserID";
    public static final String FORMSTATUS_ID = "FormStatusID";
	public static final String FORM_NAME = "Name";
    public static final String FORM_DESCRIPTION = "Description";
    public static final String FORM_CREATED_DATE = "CreateDate";
    public static final String FORM_LAST_UPDATE = "LastUpdated";
    public static final String FORM_SMS_DESCRIPTIOPN= "sms_description";
    public static final String FORM_SURVEY_LIMIT= "survey_limit";
    public static final String FORM_SURVEY_TOTAL= "survey_total";

    //Cloumns FOR SECTION TABLE
	public static final String SECTION_ID = "SectionID";
	public static final String SECTION_NAME = "Name";
	public static final String SECTION_JSON_CODE = "JasonCode";

    //Cloumns FOR QUESTIONS TABLE

    public static final String QUESTION_ID = "QuestionID";
    public static final String QUESTION = "Question";
    public static final String SECTION_DESCRIPTION = "description";

    //Cloumns FOR ANSWER TABLE



    //Cloumns FOR Activity log TABLE
    public static final String ACTIVITY_LOG_ID = "ActivityLogID";
    //Cloumns FOR PERSON TABLE

    public static final String PERSON_ID = "PersonID";
    public static final String PERSON_NAME = "Name";
    public static final String PERSON_LAST_NAME = "Lastname";
    public static final String PERSON_PHONE = "Phone";
    public static final String PERSON_EMAIL = "Email";
    public static final String PERSON_AGE = "Age";
    public static final String PERSON_RACE = "Race";
    public static final String PERSON_GENDER = "gender";
    public static final String PERSON_MARITAL = "marital";
    public static final String PERSON_EMPLOYMENT = "employment";
    public static final String PERSON_SURNAME= "surname";
    public static final String PERSON_DATE_OF_BIRTH = "person_dateOfbirth";
    public static final String PERSON_INCOME= "income";
    public static final String PERSON_NO_OCCUPANTS = "noOfOccupants";
    public static final String PERSON_COMPANY_PHONE= "company_phone";



    //Cloumns FOR SERVICE PROVIDER TABLE

    public static final String SERVICE_PROVIDER_ID = "ServiceProviderID";
    public static final String SERVICE_PROVIDER_NAME = "dispensaryName";

    //Cloumns FOR FORM STATUS TABLE

    public static final String FORM_STATUS_DESCRIPTION = "Description";
    public static final String FORM_STATUS_NAME = "Name";

    //Cloumns FOR FORM Activity TABLE



    //Cloumns FOR FORM User TABLE
    public static final String USER_NAME = "Name";
    public static final String USER_LASTNAME = "lastname";
    public static final String USER_USERNAME = "username";
    public static final String USER_PASSWORD = "password";
    public static final String USER_PHONE = "phone";
    public static final String USER_EMAIL = "email";
    public static final String USER_PHYSICALADDRESS = "physicaladdress";
    public static final String USER_POSTALADDRESS = "postalAddress";
    public static final String USER_PERMISSIONID = "permissionid";
    public static final String USER_COMPANYID = "companyID";
    public static final String USER_IMAGE = "image";



    //Table for form count
    public static  final String FRM_COUNTID = "form_c_id";
    public static final  String FRM_COUNT_DESCRIPTION = "frm_count_description";
    public static final String FRM_COUNT_DATE="frm_count_date";
    //Table for save survey

    public static  final  String SAVE_SURVEYID = "save_ID";
    public static final String ACTIVITY_ID = "ActivityID";
    public static final String ACTIVITY_NAME = "Name";
    public static final String ACTIVITY_DESCRIPTION = "Descripition";
    public static final String ANSWER_ID = "AnswerID";
    public static final String ANSWER = "Answer";

    //Table for SURVEY_ACTION_TABLE


    public static final String SURVEY_ACTION_ID = "survey_action_id";
    public static final String SURVEY_ACTION_PLACE = "survey_action_place";
    public static final String SURVEY_ACTION_START_DATE_TIME = "survey_action_start_datetime";
    public static final String SURVEY_ACTION_END_DATE_TIME = "survey_action_end_datetime";
    public static final String SURVEY_ACTION_START_LAT = "survey_action_start_lat";
    public static final String SURVEY_ACTION_START_LONG = "survey_action_start_long";

    public static final String SURVEY_ACTION_END_LAT = "survey_action_end_lat";
    public static final String SURVEY_ACTION_END_LONG = "survey_action_end_long";
    public static  final String SURVEY_ACTION_START_CITY = "survey_action_start_city";
    public static  final String SURVEY_ACTION_END_CITY = "survey_action_end_city";

    public static final String DERMOGRAPHIC_ID = "demoid";
    public static final String DERMOGRAPHIC_GENDER = "gender";
    public static final String DERMOGRAPHIC_RACE = "race";
    public static final String DERMOGRAPHIC_AGE = "age";
    public static final String DERMOGRAPHIC_PERSONAL_DATA = "demo_personal_data";
    public static final String DERMOGRAPHIC_INCOME = "demo_income";
    public static final String DERMOGRAPHIC_EMPLOYMENT = "employment";
    public static final String DERMOGRAPHIC_MARITAL = "marital";


    public static final String CONTACT_ID = "contact_id";
    public static final String CONTACT_PHONE = "contact_phone";

    public static final String ID_QUESTION_IMAGE = "id_questin_image";
    public static final String QUESTION_IMAGE_LOCAION = "question_image_location";


    public static final String SMS_RECIEVE = "sms_recieve";

    public static final String UID = "uid";

    public static final String SAVE_STATUS = "save_status";


    public static final String ADDRESS = "person_adress";

    public static final String DERMOGRAPHIC_MUNICIPALITY = "municipality";
    public static final String DERMOGRAPHIC_NUMBER_HOUSEHOLD = "inhousehold";

}
