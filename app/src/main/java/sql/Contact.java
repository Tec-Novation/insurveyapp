package sql;

public class Contact {

	// private variables
	int _id;
	String _name;
	byte[] _image;
    int _userid;
	// Empty constructor
	public Contact() {

	}

	// constructor
	public Contact(int keyId, String name, byte[] image,int userid) {
		this._id = keyId;
		this._name = name;
		this._image = image;
        this._userid = userid;

	}
	public Contact(int _userid ,String name, byte[] image) {
		this._name = name;
		this._image = image;
        this._userid = _userid;

	}
	public Contact(int keyId) {
		this._id = keyId;

	}

	// getting ID
	public int getID() {
		return this._id;
	}

    public int get_userid() {
        return _userid;
    }

    public void set_userid(int _userid) {
        this._userid = _userid;
    }

    // setting id
	public void setID(int keyId) {
		this._id = keyId;
	}

	// getting name
	public String getName() {
		return this._name;
	}

	// setting name
	public void setName(String name) {
		this._name = name;
	}

	// getting phone number
	public byte[] getImage() {
		return this._image;
	}

	// setting phone number
	public void setImage(byte[] image) {
		this._image = image;
	}
}
