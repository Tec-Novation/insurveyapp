package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/26/2015.
 */
public class ServiceProviderTable {
    private static final String DATABASE_CREATE = "create table " + FD.SERVICE_PROVIDER_TABLE
            + " (" + FD.SERVICE_PROVIDER_ID + " integer, "
            + FD.SERVICE_PROVIDER_NAME + " text  );";

    public static void onCreate(SQLiteDatabase database) {
        Log.i("Form Table", "############# --> onCreate FormTable\n");
        database.execSQL(DATABASE_CREATE);

        Log.d("Form Table", "--> Done creating indexes on FormTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.SERVICE_PROVIDER_TABLE);
        onCreate(database);
    }
}
