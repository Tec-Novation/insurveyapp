package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by iFluid-Dev on 2015-03-11.
 */
public class SaveAnswerTable {
    private static final String DATABASE_CREATE = "create table " + FD.TABLE_SAVE_SURVEY
            + " (" + FD.SAVE_SURVEYID + " INTEGER PRIMARY KEY, "
            + FD.ACTIVITY_ID + " integer, "
            + FD.USER_ID + " integer, "
            + FD.ACTIVITY_LOG_ID + " integer, "
            + FD.FORM_ID + " integer, "
            + FD.ANSWER_ID + " integer, "
            + FD.ANSWER + " text, "
            + FD.PERSON_PHONE + " text, "
            + FD.PERSON_NAME + " text, "
            + FD.PERSON_LAST_NAME + " text, "
            + FD.QUESTION_ID + " integer, "
            + FD.PERSON_ID + " integer, "
            +" FOREIGN KEY("+ FD.PERSON_ID +") REFERENCES "+FD.PERSON_TABLE+"("+FD.SAVE_SURVEYID+") );";


    public static void onCreate(SQLiteDatabase database) {
        Log.i("SaveAnswerTable", "############# --> onCreate SaveAnswerTable\n");
        database.execSQL(DATABASE_CREATE);
       // database.execSQL(CREATE_INDEX);

        Log.d("SaveAnswerTable", "--> Done creating indexes on SaveAnswerTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.TABLE_SAVE_SURVEY);
        onCreate(database);
    }
}
