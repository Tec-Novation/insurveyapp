package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.ContactDTO;

/**
 * Created by Charmaine on 2015/05/28.
 */
public class ContactTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "ContactTableAdapter";

    public ContactTableAdapter(Context context) {
        this.context = context;
    }

    public ContactTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "ContactTableAdapter DB table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG, "ContactTableAdapter DB table closed");
    }

    public long insertContact(ContactDTO c)
            throws SQLException {

        ContentValues v = createContentValues(c);
        long id = db.insert(FD.CONTACT_TABLE, null, v);
        Log.i(TAG, "contact log inserted,: " + c.getUserID()+ " id: " + id);
        return id;
    }
    public int deleteContact() throws SQLException {
        int deleted = db.delete(FD.CONTACT_TABLE, null, null);
        Log.i(TAG, "contact logs deleted, count: "
                + deleted);
        return deleted;
    }
    public int deleteContact(int userID,int formID) throws SQLException {
        int deleted = db.delete(FD.CONTACT_TABLE, FD.USER_ID + "="
                + userID + " AND "+FD.FORM_ID+"="+formID , null);
        Log.i(TAG, "contatc deleted, count: "
                + deleted);
        return deleted;
    }

    public List<ContactDTO> getContactList(int userID) throws SQLException {

        Cursor cur = fetchContactByUserID(userID);
        ArrayList<ContactDTO> list = getList(cur);
        Log.i(TAG, "contact found: " + list.size());
        return list;
    }
    public List<ContactDTO> getList() throws SQLException {

        Cursor cur = fetchProvinceList();
        ArrayList<ContactDTO> list = getList(cur);
        Log.i(TAG, "contact log found: " + list.size());
        return list;
    }
    private Cursor fetchContactByUserID(int userID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.CONTACT_TABLE, cols, FD.USER_ID
                + " = " + userID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "contacts: " + count);
        return mCursor;
    }

    private ArrayList<ContactDTO> getList(Cursor cur) {
        ArrayList<ContactDTO> list = new ArrayList<ContactDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                ContactDTO c = getProvince(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }
    private ContactDTO getProvince(Cursor cur) {
        ContactDTO a = new ContactDTO();
        a.setUserID(cur.getInt(cur.getColumnIndex(FD.USER_ID)));
        a.setPhone(cur.getString(cur.getColumnIndex(FD.CONTACT_PHONE)));
        a.setFormID(cur.getInt(cur.getColumnIndex(FD.FORM_ID)));

        return a;
    }

    private Cursor fetchProvinceList() throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.CONTACT_TABLE, cols,
                null, null, null,
                null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "classes: " + count);
        return mCursor;
    }

    private ContentValues createContentValues(ContactDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.USER_ID, a.getUserID());
        v.put(FD.FORM_ID, a.getFormID());
        v.put(FD.CONTACT_PHONE, a.getPhone());
        return v;
    }
}
