package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.QuestionDTO;

/**
 * Created by Vic on 1/29/2015.
 */
public class QuestionTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "QuestionTableAdapter";

    public QuestionTableAdapter(Context context) {
        this.context = context;
    }

    public QuestionTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "QuestionTableAdapter table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG, "QuestionTableAdapter table closed");
    }

    public long insertQuestions(QuestionDTO f) throws SQLException {

        ContentValues v = createContentValues(f);
        long id = db.insert(FD.QUESTION_TABLE, null, v);
        Log.i(TAG, "Question inserted,: " + f.getQuestion() + " id: " + id);
        return id;
    }

    public List<QuestionDTO> getQuestionByID(int formID) throws SQLException {

        Cursor cur = fetchFormByFormID(formID);
        ArrayList<QuestionDTO> list = getList(cur);
        Log.i(TAG, "Question found: " + list.size());
        return list;
    }

    private ArrayList<QuestionDTO> getList(Cursor cur) {
        ArrayList<QuestionDTO> list = new ArrayList<QuestionDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                QuestionDTO c = getForms(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }

    private QuestionDTO getForms(Cursor cur) {
        QuestionDTO a = new QuestionDTO();
        a.setSectionID(cur.getInt(cur.getColumnIndex(FD.SECTION_ID)));
        a.setQuestionID(cur.getInt(cur.getColumnIndex(FD.QUESTION_ID)));
        a.setQuestion(cur.getString(cur.getColumnIndex(FD.QUESTION)));
        a.setJson(cur.getString(cur.getColumnIndex(FD.SECTION_JSON_CODE)));
        return a;
    }

    private Cursor fetchFormByFormID(int formID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.QUESTION_TABLE, cols, FD.QUESTION_ID
                + " = " + formID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "section: " + count);
        return mCursor;
    }

    private ContentValues createContentValues(QuestionDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.SECTION_ID, a.getSectionID());
        v.put(FD.QUESTION_ID, a.getQuestionID());
        v.put(FD.QUESTION, a.getQuestion());
        v.put(FD.SECTION_JSON_CODE, a.getJson());

        return v;
    }
}

