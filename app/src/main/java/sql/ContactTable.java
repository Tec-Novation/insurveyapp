package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Charmaine on 2015/05/28.
 */
public class ContactTable {

    private static final String DATABASE_CREATE = "create table " + FD.CONTACT_TABLE
            + " (" + FD.FORM_ID + " integer, "
            + FD.CONTACT_PHONE + " text, "
            + FD.USER_ID + " integer  );";

    private static final String CREATE_INDEX = "create index indexContact on "
            + FD.CONTACT_TABLE + " ( " + FD.USER_ID + ")";
    private static final String CREATE_INDEX_1 = "create index indexContact1 on "
            + FD.CONTACT_TABLE + " ( " + FD.FORM_ID + ")";
    public static void onCreate(SQLiteDatabase database) {
        Log.i("Contact Tablee", "############# --> onCreate Contact Table\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);
        database.execSQL(CREATE_INDEX_1);

        Log.d("Contact Table", "--> Done creating indexes on Contact Table");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.CONTACT_TABLE);
        onCreate(database);
    }
}
