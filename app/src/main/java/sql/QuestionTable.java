package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/26/2015.
 */
public class QuestionTable {
    private static final String DATABASE_CREATE = "create table " + FD.QUESTION_TABLE
            + " (" + FD.QUESTION_ID + " integer, "
            + FD.SECTION_ID + " integer, "
            + FD.SECTION_JSON_CODE + " text, "
            + FD.QUESTION + " text  );";

    private static final String CREATE_INDEX = "create index indexQuestion on "
            + FD.QUESTION_TABLE + " ( " + FD.SECTION_ID + ")";
    public static void onCreate(SQLiteDatabase database) {
        Log.i("QuestionTable", "############# --> onCreate QuestionTable\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);

        Log.d("QuestionTable", "--> Done creating indexes on QuestionTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.QUESTION_TABLE);
        onCreate(database);
    }
}
