package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.SurveyActionDTO;

/**
 * Created by Charmaine on 2015/04/20.
 */
public class SurveyActionTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "SurveyActionTableAdapter";

    public SurveyActionTableAdapter(Context context) {
        this.context = context;
    }

    public SurveyActionTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "SurveyActionTableAdapter table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG, "SurveyActio table closed");
    }

    public int updateSurveyAction(SurveyActionDTO c) throws SQLException {
        //  db.update(FD.USER_TABLE, createContentValues(c),FD.USER_IMAGE +" = ?"+c.getImage(), null);
        int updated = db.update(FD.SURVEY_ACTION_TABLE, createContentValues(c),
                FD.USER_ID + "= " + c.getId_survey_action(), null);
        Log.i(TAG, "SURVEY, count: "
                + updated);
        return updated;
    }
    public long insertSurveyAction(SurveyActionDTO f) throws SQLException {

        ContentValues v = createContentValues(f);
        long id = db.insert(FD.SURVEY_ACTION_TABLE, null, v);
        Log.i(TAG, "Users inserted,: " + f.getId_survey_action() + " id: " + id);
        return id;
    }
    public List<SurveyActionDTO> getList() throws SQLException {

        Cursor cur = fetchUserList();
        ArrayList<SurveyActionDTO> list = getList(cur);
        Log.i(TAG, "users found: " + list.size());
        return list;
    }
    public List<SurveyActionDTO> getListSurveyActionID(int userID) throws SQLException {

        Cursor cur = fetchUserID(userID);
        ArrayList<SurveyActionDTO> list = getList(cur);
        Log.i(TAG, "Users found: " + list.size());
        return list;
    }

    private ArrayList<SurveyActionDTO> getList(Cursor cur) {
        ArrayList<SurveyActionDTO> list = new ArrayList<SurveyActionDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                SurveyActionDTO c = getForms(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }
    private Cursor fetchUserList() throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.SURVEY_ACTION_TABLE, cols,
                null, null, null,
                null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "classes: " + count);
        return mCursor;
    }
    public int updateUserDTO(SurveyActionDTO c) throws SQLException {
        int updated = db.update(FD.SURVEY_ACTION_TABLE, createContentValues(c),
                FD.USER_ID + "= " + c.getId_user_fk(), null);
        Log.i(TAG, "Survey: "  + updated);

        return updated;
    }

        public int deleteSurveyActions(int userID) throws SQLException {
            int deleted = db.delete(FD.SURVEY_ACTION_TABLE, FD.USER_ID + "="
                    + userID, null);
            Log.i(TAG, "form Count deleted, count: "
                    + deleted);
            return deleted;
        }



    private SurveyActionDTO getForms(Cursor cur) {
        SurveyActionDTO a = new SurveyActionDTO();
        a.setId_user_fk(cur.getInt(cur.getColumnIndex(FD.USER_ID)));
        a.setId_form_fk(cur.getInt(cur.getColumnIndex(FD.FORM_ID)));
        a.setStart_city(cur.getString(cur.getColumnIndex(FD.SURVEY_ACTION_START_CITY)));
        a.setEnd_city(cur.getString(cur.getColumnIndex(FD.SURVEY_ACTION_END_CITY)));
        a.setStart_date_time(cur.getString(cur.getColumnIndex(FD.SURVEY_ACTION_START_DATE_TIME)));
        a.setEnd_date_time(cur.getString(cur.getColumnIndex(FD.SURVEY_ACTION_END_DATE_TIME)));
        a.setStart_latitude(cur.getString(cur.getColumnIndex(FD.SURVEY_ACTION_START_LAT)));
        a.setStart_longitude(cur.getString(cur.getColumnIndex(FD.SURVEY_ACTION_START_LONG)));
        a.setEnd_latitude(cur.getString(cur.getColumnIndex(FD.SURVEY_ACTION_END_LAT)));
        a.setEnd_longitude(cur.getString(cur.getColumnIndex(FD.SURVEY_ACTION_END_LONG)));

        return a;
    }

    private Cursor fetchUserID(int userID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.SURVEY_ACTION_TABLE, cols, FD.USER_ID
                + " = " + userID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "Users: " + count);
        return mCursor;
    }

    private ContentValues createContentValues(SurveyActionDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.USER_ID, a.getId_user_fk());
        v.put(FD.FORM_ID, a.getId_form_fk());
        v.put(FD.SURVEY_ACTION_START_CITY, a.getStart_city());
        v.put(FD.SURVEY_ACTION_END_CITY, a.getEnd_city());
        v.put(FD.SURVEY_ACTION_START_LAT, a.getStart_latitude());
        v.put(FD.SURVEY_ACTION_START_LONG, a.getStart_longitude());
        v.put(FD.SURVEY_ACTION_END_LAT, a.getEnd_latitude());
        v.put(FD.SURVEY_ACTION_END_LONG, a.getEnd_longitude());
        v.put(FD.SURVEY_ACTION_START_DATE_TIME, a.getStart_date_time());
        v.put(FD.SURVEY_ACTION_END_DATE_TIME, a.getEnd_date_time());
       ;


        return v;
    }
}
