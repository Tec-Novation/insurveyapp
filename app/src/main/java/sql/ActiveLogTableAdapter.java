package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.ActivityLogDTO;

/**
 * Created by iFluid-Dev on 2015-02-19.
 */
public class ActiveLogTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "ActiveLogTableAdapter";

    public ActiveLogTableAdapter(Context context) {
        this.context = context;
    }

    public ActiveLogTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "ActiveLogTableAdapter DB table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG, "ActiveLogTableAdapter DB table closed");
    }

    public long insertActivityLog(ActivityLogDTO c)
            throws SQLException {

        ContentValues v = createContentValues(c);
        long id = db.insert(FD.ACTIVITY_LOG_TABLE, null, v);
        Log.i(TAG, "avtivity log inserted,: " + c.getUserID()+ " id: " + id);
        return id;
    }
    public int deleteActivityLogs() throws SQLException {
        int deleted = db.delete(FD.ACTIVITY_LOG_TABLE, null, null);
        Log.i(TAG, "activity logs deleted, count: "
                + deleted);
        return deleted;
    }
    public int deleteActivityLogs(int userID) throws SQLException {
        int deleted = db.delete(FD.ACTIVITY_LOG_TABLE, FD.USER_ID + "="
                + userID, null);
        Log.i(TAG, "form Count deleted, count: "
                + deleted);
        return deleted;
    }

    /*public boolean recordExists(ProvinceDTO c) {
		String[] cols = { FD.CLASS_YEAR };
		Cursor mCursor = db.query(true, FD.CLASS_TABLE, cols,
				FD.CLASS_NAME + "= '" + c.getClassName() + "' and " +
				FD.CLASS_YEAR + "=" + c.getClassYear(),
				null, null, null, null, null);
		int count = mCursor.getCount();
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}
	public int deleteProvinceDTO(long classKeyID) throws SQLException {
		int deleted = db.delete(FD.CLASS_TABLE, FD.KEY_ID + "="
				+ classKeyID, null);
		Log.i(TAG, "ProvinceDTO deleted, count: "
				+ deleted);
		return deleted;
	}
	public int updateProvinceDTO(ProvinceDTO c) throws SQLException {
		int updated = db.update(FD.CLASS_TABLE, createContentValues(c),
				FD.KEY_ID + "= " + c.getClassKeyID(), null);
		Log.i(TAG, "ProvinceDTO updated, count: "
				+ updated);
		return updated;
	}
	*/
    public List<ActivityLogDTO> getActivityLogList(int userID) throws SQLException {

        Cursor cur = fetchActivityLogByuserID(userID);
        ArrayList<ActivityLogDTO> list = getList(cur);
        Log.i(TAG, "Forms found: " + list.size());
        return list;
    }
    public List<ActivityLogDTO> getList() throws SQLException {

        Cursor cur = fetchProvinceList();
        ArrayList<ActivityLogDTO> list = getList(cur);
        Log.i(TAG, "avtivity log found: " + list.size());
        return list;
    }
    private Cursor fetchActivityLogByuserID(int userID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.ACTIVITY_LOG_TABLE, cols, FD.USER_ID
                + " = " + userID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "forms: " + count);
        return mCursor;
    }

    private ArrayList<ActivityLogDTO> getList(Cursor cur) {
        ArrayList<ActivityLogDTO> list = new ArrayList<ActivityLogDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                ActivityLogDTO c = getProvince(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }
    private ActivityLogDTO getProvince(Cursor cur) {
        ActivityLogDTO a = new ActivityLogDTO();
        a.setUserID(cur.getInt(cur.getColumnIndex(FD.USER_ID)));
        a.setActivityID(cur.getInt(cur.getColumnIndex(FD.ACTIVITY_ID)));
        a.setFormID(cur.getInt(cur.getColumnIndex(FD.FORM_ID)));

        return a;
    }

    private Cursor fetchProvinceList() throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.ACTIVITY_LOG_TABLE, cols,
                null, null, null,
                null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "classes: " + count);
        return mCursor;
    }

    private ContentValues createContentValues(ActivityLogDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.USER_ID, a.getUserID());
        v.put(FD.ACTIVITY_ID, a.getActivityID());
        v.put(FD.FORM_ID, a.getFormID());
        return v;
    }

}