package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/26/2015.
 */
public class SectionTable {
    private static final String DATABASE_CREATE = "create table " + FD.SECTION_TABLE
            + " (" + FD.SECTION_ID + " integer, "
            + FD.FORM_ID + " integer, "
            + FD.SECTION_NAME + " text, "
            + FD.SECTION_JSON_CODE + " text  );";

    private static final String CREATE_INDEX = "create index indexSection on "
            + FD.SECTION_TABLE + " ( " + FD.FORM_ID + ")";
    public static void onCreate(SQLiteDatabase database) {
        Log.i("SectionTable", "############# --> onCreate SectionTable\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);

        Log.d("SectionTable", "--> Done creating indexes on SectionTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.SECTION_TABLE);
        onCreate(database);
    }
}
