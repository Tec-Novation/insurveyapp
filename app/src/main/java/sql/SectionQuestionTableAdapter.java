package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.SectionQuestionDTO;

/**
 * Created by Vic on 1/29/2015.
 */
public class SectionQuestionTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "SectionQuestionTableAdapter";

    public SectionQuestionTableAdapter(Context context) {
        this.context = context;
    }

    public SectionQuestionTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        Log.d(TAG, "SectionQuestionTableAdapter table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.w(TAG,"SecQuesTabAdapter table closed");
    }

    public long insertSectionsQuestions(SectionQuestionDTO f) throws SQLException {

        ContentValues v = createContentValues(f);
        long id = db.insert(FD.SECTION_QUESTION_TABLE, null, v);
        Log.i(TAG, "SectionsqUESTION inserted,: " + f.getName() + " id: " + id);
        return id;
    }

    public List<SectionQuestionDTO> getListSectionQuestionFormID(int formID) throws SQLException {

        Cursor cur = fetchFormByFormID(formID);
        ArrayList<SectionQuestionDTO> list = getList(cur);
        Log.i(TAG, "SectionsqUESTION found: " + list.size());
        return list;
    }

    private ArrayList<SectionQuestionDTO> getList(Cursor cur) {
        ArrayList<SectionQuestionDTO> list = new ArrayList<SectionQuestionDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                SectionQuestionDTO c = getForms(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }

    private SectionQuestionDTO getForms(Cursor cur) {
        SectionQuestionDTO a = new SectionQuestionDTO();
        a.setSectionID(cur.getInt(cur.getColumnIndex(FD.SECTION_ID)));
        a.setName(cur.getString(cur.getColumnIndex(FD.SECTION_NAME)));
        a.setFormID(cur.getInt(cur.getColumnIndex(FD.FORM_ID)));
        a.setJasonCode(cur.getString(cur.getColumnIndex(FD.SECTION_JSON_CODE)));
        a.setQuestionID(cur.getInt(cur.getColumnIndex(FD.QUESTION_ID)));
        a.setQuestion(cur.getString(cur.getColumnIndex(FD.QUESTION)));
        a.setDescription(cur.getString(cur.getColumnIndex(FD.SECTION_DESCRIPTION)));
        return a;
    }

    private Cursor fetchFormByFormID(int formID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.SECTION_QUESTION_TABLE, cols, FD.FORM_ID
                + " = " + formID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.d(TAG, "section: " + count);
        return mCursor;
    }

    private ContentValues createContentValues(SectionQuestionDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.SECTION_ID, a.getSectionID());
        v.put(FD.SECTION_NAME, a.getName());
        v.put(FD.FORM_ID, a.getFormID());
        v.put(FD.SECTION_JSON_CODE, a.getJasonCode());
        v.put(FD.QUESTION_ID, a.getQuestionID());
        v.put(FD.QUESTION, a.getQuestion());
        v.put(FD.SECTION_DESCRIPTION, a.getDescription());

        return v;
    }
}
