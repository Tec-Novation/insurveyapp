package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Charmaine on 2015/05/09.
 */
public class DemorgraphicTable {

    private static final String DATABASE_CREATE = "create table " + FD.DERMOGRAPHIC_DATA_TABLE
            + " (" + FD.DERMOGRAPHIC_AGE + " text, "
            + FD.DERMOGRAPHIC_GENDER + " text, "
            + FD.DERMOGRAPHIC_MARITAL + " text, "
            + FD.DERMOGRAPHIC_EMPLOYMENT + " text, "
            + FD.DERMOGRAPHIC_RACE + " text, "
            + FD.DERMOGRAPHIC_PERSONAL_DATA + " text, "
            + FD.DERMOGRAPHIC_INCOME + " text, "
            + FD.DERMOGRAPHIC_NUMBER_HOUSEHOLD + " text, "
            + FD.DERMOGRAPHIC_MUNICIPALITY + " text, "
            + FD.FORM_ID + " integer );";

    private static final String CREATE_INDEX = "create index indexdemo on "
            + FD.DERMOGRAPHIC_DATA_TABLE + " ( " + FD.FORM_ID + ")";

    public static void onCreate(SQLiteDatabase database) {
        Log.i("DemorgraphicTable", "############# --> onCreate DemorgraphicTable\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);

        Log.i("DemorgraphicTable", "--> Done creating indexes on DemorgraphicTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.i(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.DERMOGRAPHIC_DATA_TABLE);
        onCreate(database);
    }
}
