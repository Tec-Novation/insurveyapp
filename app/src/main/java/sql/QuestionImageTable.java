package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/26/2015.
 */
public class QuestionImageTable {
    private static final String DATABASE_CREATE = "create table " + FD.QUESTION_IMAGE_TABLE
            + " (" + FD.ID_QUESTION_IMAGE + " integer, "
            + FD.QUESTION_IMAGE_LOCAION + " text, "
            + FD.FORM_ID + " integer, "
            + FD.QUESTION_ID + " integer  );";

    private static final String CREATE_INDEX = "create index indexQuestionImage on "
            + FD.QUESTION_IMAGE_TABLE + " ( " + FD.QUESTION_ID + ")";

    public static void onCreate(SQLiteDatabase database) {
        Log.i("QuestionImageTable ", "############# --> onCreate QuestionImageTable\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);

        Log.d("QuestionImageTable ", "--> Done creating indexes on QuestionImageTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(QuestionImageTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.QUESTION_IMAGE_TABLE);
        onCreate(database);
    }
}
