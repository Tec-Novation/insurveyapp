package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/26/2015.
 */
public class PersonTable {
    private static final String DATABASE_CREATE = "create table " + FD.PERSON_TABLE
            + " (" + FD.PERSON_ID + " INTEGER PRIMARY KEY, "
            + FD.USER_ID + " integer, "
            + FD.FORM_ID + " integer, "
            + FD.PERSON_NAME + " text, "
            + FD.PERSON_LAST_NAME + " text, "
            + FD.PERSON_PHONE + " text, "
            + FD.PERSON_EMAIL + " text, "
            + FD.PERSON_AGE + " text, "
            + FD.PERSON_RACE + " text, "
            + FD.PERSON_GENDER + " text, "
            + FD.PERSON_MARITAL + " text, "
            + FD.PERSON_EMPLOYMENT + " text, "
            + FD.SMS_RECIEVE + " integer, "
            + FD.UID + " text, "
            + FD.PERSON_DATE_OF_BIRTH + " text, "
            + FD.PERSON_INCOME + " text, "
            + FD.ADDRESS + " text, "
            + FD.PERSON_NO_OCCUPANTS + " text, "
            + FD.PERSON_COMPANY_PHONE + " text, "
            + FD.DERMOGRAPHIC_NUMBER_HOUSEHOLD + " text, "
            + FD.DERMOGRAPHIC_MUNICIPALITY + " text, "
            + FD.SERVICE_PROVIDER_ID + " integer );";

    private static final String CREATE_INDEX = "create index indexPerson on "
            + FD.PERSON_TABLE + " ( " + FD.SERVICE_PROVIDER_ID + ")";
    public static void onCreate(SQLiteDatabase database) {
        Log.i("PersonTable", "############# --> onCreate PersonTable\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);

        Log.d("PersonTable", "--> Done creating indexes on PersonTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.PERSON_TABLE);
        onCreate(database);
    }
}
