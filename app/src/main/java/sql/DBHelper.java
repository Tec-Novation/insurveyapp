package sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper instance;
    private static final int DATABASE_VERSION = 101;

    public DBHelper(Context context) {
        super(context, FD.DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DBHelper getHelper(Context context) {
        if (instance == null)
            instance = new DBHelper(context);
        return instance;
    }
    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
        ActiveLogTable.onCreate(database);
        ActivityTable.onCreate(database);
        AnswerTable.onCreate(database);
        FormTable.onCreate(database);
        FormStatusTable.onCreate(database);
        PersonTable.onCreate(database);
        QuestionTable.onCreate(database);
        SectionTable.onCreate(database);
        ServiceProviderTable.onCreate(database);
        SectionQuwstionTable.onCreate(database);
        UserTable.onCreate(database);
        FormCountTable.onCreate(database);
        SaveAnswerTable.onCreate(database);
        SurveyActionTable.onCreate(database);
        DemorgraphicTable.onCreate(database);
        ContactTable.onCreate(database);
        QuestionImageTable.onCreate(database);



        Log.i("DBHelper", "--> In-Survey tables onCreate complete");
    }
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
                          int newVersion) {
         ActiveLogTable.onUpgrade(database,oldVersion,newVersion);
         ActivityTable.onUpgrade(database,oldVersion,newVersion);
         AnswerTable.onUpgrade(database,oldVersion,newVersion);
         FormTable.onUpgrade(database,oldVersion,newVersion);
         FormStatusTable.onUpgrade(database,oldVersion,newVersion);
         PersonTable.onUpgrade(database,oldVersion,newVersion);
         QuestionTable.onUpgrade(database,oldVersion,newVersion);
         SectionTable.onUpgrade(database,oldVersion,newVersion);
         ServiceProviderTable.onUpgrade(database,oldVersion,newVersion);
         SectionQuwstionTable.onUpgrade(database,oldVersion,newVersion);
         UserTable.onUpgrade(database,oldVersion,newVersion);
         FormCountTable.onUpgrade(database,oldVersion,newVersion);
         SaveAnswerTable.onUpgrade(database,oldVersion,newVersion);
         SurveyActionTable.onUpgrade(database,oldVersion,newVersion);
         DemorgraphicTable.onUpgrade(database,oldVersion,newVersion);
         ContactTable.onUpgrade(database,oldVersion,newVersion);
         QuestionImageTable.onUpgrade(database,oldVersion,newVersion);

        Log.i("DBHelper", "--> In-Survey tables onUpgrade complete");

    }
}

	// Method is called during an upgrade of the database,