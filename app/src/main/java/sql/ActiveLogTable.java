package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/26/2015.
 */
public class ActiveLogTable {
    private static final String DATABASE_CREATE = "create table " + FD.ACTIVITY_LOG_TABLE
            + " (" + FD.ACTIVITY_LOG_ID + " INTEGER PRIMARY KEY, "
            + FD.ACTIVITY_ID + " integer, "
            + FD.USER_ID + " integer, "
            + FD.FORM_ID + " integer  );";

    private static final String CREATE_INDEX = "create index indexActiveLog on "
            + FD.ACTIVITY_LOG_TABLE + " ( " + FD.ACTIVITY_ID + ")";
    private static final String CREATE_INDEX_1 = "create index indexActiveLog1 on "
            + FD.ACTIVITY_LOG_TABLE + " ( " + FD.USER_ID + ")";
    private static final String CREATE_INDEX_2 = "create index indexActiveLog2 on "
            + FD.ACTIVITY_LOG_TABLE + " ( " + FD.FORM_ID + ")";
    public static void onCreate(SQLiteDatabase database) {
        Log.i("ActiveLogTable", "############# --> onCreate ActiveLogTable\n");
        database.execSQL(DATABASE_CREATE);
        database.execSQL(CREATE_INDEX);
        database.execSQL(CREATE_INDEX_1);
        database.execSQL(CREATE_INDEX_2);

        Log.d("ActiveLogTable", "--> Done creating indexes on ActiveLogTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.ACTIVITY_LOG_TABLE);
        onCreate(database);
    }
}
