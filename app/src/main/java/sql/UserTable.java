package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by iFluid-Dev on 2015-02-05.
 */
public class UserTable {
        private static final String DATABASE_CREATE = "create table " + FD.USER_TABLE
                + " (" + FD.USER_ID + " integer, "
                + FD.USER_NAME + " text, "
                + FD.USER_USERNAME + " text, "
                + FD.USER_LASTNAME + " text, "
                + FD.USER_PASSWORD + " text, "
                + FD.USER_PHONE + " text, "
                + FD.USER_EMAIL + " text, "
                + FD.USER_PHYSICALADDRESS + " text, "
                + FD.USER_POSTALADDRESS + " text, "
                + FD.USER_PERMISSIONID + " integer, "
                + FD.USER_IMAGE + " blob, "
                + FD.USER_COMPANYID + " integer  );";

        private static final String CREATE_INDEX = "create index indexUser on "
                + FD.USER_TABLE + " ( " + FD.USER_PERMISSIONID + ")";
        private static final String CREATE_INDEX_1 = "create index indexUser1 on "
                + FD.USER_TABLE + " ( " + FD.USER_COMPANYID + ")";
        public static void onCreate(SQLiteDatabase database) {
            Log.i("UserTable", "############# --> onCreate UserTable\n");
            database.execSQL(DATABASE_CREATE);
            database.execSQL(CREATE_INDEX);
            database.execSQL(CREATE_INDEX_1);

            Log.d("UserTable", "--> Done creating indexes on UserTable");
        }
        public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                     int newVersion) {
            Log.w(FormTable.class.getName(), "Upgrading database from version "
                    + oldVersion + " to " + newVersion
                    + ", which will destroy all old data");
            database.execSQL("DROP TABLE IF EXISTS " + FD.USER_TABLE);
            onCreate(database);
        }
    }


