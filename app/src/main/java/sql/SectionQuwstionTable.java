package sql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vic on 1/29/2015.
 */
public class SectionQuwstionTable {
    private static final String DATABASE_CREATE = "create table " + FD.SECTION_QUESTION_TABLE
            + " (" + FD.SECTION_ID + " integer, "
            + FD.FORM_ID + " integer, "
            + FD.QUESTION_ID + " integer, "
            + FD.SECTION_NAME + " text, "
            + FD.QUESTION + " text, "
            + FD.SECTION_DESCRIPTION + " text, "
            + FD.SECTION_JSON_CODE + " text  );";

    public static void onCreate(SQLiteDatabase database) {
        Log.i("SectionQuwstionTable", "############# --> onCreate SectionQuwstionTable\n");
        database.execSQL(DATABASE_CREATE);

        Log.d("SectionQuwstionTable", "--> Done creating indexes on SectionQuwstionTable");
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(FormTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FD.SECTION_QUESTION_TABLE);
        onCreate(database);
    }
}
