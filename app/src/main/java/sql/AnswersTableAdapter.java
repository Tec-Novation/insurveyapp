package sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dto.AnswersDTO;

/**
 * Created by Vic on 2/1/2015.
 */
public class AnswersTableAdapter {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private static final String TAG = "AnswersTableAdapter";

    public AnswersTableAdapter(Context context) {
        this.context = context;
    }

    public AnswersTableAdapter open() throws SQLException {
        dbHelper = DBHelper.getHelper(context);
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        Log.i(TAG, "AnswersTableAdapter table opened");
        return this;
    }

    public void close() {
        db.close();
        dbHelper.close();
        Log.i(TAG, "AnswersTableAdapter table closed");
    }

    public long insertAnswers(AnswersDTO f) throws SQLException {

        ContentValues v = createContentValues(f);
        long id = db.insert(FD.ANSWER_TABLE, null, v);
        Log.i(TAG, "Answers inserted,: " + f.getAnswer() + " id: " + id);
        return id;
    }
    public List<AnswersDTO> getAnswerList(int answerID) throws SQLException {

        Cursor cur = fetchFormByFormID(answerID);
        ArrayList<AnswersDTO> list = getList(cur);
        Log.i(TAG, "Forms found: " + list.size());
        return list;
    }

    private ArrayList<AnswersDTO> getList(Cursor cur) {
        ArrayList<AnswersDTO> list = new ArrayList<AnswersDTO>();
        try {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                AnswersDTO c = getAnswers(cur);
                list.add(c);
                cur.moveToNext();
            }
        } finally {
            cur.close();
        }
        return list;
    }
    public List<AnswersDTO> getList() throws SQLException {

        Cursor cur = fetchAnswersList();
        ArrayList<AnswersDTO> list = getList(cur);
        Log.i(TAG, "answers found: " + list.size());
        return list;
    }
    private AnswersDTO getAnswers(Cursor cur) {
        AnswersDTO a = new AnswersDTO();
        a.setAnswerID(cur.getInt(cur.getColumnIndex(FD.ANSWER_ID)));
        a.setAnswer(cur.getString(cur.getColumnIndex(FD.ANSWER)));
        a.setQuestionID(cur.getInt(cur.getColumnIndex(FD.QUESTION_ID)));
        a.setActivityLogID(cur.getInt(cur.getColumnIndex(FD.ACTIVITY_LOG_ID)));
        a.setPersonID(cur.getInt(cur.getColumnIndex(FD.PERSON_ID)));

        return a;
    }

    private Cursor fetchFormByFormID(int answerID) throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.ANSWER_TABLE, cols, FD.ANSWER_ID
                + " = " + answerID, null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.i(TAG, "Answers: " + count);
        return mCursor;
    }
    private Cursor fetchAnswersList() throws SQLException {
        String[] cols = { "*" };
        Cursor mCursor = db.query(true, FD.ANSWER_TABLE, cols,
                null, null, null,
                null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        Log.i(TAG, "classes: " + count);
        return mCursor;
    }
    private ContentValues createContentValues(AnswersDTO a) {
        ContentValues v = new ContentValues();
        v.put(FD.ANSWER_ID, a.getAnswerID());
        v.put(FD.ANSWER, a.getAnswer());
        v.put(FD.QUESTION_ID, a.getQuestionID());
        v.put(FD.ACTIVITY_LOG_ID, a.getActivityLogID());
        v.put(FD.PERSON_ID, a.getPersonID());

        return v;
    }
}
