package adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.surveyor.insurvey.in_surveyapp.R;

import java.util.List;

import dto.FormDTO;
import sql.SectionQuestionTableAdapter;

/**
 * Created by Vic on 1/29/2015.
 */
public class SurveyListdapterSQLLiteOne extends ArrayAdapter<FormDTO> {
    private SectionQuestionTableAdapter sqta;
    private LayoutInflater mInflate;
    private final int mResource;
    private Context context;
    private ProgressDialog dialog;
    private List<FormDTO> list;
    private int lastPosition = -1;
    private SharedPreferences sp;
    public SurveyListdapterSQLLiteOne(Context context, int textViewResourceId,
                                      List<FormDTO> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        mResource = textViewResourceId;
        this.list = list;
        mInflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Holder holder;
        if (view == null) {
            view = mInflate.inflate(mResource, parent, false);
            holder = new Holder();
            holder.TV_descript = (TextView) view.findViewById(R.id.tv_desc);
            holder.TV_dateCreated = (TextView) view.findViewById(R.id.tv_created);
            holder.TV_dateUpdated = (TextView)view.findViewById(R.id.tv_updated);
            holder.TV_name = (TextView)view.findViewById(R.id.tv_names1);
            holder.imgGo = (ImageView)view.findViewById(R.id.img_do);
            view.setTag(holder);
        } else {

            holder = (Holder) view.getTag();
        }

        final FormDTO dto = list.get(position);

        holder.TV_descript.setText(dto.getDescription());
        holder.TV_dateCreated.setText("Created :"+ dto.getCreateDate());
        holder.TV_dateUpdated.setText("Last Update :"+dto.getLastUpdated());
        holder.TV_name.setText(dto.getName());
        holder.imgGo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(200);
                sp = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor ed = sp.edit();
                int formID = dto.getFormID();
                ed.putInt("formID1",formID);
                ed.commit();
               // Intent intent = new Intent(context, RegisterPersonActivity.class)
                    //    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               // context.startActivity(intent);
//                ((Activity)context).finish();

            }
        });
        Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        view.startAnimation(animation);
        lastPosition = position;
        return view;
    }

    static class Holder {
        TextView TV_descript;
        TextView TV_dateCreated;
        TextView TV_dateUpdated;
        TextView TV_name;
        ImageView imgGo;
    }

}
