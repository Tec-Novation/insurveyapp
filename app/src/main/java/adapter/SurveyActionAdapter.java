package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.surveyor.insurvey.in_surveyapp.R;

import java.util.List;

import dto.SurveyActionDTO;

/**
 * Created by Charmaine on 2015/04/21.
 */
public class SurveyActionAdapter extends ArrayAdapter<SurveyActionDTO> {

    private LayoutInflater mInflate;
    private final int mResource;
    private Context context;
    private List<SurveyActionDTO> list;
    private int lastPosition = -1;


    public SurveyActionAdapter(Context context, int textViewResourceId,
                                      List<SurveyActionDTO> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        mResource = textViewResourceId;
        this.list = list;
        mInflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Holder holder;
        if (view == null) {
            view = mInflate.inflate(mResource, parent, false);
            holder = new Holder();
            holder.tv_surveyname = (TextView) view.findViewById(R.id.tv_survey_name);
            holder.tv_start_date = (TextView) view.findViewById(R.id.tv_start_date);
            holder.tv_end_date = (TextView)view.findViewById(R.id.tv_endDate);
            holder.tv_start_city = (TextView)view.findViewById(R.id.tv_start_city);
            holder.tv_end_city = (TextView)view.findViewById(R.id.tv_end_city);
            view.setTag(holder);
        } else {

            holder = (Holder) view.getTag();
        }

        final SurveyActionDTO dto = list.get(position);

        holder.tv_surveyname.setText(dto.getSurvey_name());
        holder.tv_start_date.setText(dto.getStart_date_time());
        holder.tv_end_date.setText(dto.getEnd_date_time());
        if(!dto.getStart_city().equals(null) || !dto.getStart_city().equals("")){
            holder.tv_start_city.setText(dto.getStart_city()+": ");
        }else{
            holder.tv_start_city.setText("Not Available");
        }
        if(!dto.getEnd_city().equals(null) || !dto.getEnd_city().equals(null)){
            holder.tv_end_city.setText(dto.getEnd_city()+": ");
        }else{
            holder.tv_end_city.setText("Not Available");
        }

        Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        view.startAnimation(animation);
        lastPosition = position;
        return view;
    }

    static class Holder {
        TextView tv_surveyname;
        TextView tv_start_date;
        TextView tv_end_date;
        TextView tv_start_city;
        TextView tv_end_city;
    }
}
