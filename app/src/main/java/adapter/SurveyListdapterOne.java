package adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.surveyor.insurvey.in_surveyapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import dto.FormDTO;
import dto.SectionQuestionDTO;
import sql.FormTableAdapter;
import sql.SectionQuestionTableAdapter;

/**
 * Created by Vic on 1/29/2015.
 */
public class SurveyListdapterOne extends ArrayAdapter<FormDTO> {

    private LayoutInflater mInflate;
    private final int mResource;
    private Context context;
    private List<FormDTO> list;
    private SharedPreferences sp;
    private FormTableAdapter fta;
    private SectionQuestionTableAdapter sqta;
    public SurveyListdapterOne(Context context, int textViewResourceId,
                            List<FormDTO> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        mResource = textViewResourceId;
        this.list = list;
        mInflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Holder holder;
        if (view == null) {
            view = mInflate.inflate(mResource, parent, false);
            holder = new Holder();
            holder.TV_descript = (TextView) view.findViewById(R.id.tv_desc);
            holder.TV_dateCreated = (TextView) view.findViewById(R.id.tv_created);
            holder.TV_dateUpdated = (TextView)view.findViewById(R.id.tv_updated);
            holder.TV_name = (TextView)view.findViewById(R.id.tv_names1);
            holder.imgGo = (ImageView)view.findViewById(R.id.img_do);

            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }

        final FormDTO dto = list.get(position);

        holder.TV_descript.setText(dto.getDescription());
        holder.TV_dateCreated.setText("Date Created :"+ dto.getCreateDate());
        holder.TV_dateUpdated.setText("Last Updated Date :"+dto.getLastUpdated());
        holder.TV_name.setText(dto.getName());
        holder.imgGo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(200);
                fta = new FormTableAdapter(getContext());
                fta.open();
                fta.insertForm(dto);
                fta.close();
                getVolley(dto.getFormID());
                Toast.makeText(getContext(), dto.getName()+ " downloaded", Toast.LENGTH_SHORT).show();
               // Intent intent = new Intent(context,list_surveys_activity.class)
                      //  .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               // context.startActivity(intent);
//                ((Activity)context).finish();

            }
        });
        return view;
    }

    static class Holder {
        TextView TV_descript;
        TextView TV_dateCreated;
        TextView TV_dateUpdated;
        TextView TV_name;
        ImageView imgGo;
    }
    private void volySecQues(String arg0) {
        sqta = new SectionQuestionTableAdapter(context);
        try {
            JSONObject obj = new JSONObject(arg0);
            if (obj.getString("success").equals("0")) {

                JSONArray array = obj.getJSONArray("questions");
                JSONObject jso = new JSONObject();
                for (int i = 0; i < array.length(); ++i) {
                    jso = array.getJSONObject(i);

                    SectionQuestionDTO dto = new SectionQuestionDTO(jso.getInt("id_section"),
                            jso.getInt("id_form_fk"),
                            jso.getInt("id_question"), jso.getString("name"),
                            jso.getString("json"), jso.getString("question"),jso.getString("description"));

                    Log.i("TAG", " " + dto.getName());

                    //sectionQuestionDTOs.add(dto);

                    insertSectionQuestionsToSqlLite(dto);
                     //dialog.dismiss();
                }
            } else {
                // toast(obj.getString("message"));
                 //dialog.dismiss();
                Log.i("TAG", obj.getString("message"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private void insertSectionQuestionsToSqlLite(SectionQuestionDTO s) {
        //FormDTO f  = new FormDTO();
        sqta.open();
        sqta.insertSectionsQuestions(s);
        sqta.close();
    }
    private void getVolley(int formID) {
        RequestQueue queue = Volley
                .newRequestQueue(context);
         //dialog();
        String urlSecQues = "http://www.insurvey.co.za/api?requestType=4&formID=" + formID;
        //String urlSecQues = "http://10.0.2.2/insurvey/index.php?requestType=4&formID="+formID;
        StringRequest requestSecQues = new StringRequest(urlSecQues,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String arg0) {
                        //Log.i(LOG, arg0.toString());
                        volySecQues(arg0);
                         //dialog.dismiss();
                        // sla.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(
                    VolleyError arg0) {
                // TODO Auto-generated method stub
                // dialog.dismiss();
                // toast("Network Connection Error");
                //toast("Check your network connection");
                // Log.i(LOG, arg0.toString());
            }
        });
        queue.add(requestSecQues);
    }
    private void dialog() {
        dialog = new ProgressDialog(context,
                AlertDialog.THEME_HOLO_LIGHT);
        dialog.setMessage("Please wait");
        dialog.setMessage("Loading...");
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }
    private ProgressDialog dialog;
}
