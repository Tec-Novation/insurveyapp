package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.surveyor.insurvey.in_surveyapp.R;

import java.util.List;

import dto.FormCountDTO;

/**
 * Created by Charmaine on 2015/04/21.
 */
public class SurveyFormCountAdapter extends ArrayAdapter<FormCountDTO> {

    private LayoutInflater mInflate;
    private final int mResource;
    private Context context;
    private List<FormCountDTO> list;
    private int lastPosition = -1;


    public SurveyFormCountAdapter(Context context, int textViewResourceId,
                                  List<FormCountDTO> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        mResource = textViewResourceId;
        this.list = list;
        mInflate = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Holder holder;
        if (view == null) {
            view = mInflate.inflate(mResource, parent, false);
            holder = new Holder();
            holder.tv_surveyname = (TextView) view.findViewById(R.id.tv_name);
            holder.tv_date = (TextView) view.findViewById(R.id.tv_date);

            view.setTag(holder);
        } else {

            holder = (Holder) view.getTag();
        }

        final FormCountDTO dto = list.get(position);

        holder.tv_surveyname.setText(dto.getDescription());
        holder.tv_date.setText(dto.getDate());


        Animation animation = AnimationUtils.loadAnimation(getContext(), (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        view.startAnimation(animation);
        lastPosition = position;
        return view;
    }

    static class Holder {
        TextView tv_surveyname;
        TextView tv_date;

    }
}
