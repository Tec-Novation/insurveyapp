<?php session_start();
    include_once('./includes/Database.php'); 
    include_once('./includes/User.php'); 
	include_once('./includes/Report.php'); 
	include_once('./includes/SurveyReport.php');
	include_once('./includes/GenerateReport.php');
	include_once('./static/enum_dermograph.php');
	
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title id="c"></title>
 
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
         <link href="css/bootstrap-table.css" rel="stylesheet" type="text/css">
		 <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="dashboard.php">Insurvey Admin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown" style="display:none;">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown" style="display:none;">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown" style="display:none;">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="./includes/ajax_logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
					    <div class="input-group custom-search-form">
							<div class="Wrapper">
							    <input type="image" src="./images/line.png" name="saveForm" class="btTxt submit" id="line"  height="30" width="33"/>
								<input type="image" src="./images/bar.png" name="saveForm" class="btTxt submit" id="bar"  height="30" width="33"/>
								<input type="image" src="./images/pie.png" name="saveForm" class="btTxt submit" id="pie"  height="30" width="33"/>
								<input type="image" src="./images/area.png" name="saveForm" class="btTxt submit" id="area"  height="30" width="33"/>
								 <input type="image" src="./images/area_spline.png" name="saveForm" class="btTxt submit" id="area_spline"  height="30" width="33"/>
								<input type="image" src="./images/spline.png" name="spline" class="btTxt submit" id="spline"  height="30" width="33"/>
								<input type="image" src="./images/coloumn.png" name="saveForm" class="btTxt submit" id="coloumn"  height="30" width="33"/>
								<input type="image" src="./images/scatter.png" name="scatter" class="btTxt submit" id="scatter"  height="30" width="33"/>
							</div>
					    </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
						</li>
							<li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Company<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./company-create.php">Create</a>
                                </li>
                                <li>
                                    <a href="./company-list.php">List</a>
                                </li>
                            </ul>
                        </li>    
                        <li>
                            <a href="#"><i class="fa fa-edit fa-fw"></i> Forms<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./forms-create.php">Create</a>
                                </li>
                                <li>
                                    <a href="./forms-list.php">List</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Users<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./user-create.php">Create</a>
                                </li>
                                <li>
                                    <a href="./user-list.php">List</a>
                                </li>
                            </ul>
                        </li>      
						<li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./report_list.php">Report</a>
                                </li>
                            </ul>
                        </li>     							
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Survey Report</h1>

                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
                                <h4 class="panel-title">
                                    <a>
                                        Survey Report
                                    </a>
                                </h4>
                            </div>
                            <div>
                                <div class="row">
                                 <div class="col-lg-12">
                                 <div class="panel-body" >
                                     <label>Client:</label>
                                     <input type="button" id="selected_report" value="Create Report" style="float: right; display: block;margin-right:8px; margin-top:8px;" class="btn btn-primary">
                                     <label id="client"></label><br>
                                     <label>Date :</label>

                                     <label id="date">20 MAR 2015</label><br>
                                     <label>Number Of Respondence :</label>
                                     <label id="total">27</label>

                                 </div>

                                 </div>


                                    </div>
                                </div>
                            </div>
                        </div>
					 </div>
                    <!-- /.col-lg-12 -->
           <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" style="width:900px;">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Generate Report</h4>
                        <div class="clear"></div>
                      </div>
                      <div class="modal-body">
                        <div class="clear"></div>
                           
                            <fieldset>
							<div class="form-group">
							<div class="col-lg-12">
                                <label class="col-lg-2"control-label" for="checkboxes" >Client Name :</label>
                                  <div class="col-lg-8">
                                      <input type="hidden" name="id_report" id="id_report_select">
									   <label id="company">IC Company</label>
								</div>
							</div>
							
							<div class="col-lg-12">
                                <label class="col-lg-2"control-label" for="checkboxes" >Survey Name:</label>
                                  <div class="col-lg-8">
									   <label id="survey"></label>
								</div>
                            </div>
							
							
							<div class="col-lg-12" id="report_conclussion">
                                <label class="col-lg-2"control-label" for="checkboxes" >Update Logo:</label>
                             <div class="col-lg-8">
								<div id="upload-wrapper">
										<div>
										<form action="./processupload.php" method="post" enctype="multipart/form-data" id="MyUploadForm">
										<input name="image_file" id="imageInput" type="file" />
										<input type="hidden" name="id_form" id="id_form" value="<?php echo $_GET['id_form'] ?>">
										<input type="submit"  id="submit-btn" value="Upload" style="float: right; display: block;margin-right:20px; margin-top:-25px;" class="btn btn-primary"/>
										<img  class="thumbnail" src="images/ajax-loader.gif" id="loading-img"  alt="Please Wait" style="float: right; display: none;margin-right:160px; margin-top:-20px;"/>
										</form><br>
										<div id="output" style="float: right; display: block;margin-right:120px; margin-top:-70px;"></div>
										
								</div>
							</div>
						</div>
							<br>
							<hr>
							<div id="rep">
							<form id="generateFinalMainReport">	
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label class="col-lg-2"control-label" for="checkboxes" >Select Report</label>
                                        <div class="col-lg-9">
                                            <select id="report_option" name="report_option" class="form-control">
											<option value="0">---Select Report----</option>
                                                <?php
                                                $formID = $_GET['id_form']; foreach (GenerateReport::getReport($formID) as $key => $value) {
                                                    echo "<option value=\"{$value->getIdReport()}\">{$value->getName()}</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                </div><br>
						
						<div class="form-group" id="report_introduction">
							<div class="col-lg-12">
                                <label class="col-lg-2"control-label" for="checkboxes" >Introduction</label>
                                  <div class="col-lg-9">

									  <textarea id="introduction" name="introduction" required=""  placeholder="Report Introduction" class="form-control" ></textarea>
								</div>
						</div>

						<div class="form-group" id="report_name">
							<div class="col-lg-12"><br>
                                <label class="col-lg-2"control-label" for="checkboxes">Add to report</label>
                                 <div class="col-lg-10" id="found_report">
								</div>
							</div>
							<div class="col-lg-12" id="report_conclussion">
                                <label class="col-lg-2"control-label" for="checkboxes" >Conclusion</label>
                                  <div class="col-lg-9">
									  <textarea id="conclusion" name="conclusion" required=""  placeholder="Conclution" class="form-control" ></textarea>
								</div>

							</div>
						</div>	
                               
                            </fieldset>
                         
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="generateReport" value="generateReport" name="generateReport">Generate Report</button>
                      </div>
					         <input type="hidden" name="id_report" id="id_report">
							 <input type="hidden" name="client_name"  id="client_name">
							 <input type="hidden" name="survey_name" id="survey_name">
							 <input type="hidden" name="client_email"  id="client_phone">
							 <input type="hidden" name="client_phone" id="client_email">
							 <input type="hidden" id="highest_demo" name="highest_demo">
							 <input type="hidden" name="id_form" value="<?php echo $_GET['id_form'] ?>">
					     </form>
						</div>
                    </div>
                  </div>
                </div>
				<div class="modal fade" id="addtoReportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" style="width:900px;">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add to Main Report</h4>
                        <div class="clear"></div>
                      </div>
                      <div class="modal-body">
                        <div class="clear"></div>
                            <form id="addToReportForm">
                            <fieldset>
							<div class="form-group">
							<div class="col-lg-12">
                                <div id="add_report_resulis"></div>
                                <label class="col-lg-2"control-label" for="checkboxes" >Report Name</label>
                                  <div class="col-lg-9">
                                    <input type="hidden" name="id_report" id="id_report_select">
									<input id="rep_name" name="rep_name" type="text" placeholder="Report Name" required=""  class="form-control">
								</div>
							</div><br><br>
							<div class="form-group">
							<div class="col-lg-12">
                                <label class="col-lg-2"control-label" for="checkboxes" >Description</label>
                                  <div class="col-lg-9">
                                      <input type="hidden" name="id_report" id="id_report_select">
									  <textarea id="rep_description" name="rep_description" required=""  placeholder="Description" class="form-control" ></textarea>
								</div>
                                <input type="hidden" name="id_form_fk" value="<?php echo $_GET['id_form'] ?>">
							</div>
						</div>	
                   
                            </fieldset>
                            </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default"id="addtomainReportClose"  data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="addtomainReport">Save</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="selectReportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width:900px;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Add to main report</h4>
                                <div class="clear"></div>
                            </div>
                            <div class="modal-body">
                                <div class="clear"></div>
                                <form id="generateFinalReport">
                                    <fieldset>
                                        <div class="form-group">

                                            <div class="col-lg-12">
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <label class="col-lg-2"control-label" for="checkboxes" >Name</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" id="question_name" name="question_name" required=""  placeholder="Name" class="form-control" ></input>
                                                    </div>
                                                </div><br>
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <label class="col-lg-2"control-label" for="checkboxes" >Description</label>
                                                    <div class="col-lg-9">
                                                        <textarea id="question_description" name="question_description" required=""  placeholder="Description" class="form-control" ></textarea>
                                                    </div>

                                                </div>
                                                <hr>
                                                <div class="form-group">
                                                    <hr>
                                                    <div class="col-lg-12"><hr>
                                                        <div id="select_report_from_list"></div>
                                                        <label class="col-lg-2"control-label" for="checkboxes">Select Report</label>
                                                        <div class="col-lg-9">
                                                            <?php $formID = $_GET['id_form']; foreach (GenerateReport::getReport($formID) as $key => $value){ ?>
                                                                <div class="checkbox">
                                                                    <label for="radio-0">
                                                                        <input type="radio"  id="getReportID" class="gerReportIDClass" name="id[]" value="<?php echo $value->getIdReport(); ?>">
                                                                        <?php echo $value->getName()?>

                                                                    </label>
                                                                </div>
                                                            <?php } ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                    </fieldset>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" id="CloseaddAltoMainReport" name="CloseaddAltoMainReport">Close</button>
                                <button type="button" class="btn btn-primary" id="addAltoMainReport" name="addAltoMainReport">Save</button>
                            </div>
                            <input type="hidden" name="id_form" value="<?php echo $_GET['id_form'] ?>">
                            <input type="hidden" name="client_name"  id="client_name"">
                            <input type="hidden" name="selected_report_id" id="selected_report_id">
                            <input type="hidden" name="selected_chart_path" id="selected_chart_path">

                            </form>
                        </div>
                    </div>
                </div>

               <div class="row">
                <div class="col-lg-12">
				 <div class="col-lg-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Operation
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" id ="cont">
                                   <form id="demographicOptions">
                            <input type="hidden" name="id_form" value="<?php echo $_GET['id_form'] ?>">
							 <div id="demographs">
								<div class="panel-group" id="accordion">
							  <div class="panel panel-default">
								<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
								  <h4 class="panel-title">
									<a>
									  Gender
									</a>
								  </h4>
								</div>
								<div>
								  <div class="panel-body">
								  <input type="checkbox" name="All" value="All"  class="upper-box">All
									<div  id="genderDiv" name="Gender" class="sub-group"></div>
								  </div>
								</div>
							  </div>
							  <div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title"  data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo">
									<a>
									  Age Group
									</a>
								  </h4>
								</div>
								<div>
								  <div class="panel-body">
								  <input type="checkbox" name="All" value="All"  class="upper-box">All
										<div id="age" name="Age Group" class="sub-group"></div>
								  </div>
								</div>
							  </div>
							  <div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree">
									<a>
									  Race
									</a>
								  </h4>
								</div>
								<div>
								  <div class="panel-body">
								   <input type="checkbox" name="All" value="All"  class="upper-box">All
								   <div id="race" name="Race"  class="sub-group"></div>
								  </div>
								</div>
							  </div>
							  <div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" data-target="#collapseFour">
									<a>
									  Marital Status
									</a>
								  </h4>
								</div>
								<div>
								  <div class="panel-body">
									<input type="checkbox" name="All" value="All"  class="upper-box">All
									<div id="marital" name="Marital"  class="sub-group"></div>
								  </div>
								</div>
							  </div>
							  <div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" data-target="#collapseFive">
									<a class="accordion-toggle">
									  Employment Status
									</a>
								  </h4>
								</div>
								<div>
								  <div class="panel-body">
								   <input type="checkbox" name="All" value="All"  class="upper-box">All
										<div id="employment" name="Employment" class="sub-group"></div>
								  </div>
								</div>
							  </div>
							</div>
							</div>
							<button id="btnAdvancedReport"  class="btn btn-primary" type="button">Advanced Report</button><br><br>
                        </form>
							<div id="editor"></div>
                            <button id="export_all"  class="btn btn-primary" type="button" >Generate  Report</button>

                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
					 </div>
					  <div class="col-lg-10">
					  
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Questions
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body" id ="cont">
                            <div class="table-responsive">
							<form id="getQuestionsID">
                                <table id="questionsTable"></table>
							</form>
                            </div>
							   <div class="panel panel-default">
								<div class="panel-heading">
						   		   Results 
                        </div>
						<div id="save_messge"></div>
                        <!-- /.panel-heading -->
					
					<input type="button" id="exportToExcell" value="Export to Excel" onclick="tableToExcel('table_header', 'W3C Example Table')" style="float: right; display: block;margin-right:8px; margin-top:8px;"  class="btn btn-primary"/>
					<input type="button" id="selectMainReport" value="Add To Report" style="float: right; display: block;margin-right:8px; margin-top:8px;"  class="btn btn-primary"/>
						
                        <div class="panel-body">
					<div class="row" >
                    <div class="col-lg-12">
					
                        <h2 id="table_header_text">Paragraph/Text Answer</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped" id="table_header">
                                <thead>
                                    <tr>
                                        <th>Answer</th>
                                        <th>No.of Answers</th>
                                        <th>Percentage %</th>
                                    </tr>
                                </thead>
                                <tbody id="tbatho">
                                </tbody>
                            </table>
							
							<div id="allinone">
                            </div>
						
                        </div>
                    </div>
                </div>
                            
                            </div>
							
							<input type="hidden" id="questionID" name="questionID">
							<input type="hidden" id="question" name="question">
							<input type="hidden" id="question_type" name="question_type">
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
					
                    <!-- /.panel -->

                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
					 </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
			<div class="row">
                <div class="col-lg-12">
                 
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

 <!-- jQuery -->
 
 
 
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/bootstrap-table.min.js"></script>
	<script src="js/highcharts.js"></script>
    <script src="js/highcharts-3d.js"></script>
    <script src="js/exporting.js"></script>
	<script src="js/jspdf.js"></script>
	<script src="js/rgbcolor.js"></script>
	<script src="js/StackBlur.js"></script>
    <script src="js/canvg.js"></script>
    <script src="js/jspdf.plugin.from_html.js"></script>
    <script src="js/jspdf.plugin.addhtml.js"></script>
    <script src="js/jspdf.min.js"></script>
    <script src="js/jspdf.debug.js"></script>
    <script src="js/jspdf/libs/png_support/png.js"></script>
	<script src="js/jspdf/libs/png_support/zlib.js"></script>
    <script src="js/exporting.js"></script>
	<script type="text/javascript" src="js/jquery.form.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>
	<script>

   $(document).ready(function() {
		
		//if user is not administrator hide the dermographic data
        hideDermographicDivs();
		
		
		var formID = <?php echo $_GET['id_form']; ?>;
        getAnswers(formID);
		$.ajax({
    		url: './includes/ajax_returnDemo.php?',
    		type: 'post',
    		data: {'formID' : formID, 'requestType' : 1},
    		success: function( data,textStatus ){
        		$('#client').html(data.Company);
        		$('#total').html(data.no_respondence);
        		$('#date').html(data.date);
        		$('#form').html(data.name);
				$('#survey_name').val(data.name);
				$('#client_name').val(data.Company);
				$('#client_phone').val(data.phone);
				$('#client_email').val(data.email);
				$('#c').html(data.Company);
				$('#table_header').hide();
				$('#table_header_text').hide();
				
    			//console.log("This "+data.name);
    			//var html = '';
    		},
    		error: function( jqXhr, textStatus, errorThrown ){
    			console.log( errorThrown );
    		}
		});
		//demorgraphic gender
		$.ajax({
    		url: './includes/ajax_returnDemo.php?',
    		type: 'post',
			dataType:"json",
    		data: {'formID' : formID, 'requestType' : 2},
    		success: function( data,textStatus ){


        	//var html = '<label><Strong>Gender</Strong></label><br>';
        	var html = '';//'<input type="checkbox" name="All" value="All" id="genderCheckAll" class="upper-box">All<br>';
        	var i = 0;
        	data.forEach(function(o) {
					if(data[i].count > 0){
						var x = 10 + i;
						console.log(data[i].demograph+x);
						var genderCheckbox = '<input class="sub-box" checked type="checkbox" name="gender[]" value="'+data[i].demograph+'" id="'+data[i].demograph+x+'">';	 
						html += genderCheckbox+'<label for="'+data[i].demograph+x+'">'+data[i].demograph+" :"+'<span class="badge badge-info">'+data[i].count+'</span></label><br>';	
					}
                    i++;
                });
        	    $('#genderDiv').html(html);
				$('#addCritorior').append(html);
    		},
    		error: function( jqXhr, textStatus, errorThrown ){
    			console.log( errorThrown );
    		}
		});
        //demorgraphic AGE
		$.ajax({
            url: './includes/ajax_returnDemo.php?',
            type: 'post',
            data: {'formID' : formID, 'requestType' : 6},
            success: function( data,textStatus ){
				//var  html = '<label><Strong>Age Group</Strong></label><br>';
                var html = '';//'<input type="checkbox" name="All" value="All">All<br>';
          
                var i = 0;
                data.forEach(function(o) {
					if(data[i].count > 0){
						var x = 120 + i;
						var ageCheckbox = '<input class="sub-box" checked type="checkbox" name="gender[]" value="'+data[i].demograph+'" id="'+data[i].demograph+x+'">';	 
						html += ageCheckbox+'<label for="'+data[i].demograph+x+'">'+data[i].demograph+" :"+'<span class="badge badge-info">'+data[i].count+'</span></label><br>';	
					}
                    i++;
                });
			
                $('#age').html(html);
				$('#addCritorior').append(html);
				//$('#addCritorior').append(html+genderAll);
            },
    		error: function( jqXhr, textStatus, errorThrown ){
    			console.log( errorThrown );
    		}
		});
        //demorgraphic RACE
        $.ajax({
            url: './includes/ajax_returnDemo.php?',
            type: 'post',
            data: {'formID' : formID, 'requestType' : 3},
            success: function( data,textStatus ){
				//var html = '<label><Strong>Race</Strong></label><br>';
                var html = '';//<input type="checkbox" name="All" value="All">All<br>';
            
                var i = 0;
                data.forEach(function(o) {
					if(data[i].count > 0){
						var x = 30 + i;
						var raceCheckbox = '<input class="sub-box" checked type="checkbox" name="gender[]" value="'+data[i].demograph+'" id="'+data[i].demograph+x+'">';	 
						html += raceCheckbox+'<label for="'+data[i].demograph+x+'">'+data[i].demograph+" :"+'<span class="badge badge-info">'+data[i].count+'</span></label><br>';	
					}
                    i++;
                });
                $('#race').html(html);
			    $('#addCritorior').append(html);
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );	
            }
        });
        //demorgraphic MARITAL STATUS
        $.ajax({
            url: './includes/ajax_returnDemo.php?',
            type: 'post',
            data: {'formID' : formID, 'requestType' : 4},
            success: function( data,textStatus ){
				//var html = '<label><Strong>Marital Status</Strong></label><br>';
                 var html ='';// '<input type="checkbox" name="All" value="All">All<br>';
             
                var i = 0;
                data.forEach(function(o) {
					if(data[i].count > 0){
						var x = 60 + i;
						var maritalCheckbox = '<input class="sub-box" checked type="checkbox" name="gender[]" value="'+data[i].demograph+'" id="'+data[i].demograph+x+'">';	 
						html += maritalCheckbox+'<label for="'+data[i].demograph+x+'">'+data[i].demograph+" :"+'<span class="badge badge-info">'+data[i].count+'</span></label><br>';	
					}
                    i++;
                });
                $('#marital').html(html);
				$('#addCritorior').append(html);
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );	
            }
        });
        //demorgraphic EMPLOYMENT STATUS
        $.ajax({
            url: './includes/ajax_returnDemo.php?',
            type: 'post',
            data: {'formID' : formID, 'requestType' : 5},
            success: function( data,textStatus ){
			    //var html = '<label bac="#E6E6FA"><Strong>Employment Status</Strong></label><br>';
                var html = '';//'<input type="checkbox" name="All" value="All">All<br>';

                var i = 0;
                data.forEach(function(o) {
					if(data[i].count > 0){
						var x = 90 + i;
						var empCheckbox = '<input class="sub-box" checked type="checkbox" name="gender[]" value="'+data[i].demograph+'" id="'+data[i].demograph+x+'">';	 
						html += empCheckbox+'<label for="'+data[i].demograph+x+'">'+data[i].demograph+" :"+'<span class="badge badge-info">'+data[i].count+'</span></label><br>';
					}
                    i++;
                });
                $('#employment').html(html);
				$('#addCritorior').append(html);
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );	
            }
        });
		
		$("#Div_ID").find('input[type=checkbox]').each(function () {
             // some staff
             this.checked = true;
        });

    });
$("input.upper-box").on("change", function() {
    var $this = $(this); 
     $this.next("div.sub-group")
         .find(":checkbox")
         .prop("checked", $this.prop("checked"))
     .end()
     .toggle("slow"); 
});
    /*bonus part for compensation*/
$("input.sub-box").on("change", function() {
    var $parDiv = $(this).parent(".sub-group"),
    lenCheck = $parDiv.find("input:checkbox").length,
    lenChecked = $parDiv.find("input:checked").length;
    
    if(lenCheck == lenChecked) {
        $parDiv.prev("input.upper-box")
               .prop("indeterminate", false)
               .prop("checked", true)
        .end()
        .hide("slow");
    }
    else if(lenChecked == 0) {
        $parDiv.prev("input.upper-box")
        .prop("indeterminate", false)
        .prop("checked", false);
    }
    else {
        $parDiv.prev("input.upper-box")
        .prop("indeterminate", true);
    }
});
	function checkAll(divid) {
			$('#' + divid + ' :checkbox:enabled').prop('checked', true);
	}
	function uncheckAll(divid) {
			$('#' + divid + ' :checkbox:enabled').prop('checked', false);
	}
    </script>
    <script type="text/javascript">
	
		var canadd = false;
		var chart_type = "column"
		var isQuestionSelected = false;
		
		var breaks = 0;
        var add = 0;
		var ad = 0;
		var docCount = 0;
		var a = 0;
		var answers = [];
		var textAnswersHTML = "";
		var chartDiv = "";
		var percentage = [];
		var individual = [];
		var isChart = false;
		var paragraph_array = [];
		var no_respondence = [];
		var percentage = [];
		var all_answers = [];
        function getResults(id_form){

		var arr = [];
        var cat = [];
        var dat = [];
        var v;
                  // console.log( "id "+id);
                    console.log( "id_form "+id_form);
                    var charts;
                    var url = "./ajax.generate_advance_reports.php?id_form="+id_form+'&'+$('#demographicOptions').serialize()+"&"+$('#getQuestionsID').serialize();
                    console.log(url);
                    $.getJSON(url, function(json) {
						console.log("JSON "+JSON.stringify(json[1]));
					for(var r=0;r<json[0].length;r++){
						//answers.push(json[r][0]);
						answers.push(json[0][r][0]);
					}
					
					console.log(answers);
					no_respondence.push(json[1][0].data);
					percentage.push(json[1][1].data);
					all_answers.push(answers);
					   all_answers.push("#"+no_respondence); 
					   all_answers.push("#"+percentage);
						//console.log( json.length +" answers "+JSON.stringify(json[1][0].data));
						//console.log( json.length +" answers "+(JSON.stringify(ans)));
						console.log( json.length +" json 0 "+(JSON.stringify(json[1][0].data)));
						console.log( json.length +" all_answers 1 "+(JSON.stringify(all_answers)));
                        add++;
						ad++;
						docCount++;
                        console.log(json);
                        var chartCont  = "chart";
                        //var container = chartCont + add;
						var container = 'container';
						if(json.length > 0){
						canadd = true;//"style="height:700px;margin-top:10px;width: 800px"
						if($("#question_type").val() == "paragraph" || $("#question_type").val() == "text"){
						//alert(isChart);
						for(var x = 0;x < json[0].length;x++){
					
							var f = json[1][0].data[x];
							var s = json[1][1].data[x]+"%";
							var t = answers[x];
					        chartDiv = "";
							textAnswersHTML +='<tr>\
												<td>'+t+'</td>\
												<td>'+f+'</td>\
												<td>'+s+'</td>\
											</tr>';
							isChart = false;
							$('#table_header').show();
							$('#table_header_text').show();
							}
						}else{
						for(var x = 0;x < json[0].length;x++){
					
							var f = json[1][0].data[x];
							var s = json[1][1].data[x]+"%";
							var t = answers[x];
					        chartDiv = "";
							textAnswersHTML +='<tr>\
												<td>'+t+'</td>\
												<td>'+f+'</td>\
												<td>'+s+'</td>\
											</tr>';
							}
                         chartDiv = '<div class="myChart" id="container" style="width:650px; height:250px; margin-left: 40px;"></div>\
										<div id="mytable">\
										</div>\
										<br>\
										<canvas id="canvas" width="700" height="200" style="display:none;"></canvas>\
										 <canvas id="canvas1" width="700" height="200" style="display:none;"></canvas>\
										</div>';
							isChart = true;
							$('#table_header').hide();
							$('#table_header_text').hide();
							//alert(isChart);
						}
                        $("#allinone").html(chartDiv);
						$("#tbatho").html(textAnswersHTML);
						textAnswersHTML = "";
					}else{
						 $("#allinone").html('No results');
						//$('#save_messge').html('<div class="alert alert-warning">No rerults</div>');
						//$('#save_messge').delay(3000).toggle('slow');
					}
					
					
    //get selected dermoraphics
    
    // user options
		var tableTop = 310,
        colWidth = 350,
        tableLeft = 20,
        rowHeight = 20,
        cellPadding = 2.5,
        valueDecimals = 1,
        valueSuffix = ' °C';
		
		var genderSeries = [];
		var ageSeries = [];
        var raceSeries = [];
		var maritalSeries = [];
		var employmentSeries = [];
		
		var genSeries = [];
		var agSeries = [];
		var raSeries = [];
		var marSeries = [];
		var empSeries = [];
		
		var ser = [];
		var cat = [];
		var nothing  = [];
    // internal variables
   
		var gen = age = race = marital = emplyment = "";
		//selected checkboxes/values
		//gender div selected
		var getin = false;
		var headers = [];
		var g = "Gender";
		var a = "Age Group";
		var r= "Race";
		var m = "Marital Status ";
		var e = "Employment";
		
		$('#genderDiv input:checked').each(function() {
			var val = $("label[for='" + this.id + "']").text();
			genSeries.push(val);
			console.log(val);
		
			getin = true;
		});
		
		//ageGroup div selected
		$('#age input:checked').each(function() {
			var val = $("label[for='" + this.id + "']").text();
			agSeries.push(val);
			getin = true;
		});
		//Race div selected
		$('#race input:checked').each(function() {
			var val = $("label[for='" + this.id + "']").text();
			raSeries.push(val);
			//raceSeries.push(val);
			getin = true;
			
		});
		//Marital div selected
		$('#marital input:checked').each(function() {
			var val = $("label[for='" + this.id + "']").text();
			marSeries.push(val);
			//maritalSeries.push(val);
		});
		//Employment div selected
		$('#employment input:checked').each(function() {
			var val = $("label[for='" + this.id + "']").text();
			empSeries.push(val);
			//employmentSeries.push(val);
			getin = true;
		});
		var textValue = "";
		
		function toObject(arr) {
			var rv = {};
			for (var i = 0; i < arr.length; ++i)
			if (arr[i] !== undefined) rv[i] = arr[i];
			return rv;
		}
	
	  //find highest value and append the row
	  
	  var getdemo_url = "id_form="+id_form+'&'+$('#demographicOptions').serialize()+"&"+$('#getQuestionsID').serialize();
     $.ajax({
			type: "POST",
			url: './ajax.generate_foundadvance_reports.php',
			dataType:'json',
			data: getdemo_url,
			success: function(data){
				console.log(data);
				for (var i = 0; i < data.length; i++) {
				    if (JSON.stringify(data[i].gender)) {
				    	genderSeries.push(data[i].gender);
				    }
				    if (JSON.stringify(data[i].age)) {
				    	ageSeries.push(data[i].age);
				    }
				    if (JSON.stringify(data[i].race)) {
				    	raceSeries.push(data[i].race);
				    }
				    if (JSON.stringify(data[i].marital_status)) {
				    	maritalSeries.push(data[i].marital_status);
				    }
				    if (JSON.stringify(data[i].employment)) {
				    	employmentSeries.push(data[i].employment);
				    }
				}
				
	  genderLength = genderSeries.length;
	  ageLength = ageSeries.length;
	  raceLength = raceSeries.length;
	  maritalLength = maritalSeries.length;
	  empLength = employmentSeries.length;
	  
	  genSeriesLength = genSeries.length;
	  agSeriesLength = agSeries.length;
	  raSeriesLength = raSeries.length;
	  marSeriesLength = marSeries.length;
	  empSeriesLength = empSeries.length;
	  
	  
	  var array = [genderLength, ageLength, raceLength ,maritalLength,empLength];
      var largest = Math.max.apply(Math, array);

		 for(i = 0;i < largest - genderLength; i++){
			  genderSeries.push("-");
		 }
		for(i = 0;i < largest - ageLength; i++){
			  ageSeries.push("-");
		 }
		for(i = 0;i < largest - raceLength; i++){
			 raceSeries.push("-");
		 }
		 
		for(i = 0;i < largest - maritalLength; i++){
			 maritalSeries.push("-");
		 }	
		 
		 for(i = 0;i < largest - empLength; i++){
			   employmentSeries.push("-");
		 }		

	  
	  
	  var genderObject = toObject(genderSeries);
	  var ageObject = toObject(ageSeries);
	  var raceObject = toObject(raceSeries);
	  var maritalObject = toObject(maritalSeries);
	  var employmentObject = toObject(employmentSeries);
     
	  var head = [];
	  head.push(g);
	  head.push(a);
	  head.push(r);
	  head.push(m);
	  head.push(e);
	 // console.log("obj "+JSON.stringify(obj));
		
	  var gh = g+"<br>"+a+"<br>"+r+"<br>"+m+"<br>"+e;
		
	  console.log("g "+g);
	  console.log("a "+a);
	  console.log("r "+r);
	  console.log("m "+m);
	  console.log("e "+e);
		
	  genSeriesLength = genSeries.length;
	  agSeriesLength = agSeries.length;
	  raSeriesLength = raSeries.length;
	  marSeriesLength = marSeries.length;
	  empSeriesLength = empSeries.length;	
		
	var genderObject1 = toObject(genSeries);
	var ageObject1 = toObject(agSeries);
	var raceObject1 = toObject(raSeries);
	var maritalObject1 = toObject(marSeries);
	var employmentObject1 = toObject(empSeries);	
	
    var array1 = [genSeriesLength, agSeriesLength, raSeriesLength ,marSeriesLength,empSeriesLength];
     var largest1 = Math.max.apply(Math, array1);	
      
	
    addToMainReport(genSeries,agSeries,raSeries,marSeries,empSeries,largest1);
	
		 for(i = 0;i < largest1 - genSeriesLength; i++){
			  genSeries.push("-");
		 }
		for(i = 0;i < largest1 - agSeriesLength; i++){
			  agSeries.push("-");
		 }
		for(i = 0;i < largest1 - raSeriesLength; i++){
			 raSeries.push("-");
		 }
		 
		for(i = 0;i < largest1 - marSeriesLength; i++){
			 marSeries.push("-");
		 }	
		 
		 for(i = 0;i < largest1 - empSeriesLength; i++){
			   empSeries.push("-");
		 }		
	var tab = [];
	tab.push(genderObject1);
	tab.push(ageObject1);
	tab.push(raceObject1);
	tab.push(maritalObject1);
	tab.push(employmentObject1);
			console.log("genderObject1 "+JSON.stringify(agSeries));
	$.makeTable = function (mydata) {
    var table = $('<table class="table table-bordered" id="mytab" >');
    var tblHeader = "<tr class='active'>";
    for (var k in mydata[0]) tblHeader += "<th>" + k + "</th>";
    tblHeader += "</tr>";
   // $(tblHeader).appendTo(table);
   var comp = 1;
    $.each(mydata, function (index, value) {
        var TableRow = '<tr>';
        $.each(value, function (key, val) {
			//console.log(val);
            TableRow += "<td>" + val + "</td>";
        });
        TableRow += "</tr>";
        $(table).append(TableRow);
    });
    return ($(table));
};
	var mydata = eval(tab);
	var table = $.makeTable(mydata);
	$("#table").html(table);
	var t = 0;
	$("#table").find('tr').each(function(){
		
		console.log("head t "+t+" "+head[t]);
           var trow = $(this);
            // if(trow.index() === 0){
             //    trow.prepend('<td>xxxxxxxxxxx</td>');
             //}else{
                 trow.prepend('<td ><strong>'+head[t]+'</strong></td>');
				 t++;
            // }
            
         });
    // draw category labels
		//var head = '';
          }
        });
	  if(isChart == true){
                    var canvas = document.getElementById("canvas1");
                    var ctx = canvas.getContext("2d");
                    var data = "<svg xmlns='http://www.w3.org/2000/svg' width='700' height='100'>" +
                        "<foreignObject width='100%' height='100%'>" + $("#mytable").html() +
                        "</foreignObject>" +
                        "</svg>";
                    var DOMURL = self.URL || self.webkitURL || self;
                    var img = new Image();
                    var svg = new Blob([data], {
                        type: "image/svg+xml;charset=utf-8"
                    });
                    var url = DOMURL.createObjectURL(svg);
                    img.onload = function() {
                        ctx.drawImage(img, 0, 0);
                        DOMURL.revokeObjectURL(url);
                    };
                    img.src = url;
                    console.log("img.src "+img);
					
        var category  = json[1][0].answers;
		var car = [];
		for(var j = 0;j < json[0].lenght; j++){
			car.push(json[0][0][0]);
		}
		
			console.log("car "+ car);//actegory.push(json[r][0]
		var body = '';
		var c ;
		var plot1 =  "";
		  //if(chart_type == "pie"){
			plot1 =  { 
			plotOptions: {
            series: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
			}};
		//  }
		//  else{
			 var plot =  { 
			 plotOptions: {
             series: {
                dataLabels: {
                    enabled: true,
                    borderRadius: 3,
                    backgroundColor: 'rgba(252, 255, 197, 0.7)',
                    borderWidth: 1,
                    borderColor: '#AAA',
                    y: -6
                }
            }
			}};
		 // }
		 
		  $('#bar').click(function () {
			 chart_type = "bar";
			 //getChartType('column');
		 });
		  $('#line').click(function () {
			  chart_type = "line";
			// getChartType('line');
			  
		  });
		 $('#area').click(function () {
			 chart_type = "area"
			 //getChartType( chart_type);
		
		  });
		  $('#pie').click(function () {
			  chart_type = "pie";
			  //getChartType(chart_type );
		});
		 $('#scatter').click(function () {
			  chart_type = "scatter";
			  //getChartType(chart_type );
		});
		 $('#coloumn').click(function () {
				chart_type = "column";
				//getChartType(chart_type );
			});
		 $('#spline').click(function () {
				chart_type = "spline"
				//getChartType(chart_type );
			});
		 $('#area_spline').click(function () {
			  chart_type = "areaspline";
			 // getChartType(chart_type );
		});		
			
		var car_array = [];
		for(var j = 0;j < json[0].length; j++){
				car_array.push(json[0][j][0]);
			}	
			
			
		if(chart_type == "pie"){
			charts = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: chart_type,
				options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
            },
            title: {
                text: $('#question').val(),
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20,
				y:35
            },
            xAxis: {
                categories: category
            },
			plotOptions: {
            series: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
			},
            yAxis: {
                title: {
                    text: 'Insurvey'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
			exporting: {
				enabled:false
			},
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y ;
                }
            },
            series: [{
           type: chart_type,
            data: json[0]
        }]
        });	
		}else{
		charts = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: chart_type,
				options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
			}
            },
            title: {
                text: $('#question').val(),
                x: -20 //center
            },
            subtitle: {
                text: '',
                x: -20,
				y:35
            },
            xAxis: {
                categories: car_array
            },
			plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
					crop: false,
                    borderRadius: 3,
                    backgroundColor: 'rgba(252, 255, 197, 0.7)',
                    borderWidth: 1,
                    borderColor: '#AAA',
                    y: -6
                }
            }
			},
            yAxis: {
                title: {
                    text: 'Insurvey'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
			exporting: {
				enabled:false
			},
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y ;
                }
            },
            series: json[1]
        });
		}
		car_array = [];	
	 var chart = $('#container').highcharts();
   
		function getChartType(type){
	        chart.inverted = true;
			 chart.xAxis[0].update({}, false);
			 chart.yAxis[0].update({}, false);
			 chart.series[0].update({
             type: type
			});
		}
}

	$( ".remove" ).click(function() {
		     $(this).parent().remove();
		});
	$('tspan').html(function (i, html)
	{
		return html.replace(",gogo", '');
	});
	$('tspan').html(function (i, html)
	{
		console.log("cat.length "+cat.length);
		return html.replace("[object HTMLCollection]", '');
	});
	if(getin == false)
	$('tspan').html(function (i, html)
	{
		console.log("cat.length "+cat.length);
		return html.replace(",", '');
	});
	$('tspan').html(function (i, html)
	{
		console.log("cat.length "+cat.length);
		return html.replace("NaN", '');
	});
	//$(':contains("gogo")').each(function(){
	//	$(this).html($(this).html().split(",gogo").join(""));
	//});
	console.log("ad "+ad);
	if(ad == 3){
		ad = 0;
		breaks++;
	}
    });
 }
		  var id_report_fk = 0;
 	      var date = new Date();
                m = date.getMonth() + 1;
                d = date.getDate();
                y = date.getFullYear();
                t = date.toLocaleTimeString().toUpperCase();
                var today = (d + '-' + m + '-' + y + ' ' + t);
                var getDate = $('#client').text()+"-Report-"+today;
                var chart_file = getDate+".pdf";
            (function (H) {
                H.Chart.prototype.createCanvas = function (divId) {
                    var svg = this.getSVG(),
                        width = parseInt(svg.match(/width="([0-9]+)"/)[1]),
                        height = parseInt(svg.match(/height="([0-9]+)"/)[1]),
                        canvas = document.createElement('canvas');

                    canvas.setAttribute('width', width);
                    canvas.setAttribute('height', height);

                    if (canvas.getContext && canvas.getContext('2d')) {

                        canvg(canvas, svg);

                        return canvas.toDataURL("image/jpeg");

                    }
                    else {
                        alert("Your browser doesn't support this feature, please use a modern browser");
                        return false;
                    }

                }
            }(Highcharts));

            $('#addtomainReport1').click(function () {
                var doc = new jsPDF();
				var image = getBase64FromImageUrl('./images/ic_logo.png');
				$image1 = "./includes/150629023916.jpg"
                // chart height defined here so each chart can be palced
                // in a different position
                var chartHeight = 80;
				//doc.addImage(logo, 'JPEG', 10, 10, 60, 30);
                // All units are in the set measurement for the document
                // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
				console.log("breaks "+breaks);
				
                doc.setFontSize(16);
                doc.text(20, 20, "Client: "+$('#client').text());
				doc.setFontSize(12);
				doc.text(20, 30, $('#form').text()+" Summary Report");
				doc.setFontSize(10);
				doc.text(20, 35, 'Date :'+today);
				
			/*for(i =0;i<breaks - 1;i++){
					console.log("breaks in"+breaks);
					doc.addPage();
					
				}*/
                //loop through each chart
				var i;
                $('.myChart').each(function (index) {
                    var imageData = $(this).highcharts().createCanvas();

                    // add image to doc, if you have lots of charts,
                    // you will need to check if you have gone bigger
                    // than a page and do doc.addPage() before adding
                    // another image.

                    /**
                     * addImage(imagedata, type, x, y, width, height)
                     */
                        var rep_desc = $('#rep_description').val();
                        var rep_name = $('#rep_name').val();
         
				  	//console.log("count "+count++); 
					//if(index > 0){
						// doc.addPage();
						 //doc.addImage(image, 'JPEG', 45, (index * chartHeight) + 40, 120, chartHeight);
					//} 
                  
                });
				doc.addImage(image, 'JPEG', 45, (22 * chartHeight) + 40, 120, chartHeight);
                doc.save(chart_file);
            }); 
			
			
			$("#addtomainReport1").click(function(){
				var svg = document.getElementById('container').children[0].innerHTML;
				canvg(document.getElementById('canvas'),svg);
				var img = canvas.toDataURL("image/png"); //img is data:image/png;base64
				img = img.replace('data:image/png;base64,', '');
				var data = "bin_data=" + img;
				$.ajax({
					type: "POST",
					url: './includes/savecharts.php',
					data: data,
					success: function(data){
					//alert(data);
          }
        });
    });

//add to main report
		$('#selected_report').click(function () {
            $("#addtoReportModal").modal('show');
		});
        $('#selectMainReport').click(function () {
            console.log(canadd);
            if(canadd == false){
                $('#save_messge').html('<div class="alert alert-danger">Create chart to add to report</div>');
                $('#save_messge').delay(3000).toggle('slow');
            }else{
                $("#selectReportModal").modal('show');
            }

        });

		
    </script>

	
    <script type="text/javascript">
        function getAnswers(formID){
          path = './ajax.getQuestions.php?id_form='+formID;
          $('#questionsTable').bootstrapTable({
              method: 'GET',
              url: path,
              cache: false,
              height: 400,
              striped: true,
              pagination: true,
              pageSize: 10,
              pageList: [10, 25, 50, 100, 200],
              search: true,
              showColumns: false,
              showRefresh: true,
              showToggle:true,
              minimumCountColumns: 2,
              clickToSelect: true,
              columns:  [{
                  field: 'operate',
                  title: 'Select',
                  align: 'center',
                  valign: 'middle',
                  clickToSelect: false,
                  formatter: operateFormatterAnswer,
                  events: operateEventsAnswer
              },{
                field: 'id_question',
                title: 'ID',
                align: 'left',
                valign: 'middle',
                sortable: true
            }, {
                field: 'quesiton',
                title: 'Question',
                align: 'left',
                valign: 'middle',
                sortable: true
            }, {
                field: 'type',
                title: 'Type',
                align: 'left',
                valign: 'middle',
                sortable: true
            }]
          });
      }
	  
      function operateFormatterAnswer(value, row, index) {
        return [
          '<input class="questionID" type="checkbox" value="'+row['id_question']+'" name="id_question[]" style="margin-right:5px;" ><span class="icomoon-icon-pencil"></span></checkbox>'
          ].join('');
      }
      window.operateEventsAnswer = {
        'click .questionID': function (e, value, row, index) {
		$('#question').val(row['quesiton']);
	
			
		if ($(this).is(":checked")) {
			$('#question').val(row['quesiton']);
			$('#question_type').val(row['type']);
            textValue =  row['quesiton'];
			isQuestionSelected = true;
		} else {
            isQuestionSelected = false;
			$('#question').val('');
        }
        },
        'click .delete': function (e, value, row, index) {
			
        }
      };
	$('#btnAdvancedReport').click(function () {
			if(isQuestionSelected == false){
				 $('#save_messge').html('<div class="alert alert-danger">Select the question and dermographic criterior</div>');
				 $('#save_messge').delay(3000).toggle('slow');
			}else{
				var formID = <?php echo $_GET['id_form']; ?>;
				getResults(formID);
			}
	});
	$('#export_all').click(function () {
		var co = $('#client_name').val();
		var sur = $('#survey_name').val();
		$('#survey').html(sur);
		$('#company').html(co);
		
		$("#myModal").modal('show');
	});	

	
	 $('#generateReport').click(function () {
		 var demo = getOverAllDermographics();
		 console.log("noooo "+ './includes/generatePDF.php?'+$('#generateFinalMainReport').serialize()+'&demo1='+demo[0]+'&demo2='+demo[1]+'&demo3='+demo[2]+'&demo4='+demo[3]+'&demo5='+demo[4]);
		 window.location = './includes/generatePDF.php?'+$('#generateFinalMainReport').serialize()
		 +'&demo1='+demo[0]+'&demo2='+demo[1]+'&demo3='+demo[2]+'&demo4='+demo[3]+'&demo5='+demo[4];
	});
    function addToMainReport(genderSeries,ageSeries,raceSeries,maritalSeries,employmentSeries,highestDemorgraph){
        var saved = false;
        var id_report_fk = 0;
        $(".gerReportIDClass").click(function(){
            id_report_fk = $(this).attr('value');
        });

        $("#addAltoMainReport").click(function(){
            if(id_report_fk == 0){
                $('#select_report_from_list').html('<div class="alert alert-danger">Select report</div>');
            }else{

                console.log("Ke tsene wena o tsene");
               
				//configure file
				var array = [];
				array.push(all_answers);
                var bin_data = "";
				console.log("'#question_type').val() "+$('#question_type').val());
				if($('#question_type').val() == "text" || $('#question_type').val() == "paragraph"){
					bin_data ="bin_data=" + array+"&request=paragraph";
					console.log('file includes/saveCharts.php?'+bin_data);
				}else{
					var svg = document.getElementById('container').children[0].innerHTML;
					canvg(document.getElementById('canvas'),svg);
					var img = canvas.toDataURL("image/png"); //img is data:image/png;base64
					img = img.replace('data:image/png;base64,', '');
					bin_data ="bin_data=" + img+"&request=chart";
					console.log('image includes/saveCharts.php?'+bin_data);
				}

                var question_description = $('#question_description').val();
                var question_name = $('#question_name').val();
                $('#selected_report_id').val(id_report_fk);
                $('#selected_report_id').val(id_report_fk);
			
                $.ajax({
                    type: "POST",
                    url: './includes/saveCharts.php',
                    data: bin_data,
                    success: function(data){
                        console.log("Image "+data.cnt);
                        $('#selected_chart_path').val(data.file);

                        addCharSucess = data.cnt;
                        if(data.cnt > 0){
							console.log("genderSeries "+JSON.stringify(genderSeries));
                            $.ajax({
                                url:"./ajax_create_report.php",
                                type: "POST",
                                data: {'selected_report_id':id_report_fk,'question_name':question_name,'question_description':question_description
                                    ,'selected_chart_path':data.file, 'gender':genderSeries,'age':ageSeries,'race':raceSeries,'marital':maritalSeries,'employment':employmentSeries,"highestDemorgraph":highestDemorgraph},
                                success: function(data){
                                    console.log("report "+data);
                                    if(data > 0){
                                        saved = true;
                                        $('#select_report_from_list').html('<div class="alert alert-success">Report was saved successfully</div>');
                                        $('#select_report_from_list').delay(3000).toggle('slow');
                                        // location.reload().delay(3000);
                                    }else{
                                        $('#select_report_from_list').html('<div class="alert alert-danger">Report was not saved</div>');
                                        $('#select_report_from_list').delay(100).toggle('slow');
                                    }

                                }
                                //alert(data);
                            });
                        }
                    }
                });
            }

        });
        $('#CloseaddAltoMainReport').click(function(){
            if(saved == true){
                location.reload();
            }
        });
    }

</script>
<script>
    var report_saved = false;
        $("#addtomainReport").click(function(){
            console.log("./ajax_add_report.php?"+$('#addtoReportModal').serialize());
            console.log("Ke tsene wena");
            $.ajax({
                url:"./ajax_add_report.php",
                type: "POST",
                data: $('#addToReportForm').serialize(),
                success: function(data){
                    console.log("report "+data);
                    if(data > 0){
                        report_saved = true;
                        $('#add_report_resulis').html('<div class="alert alert-success">Saved successfully</div>');
                        $('#add_report_resulis').delay(3000).toggle('slow');
                    }else{
                        $('#save_messge').html('<div class="alert alert-danger">Not saved</div>');
                    }

                }
                //alert(data);
            });

        });
    $('#addtomainReportClose').click(function(){
        if(report_saved == true){
            location.reload();
        }
    });

</script>
<script>
   // ajax_getReportBySurveyID.php

    $('#report_option').change(function(){
        var reportID = $('#report_option').val();
        $('#id_report').val(reportID);
         console.log('includes/ajax_getReportBySurveyID.php?reportID='+reportID);
        $.ajax({
            url: './includes/ajax_getReportBySurveyID.php',
            type: 'post',
			dataType: "json",
            data: {'reportID' : reportID},
            success: function(data,textStatus ){
                console.log("data "+data);
                var report_html;
                var i = 0;
                var html = "";
                data.data.forEach(function(rep) {
						if(rep.name != ""){
                         report_html = '<div ><input type="checkbox" name="id[]" id="checkboxes-0" value="'+rep.id_chart+'">'+rep.name+'</div></div>';
						 html+=report_html;
						}
						i++;
                });
                $('#found_report').html(html+'<br>');
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });

    });
</script>
<script type="text/javascript">
$(document).ready(function() { 
	var options = { 
			target: '#output',   // target element(s) to be updated with server response 
			beforeSubmit: beforeSubmit,  // pre-submit callback 
			success: afterSuccess,  // post-submit callback 
			resetForm: true        // reset the form after successful submit 
		}; 
		
	 $('#MyUploadForm').submit(function() { 
			$(this).ajaxSubmit(options);  			
			// always return false to prevent standard browser submit and page navigation 
			return false; 
		}); 
}); 

function afterSuccess()
{
	$('#submit-btn').show(); //hide submit button
	$('#loading-img').hide(); //hide submit button

}

//function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		
		if( !$('#imageInput').val()) //check empty input filed
		{
			$("#output").html("Select logo");
			return false
		}
		
		var fsize = $('#imageInput')[0].files[0].size; //get file size
		var ftype = $('#imageInput')[0].files[0].type; // get file type
		

		//allow only valid image file types 
		switch(ftype)
        {
            case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                break;
            default:
                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
				return false
        }
		
		//Allowed file size is less than 1 MB (1048576)
		if(fsize>1048576) 
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
			return false
		}
				
		$('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
	}
	else
	{
		//Output error to older browsers that do not support HTML5 File API
		$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
		return false;
	}
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

</script>
<script>
function getOverAllDermographics(){
	
        var genderDemo = [];
		var ageDemo = [];
        var raceDemo = [];
		var maritalDemo = [];
		var employmentDemo = [];
		var demo = [];
    // internal variables
		var gen = age = race = marital = emplyment = "";
		//selected checkboxes/values
		//gender div selected
		var getin = false;
		$('#genderDiv :input').each(function() {
			var val = $("label[for='" + this.id + "']").text();
			genderDemo.push(val);
		});
		
		//ageGroup div selected
		$('#age :input').each(function() {
			var val = $("label[for='" + this.id + "']").text();
		    ageDemo.push(val);
		});
		//Race div selected
		$('#race :input').each(function() {
			var val = $("label[for='" + this.id + "']").text();
			raceDemo.push(val);
			
		});
		//Marital div selected
		$('#marital :input').each(function() {
			var val = $("label[for='" + this.id + "']").text();
			maritalDemo.push(val);
		});
		//Employment div selected
		$('#employment :input').each(function() {
			var val = $("label[for='" + this.id + "']").text();
			employmentDemo.push(val);
		});
		
	  //find highest value and append the row
		
	  genderDemoLength = genderDemo.length;
	  ageDemoLength = ageDemo.length;
	  raceDemoLength = raceDemo.length;
	  maritalDemoLength = maritalDemo.length;
	  empDemoLength = employmentDemo.length;
	  
	  var array_demo = [genderDemoLength, ageDemoLength, raceDemoLength ,maritalDemoLength,empDemoLength];
      var largest = Math.max.apply(Math, array_demo);
	  $('#highest_demo').val(largest);

		 for(i = 0;i < largest - genderDemoLength; i++){
			  genderDemo.push("-");
		 }
		for(i = 0;i < largest - ageDemoLength; i++){
			  ageDemo.push("-");
		 }
		for(i = 0;i < largest - raceDemoLength; i++){
			 raceDemo.push("-");
		 }
		 
		for(i = 0;i < largest - maritalDemoLength; i++){
			 maritalDemo.push("-");
		 }	
		 
		 for(i = 0;i < largest - empDemoLength; i++){
			 employmentDemo.push("-");
		 }	
		
		var test = [];
		
		demo.push(genderDemo);	
		demo.push(ageDemo);
		demo.push(raceDemo);	
		demo.push(maritalDemo);
		demo.push(employmentDemo);
	
		return demo;
}
</script>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()
</script>

	

<script>
function hideDermographicDivs(){
	
	var permission = <?php echo $_SESSION['permission']; ?>;
	
	if(permission != 1){
		$('#demographs').hide();
		console.log("Hidden");
	}
	
	
}



</script>
</body>

</html>
