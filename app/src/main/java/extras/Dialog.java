package extras;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Vic on 1/5/2010.
 */
public class Dialog {
    private ProgressDialog pDialog;

    public Dialog(Context ctx, String text) {
        pDialog = new ProgressDialog(ctx);
        pDialog.setMessage(text);
        pDialog.setCancelable(false);
    }
    public void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
        pDialog.hide();
    }
    public void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
}
